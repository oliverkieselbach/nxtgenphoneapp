//
//  ConversationViewController.swift
//  NextGenPhoneApp
//
//  Created by Kieselbach, Oliver on 27.05.19.
//  Copyright © 2019 Kieselbach, Oliver. All rights reserved.
//

import UIKit
import BmoViewPager
import SmoothPicker
import NextGenPhoneFramework
import FirebaseFirestore
import BonsaiController

class HorizontalDashedView: UIView {
    override public func layoutSubviews() {
        super.layoutSubviews()
        self.makeHorizontalDashedBorderLine()
    }

}

class ConversationHeader: UIView {
    
    var conversation: CKConversation! {
        
        didSet {
            
            self.myAvatar.contactWithNoStatus = CKContactBasic(person: ConversationKit.shared.appUser)
            self.partnerAvatar.contactWithNoStatus = conversation.contactDetails
            
            self.myName.text = "You"
            self.partnerName.text = conversation.contactDetails.name
            
        }
        
    }

    var myAvatar: CKContactView! = {
        
        let view = CKContactView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.heightAnchor.constraint(equalToConstant: 50).isActive = true
        view.widthAnchor.constraint(equalToConstant: 50).isActive = true
        
        return view
        
    }()
    
    var myName: UILabel! = {
        
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: 13, weight: UIFont.Weight.regular)
        label.textAlignment = .center
        label.numberOfLines = 2

        return label
        
    }()

    var partnerAvatar: CKContactView! = {
        
        let view = CKContactView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.heightAnchor.constraint(equalToConstant: 50).isActive = true
        view.widthAnchor.constraint(equalToConstant: 50).isActive = true
        
        return view
        
    }()
    
    var partnerName: UILabel! = {
        
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: 13, weight: UIFont.Weight.regular)
        label.textAlignment = .center
        label.numberOfLines = 2

        return label
        
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let dashedView = HorizontalDashedView(frame: .zero)
        dashedView.translatesAutoresizingMaskIntoConstraints = false

        self.addSubview(dashedView)
        self.addSubview(self.myAvatar)
        //self.addSubview(self.myName)
        self.addSubview(self.partnerAvatar)
        //self.addSubview(self.partnerName)
        
        self.myAvatar.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        self.myAvatar.rightAnchor.constraint(equalTo: self.centerXAnchor, constant: -15).isActive = true

        self.partnerAvatar.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        self.partnerAvatar.leftAnchor.constraint(equalTo: self.centerXAnchor, constant: 15).isActive = true

        /*
        self.myName.topAnchor.constraint(equalTo: self.myAvatar.bottomAnchor, constant: 3).isActive = true
        self.myName.widthAnchor.constraint(equalToConstant: 60).isActive = true
        self.myName.centerXAnchor.constraint(equalTo: self.myAvatar.centerXAnchor, constant: 0).isActive = true

        self.partnerName.topAnchor.constraint(equalTo: self.partnerAvatar.bottomAnchor, constant: 3).isActive = true
        self.partnerName.widthAnchor.constraint(equalToConstant: 60).isActive = true
        self.partnerName.centerXAnchor.constraint(equalTo: self.partnerAvatar.centerXAnchor, constant: 0).isActive = true
        */
        dashedView.centerYAnchor.constraint(equalTo: self.myAvatar.centerYAnchor).isActive = true
        dashedView.leftAnchor.constraint(equalTo: self.myAvatar.rightAnchor, constant: 0).isActive = true
        dashedView.rightAnchor.constraint(equalTo: self.partnerAvatar.leftAnchor, constant: 0).isActive = true
        dashedView.heightAnchor.constraint(equalToConstant: 2).isActive = true

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

class ConversationViewController: UIViewController {

    
    var messages: [CKMessage]!
    var conversation: CKConversation! {
        
        didSet {

            self.header.conversation = conversation
            
            self.headerLabel.text = "You & \(conversation.contactDetails.name)"

        }

    }
    
    var messageCardViews: [MessageCardView]! = []

    var walletView: WalletView!
    
    /*
    var picker: SmoothPickerView = {
        
        let v = SmoothPickerView(frame: .zero)
        v.translatesAutoresizingMaskIntoConstraints = false
        
        return v
        
        
    }()
    
    var viewPager: BmoViewPager! = {
        
        let v = BmoViewPager()
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
        
    }()
    */
    
    var header: ConversationHeader! = {

        let v = ConversationHeader(frame: .zero)
        v.translatesAutoresizingMaskIntoConstraints = false
        v.backgroundColor = .white
        v.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        return v
        
    }()
    
    var headerLabel: UILabel! = {

        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .gray
        label.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.regular)
        label.textAlignment = .center
        label.numberOfLines = 1
        label.backgroundColor = .white
        
        return label

    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Conversation"
        self.view.backgroundColor = .clear
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(self.cancelAction))
        
        
        let effect = UIBlurEffect(style: .regular)
        var effectView = UIVisualEffectView(effect: effect)
        effectView.frame = self.view.frame
        self.view.addSubview(effectView)
        
        self.header.layoutIfNeeded()
        self.header.sizeToFit()
        self.header.translatesAutoresizingMaskIntoConstraints = true
        self.navigationItem.titleView = self.header
        
        self.view.addSubview(self.headerLabel)
        self.headerLabel.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 0).isActive = true
        self.headerLabel.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 0).isActive = true
        self.headerLabel.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 0).isActive = true
        self.headerLabel.heightAnchor.constraint(equalToConstant: 22).isActive = true
        
        
        self.walletView = WalletView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 0, height: self.view.frame.height - 64))
        self.walletView.contentInset = UIEdgeInsets(top: 30, left: 0, bottom: 0, right: 0)
        self.walletView.walletHeader = nil
        self.walletView.walletHeaderHeight = 0
        self.walletView.distanceBetweenCardViews = 10
        self.walletView.distanceBetweetCollapsedAndPresentedCardViews = 50
        self.walletView.minimalDistanceBetweenStackedCardViews = 90
        self.walletView.minimalDistanceBetweenCollapsedCardViews = 10

        
        self.view.addSubview(self.walletView)
        

    }
    
    @objc func cancelAction() {
        
        self.dbListener = nil
        self.dismiss(animated: true) {
        }
        
    }

    var dbListener: ListenerRegistration!
    
    func loadData() {

        self.dbListener = ConversationKit.shared.db.collection(ConversationKit.shared.appUser.id).document("data").collection("conversations").document(conversation.id).collection("messages").order(by: "date", descending: false).addSnapshotListener { (querySnapshot, error) in

            guard let snapshot = querySnapshot else {
                print(error?.localizedDescription ?? "Error reading messages of conversation \(self.conversation.id)")
                return
            }

            self.messages = []
            self.messageCardViews = []

            for documentSnapshot in snapshot.documents {
                
                let data = documentSnapshot.data()
                if(data != nil) {
                    
                    var message = CKMessage(id: documentSnapshot.documentID, dictionary: data)
                    if(message != nil) {
                        message?.conversationId = self.conversation.id
                        self.messages.append(message!)
                        
                        let cardView = MessageCardView()
                        cardView.respondClosure =  {message in
                            self.respondAction(message: message)
                        }
                        cardView.playMessageAction = {message in
                            self.playMessageAction(message: message)
                        }
                        cardView.message = message
                        self.messageCardViews.append(cardView)
                        self.addChild(cardView.messageFormViewController)
                        
                    }
                    
                }
            }
            
            self.walletView.reload(cardViews: self.messageCardViews)
            
        }
        
        
    }
    
    var presentedVC: AnyClass!
    
    private func playMessageAction(message: CKMessage) {
        
        
        if(message.messageType! == .voice) {
            let vc = VoiceMessagePlayerViewController()
            vc.message = message
            vc.transitioningDelegate = self
            vc.modalPresentationStyle = .custom
            self.presentedVC = vc.classForCoder
            
            
            self.present(vc, animated: true) {
                vc.playPauseAction()
            }
            
        }
        else if(message.messageType! == .video) {
            let vc = VideoMessagePlayerViewController()
            vc.message = message
            vc.transitioningDelegate = self
            vc.modalPresentationStyle = .custom
            self.presentedVC = vc.classForCoder
            
            
            self.present(vc, animated: true) {
                vc.playPauseAction()
            }
        }
        
        
        
        
    }
    
    
    func respondAction(message: CKMessage) {
        

        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "communicator.communicate.action"), object: nil, userInfo: ["contact" :  CKContact(personEmbedded: self.conversation.contactDetails), "conversationId": self.conversation.id, "contextMessage": message, "fromView" : self.view, "presentingViewController": self])
        
    }

}

extension ConversationViewController: BonsaiControllerDelegate {
    
    public func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        
        if(presented.classForCoder == VoiceMessagePlayerViewController.classForCoder() || presented.classForCoder == VideoMessagePlayerViewController.classForCoder()) {
            return BonsaiController(fromDirection: .bottom, blurEffectStyle: .dark, presentedViewController: presented, delegate: self)
        }
        else {
            return BonsaiController(fromView: self.view, blurEffectStyle: .dark,  presentedViewController: presented, delegate: self)
        }
    }
    
    public func frameOfPresentedView(in containerViewFrame: CGRect) -> CGRect {
        
        if(self.presentedVC == VoiceMessagePlayerViewController.classForCoder() || self.presentedVC == VideoMessagePlayerViewController.classForCoder()) {
            return CGRect(origin: CGPoint(x: 0, y: containerViewFrame.size.height * 0.3), size: CGSize(width: containerViewFrame.width, height: containerViewFrame.height * 0.7))
        }
        else {
            return CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: containerViewFrame.width, height: containerViewFrame.height))
        }
        
    }
}

/*
extension ConversationViewController: SmoothPickerViewDelegate {
    func didSelectItem(index: Int, view: UIView, pickerView: SmoothPickerView) {
        print("item selected")
        self.viewPager.presentedPageIndex = index
    }
}

extension ConversationViewController: SmoothPickerViewDataSource {
    func numberOfItems(pickerView: SmoothPickerView) -> Int {
        return self.messages.count
    }
    
    func itemForIndex(index: Int, pickerView: SmoothPickerView) -> UIView {
        
        let view = UIView()
        view.backgroundColor = UIColor.orange.withAlphaComponent(0.2)
        
        let label = UILabel()
        label.backgroundColor = .clear
        label.font = UIFont.systemFont(ofSize: 15, weight: .bold)
        label.textColor = .orange
        label.textAlignment = .center
        label.text = messages[index].topicAction == "" ? messages[index].topic : messages[index].topicAction
        label.sizeToFit()
        view.frame = CGRect(x: 0, y: 0, width: label.frame.size.width * 1.2, height: label.frame.size.height * 1.3)
        view.addSubview(label)
        label.frame = view.frame

        view.layer.cornerRadius = 5
        
        return view
        
    }
    
    
}


extension ConversationViewController: BmoViewPagerDelegate {
    
    func bmoViewPagerDelegate(_ viewPager: BmoViewPager, pageChanged page: Int) {
        
        self.picker.scrollToItem(index: page)
        self.picker.currentSelectedIndex = page
        
    }
    
    
    func bmoViewPagerDelegate(_ viewPager: BmoViewPager, shouldSelect page: Int) -> Bool {
        return true
    }
    
    
    
}

extension ConversationViewController: BmoViewPagerDataSource {
    func bmoViewPagerDataSourceNumberOfPage(in viewPager: BmoViewPager) -> Int {
        return 3
    }
    
    func bmoViewPagerDataSource(_ viewPager: BmoViewPager, viewControllerForPageAt page: Int) -> UIViewController {
        if(page == 0) {
            let vc = UIViewController()
            vc.view.backgroundColor = .green
            return vc
        }
        else if(page == 1) {
            let vc = UIViewController()
            vc.view.backgroundColor = .blue
            return vc
        }
        else if(page == 2) {
            let vc = UIViewController()
            vc.view.backgroundColor = .orange
            return vc
        }
        
        
        return UIViewController()
    }
    
    func bmoViewPagerDataSourceNaviagtionBarItemTitle(_ viewPager: BmoViewPager, navigationBar: BmoViewPagerNavigationBar, forPageListAt page: Int) -> String? {
        return "" //Default \(page)"
    }
    
    func bmoViewPagerDataSourceNaviagtionBarItemSize(_ viewPager: BmoViewPager, navigationBar: BmoViewPagerNavigationBar, forPageListAt page: Int) -> CGSize {
        return CGSize.zero
    }
    
    func bmoViewPagerDataSourceNaviagtionBarItemNormalBackgroundView(_ viewPager: BmoViewPager, navigationBar: BmoViewPagerNavigationBar, forPageListAt page: Int) -> UIView? {
        
        return nil
        
    }
    
    func bmoViewPagerDataSourceNaviagtionBarItemHighlightedBackgroundView(_ viewPager: BmoViewPager, navigationBar: BmoViewPagerNavigationBar, forPageListAt page: Int) -> UIView? {
        
        return nil
        
    }
    
    
    
}
*/
