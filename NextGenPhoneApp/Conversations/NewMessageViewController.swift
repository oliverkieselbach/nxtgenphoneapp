//
//  NewMessageViewController.swift
//  NextGenPhoneApp
//
//  Created by Kieselbach, Oliver on 11.06.19.
//  Copyright © 2019 Kieselbach, Oliver. All rights reserved.
//

import UIKit
import SPFakeBar
import NextGenPhoneFramework
import Lottie
import FirebaseFunctions
import Eureka
import BonsaiController

class NewMessageViewController: UIViewController {
    
    var message: CKMessage! {
        didSet {
            
            self.messageCardHeader.message = message
            
        }
    }

    
    var recipient: CKContact! {
        didSet {
            
            
        }
    }
        
    var intent: CKIntent! {
        didSet {
            self.createForm(formDocumentId: intent.formDocumentId)
        }
    }
    
    
    var formVC: MessageFormViewController = MessageFormViewController()

    var messageCardHeader: ConversationMessageViewLarge! = {
        
        let v = ConversationMessageViewLarge(frame: .zero)
        v.translatesAutoresizingMaskIntoConstraints = false
        
        v.heightAnchor.constraint(equalToConstant: 55).isActive = true
        v.layer.cornerRadius = 55/2
        
        return v
        
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = UIColor.white
        
        self.view.addSubview(self.messageCardHeader)
        self.messageCardHeader.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 15).isActive = true
        self.messageCardHeader.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0).isActive = true
        self.messageCardHeader.attachmentButton.addTarget(self, action: #selector(self.playMessageAction), for: .touchDown)

        self.addChild(self.formVC)
        self.view.addSubview(self.formVC.view)
        self.formVC.view.translatesAutoresizingMaskIntoConstraints = false
        self.formVC.view.topAnchor.constraint(equalTo: self.messageCardHeader.bottomAnchor, constant: 10).isActive = true
        self.formVC.view.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 15).isActive = true
        self.formVC.view.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -15).isActive = true
        self.formVC.view.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: 0).isActive = true
        self.formVC.view.alpha = 1
        self.formVC.view.backgroundColor = .white
        self.formVC.tableView.backgroundColor = .white
        
        self.formVC.tableView.separatorStyle = .none
        
        
    }
    
    var presentedVC: AnyClass!
    
    @objc func playMessageAction() {
        
        if(message.messageType! == .voice) {
            let vc = VoiceMessagePlayerViewController()
            vc.message = message
            vc.transitioningDelegate = self
            vc.modalPresentationStyle = .custom
            self.presentedVC = vc.classForCoder
            
            
            self.present(vc, animated: true) {
                vc.playPauseAction()
            }
            
        }
        else if(message.messageType! == .video) {
            let vc = VideoMessagePlayerViewController()
            vc.message = message
            vc.transitioningDelegate = self
            vc.modalPresentationStyle = .custom
            self.presentedVC = vc.classForCoder
            
            
            self.present(vc, animated: true) {
                vc.playPauseAction()
            }
        }
        

        
    }
    
    @objc func sendMessageAction() {
        
    }
    
    /*
    func executeActionFunction() {
        
        let functions = Functions.functions()
        functions.httpsCallable(self.action.functionName).call(["appUserId": ConversationKit.shared.appUser.id, "messageId": self.message == nil ? "none" : self.message.id, "conversationId": self.conversation.id, "formDocumentId": self.message == nil ? "none" : self.message.topicDetails.formDocumentId]) { (result, error) in
            
            if let error = error as NSError? {
                if error.domain == FunctionsErrorDomain {
                    let code = FunctionsErrorCode(rawValue: error.code)
                    let message = error.localizedDescription
                    let details = error.userInfo[FunctionsErrorDetailsKey]
                    print(message)
                }
            }
            
            let data = result?.data as! [String: Any]
            
            let formDocumentId = data["formDocumentId"] == nil ? "" : data["formDocumentId"] as! String
            self.createForm(formDocumentId: formDocumentId)
            
            
            
        }

        
    }
    */
    
    func createForm(formDocumentId: String) {
        
        if(formDocumentId != "") {
            let formDocRef = ConversationKit.shared.db.collection("conversations").document(self.message.conversationId).collection("forms").document(formDocumentId).getDocument { (documentSnapshot, error) in
                
                guard let document = documentSnapshot else {
                    print(error?.localizedDescription ?? "Error reading form data")
                    return
                }
                
                let data = document.data()
                var formParameter: [FormParameter] = []
                var formHeader = FormHeader(title: "", description: "")
                if(data != nil && data!["form"] != nil) {
                    let form = data!["form"] as! [String:Any]
                    let params = form["parameter"] as! [[String:Any]]
                    for param in params {
                        
                        let value = param as! [String:Any]
                        let id = value["id"] == nil ? "" : value["id"] as! String
                        let name = value["name"] == nil ? "" : value["name"] as! String
                        let type = value["type"] == nil ? FormParamType.string : FormParamType(rawValue: value["type"] as! String)
                        
                        let formParam = FormParameter(id: id, name: name, type: type)
                        formParameter.append(formParam)
                    }
                    formHeader.title = form["title"] == nil ? "" : form["title"] as! String
                    formHeader.description = form["description"] == nil ? "" : form["description"] as! String
                }
                
                self.formVC.form = FormFactory.createForm(form: self.formVC.form, formParamater: formParameter, formHeader: formHeader, formDocument: document)
                
            }

        }
        
        
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension NewMessageViewController: BonsaiControllerDelegate {
    
    public func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        
        if(presented.classForCoder == VoiceMessagePlayerViewController.classForCoder() || presented.classForCoder == VideoMessagePlayerViewController.classForCoder()) {
            return BonsaiController(fromDirection: .bottom, blurEffectStyle: .dark, presentedViewController: presented, delegate: self)
        }
        else {
            return BonsaiController(fromView: self.view, blurEffectStyle: .dark,  presentedViewController: presented, delegate: self)
        }
    }
    
    public func frameOfPresentedView(in containerViewFrame: CGRect) -> CGRect {
        
        if(self.presentedVC == VoiceMessagePlayerViewController.classForCoder() || self.presentedVC == VideoMessagePlayerViewController.classForCoder()) {
            return CGRect(origin: CGPoint(x: 0, y: containerViewFrame.size.height * 0.3), size: CGSize(width: containerViewFrame.width, height: containerViewFrame.height * 0.7))
        }
        else {
            return CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: containerViewFrame.width, height: containerViewFrame.height))
        }
        
    }
}

