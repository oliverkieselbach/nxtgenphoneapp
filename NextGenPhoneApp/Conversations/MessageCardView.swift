//
//  MessageCardView.swift
//  NextGenPhoneApp
//
//  Created by Kieselbach, Oliver on 29.05.19.
//  Copyright © 2019 Kieselbach, Oliver. All rights reserved.
//

import UIKit
import NextGenPhoneFramework
import SwiftRichString
import Eureka
import FirebaseFirestore

class MessageFormViewController: FormViewController {
    
    
    var changeListener: ListenerRegistration!
    var message: CKMessage! {
        didSet {
            self.conversationId = message.conversationId
            self.createForm()
        }
    }
    
    var intent: CKIntent! {
        didSet {
            self.createForm(formId: intent.formDocumentId)
        }
    }
    
    var conversationId: String!
    
    deinit {
        self.changeListener.remove()
    }

    func createForm() {
        let formId = self.message.topicActionDetails.name == "" ? self.message.topicDetails.formDocumentId : self.message.topicActionDetails.formDocumentId
        self.createForm(formId: formId)
    }
    
    func createForm(formId: String) {
        
        //self.view.backgroundColor = UIColor(red: 249/255, green: 249/255, blue: 249/255, alpha: 1)
        self.tableView.backgroundColor = .white //UIColor(red: 249/255, green: 249/255, blue: 249/255, alpha: 1)
        
        
        let formDocRef = ConversationKit.shared.db.collection("conversations").document(self.conversationId).collection("forms").document(formId).getDocument { (documentSnapshot, error) in
            
            guard let document = documentSnapshot else {
                print(error?.localizedDescription ?? "Error reading form data")
                return
            }
            
            let data = document.data()
            var formParameter: [FormParameter] = []
            var formHeader = FormHeader(title: "", description: "")
            if(data != nil && data!["form"] != nil) {
                let form = data!["form"] as! [String:Any]
                let params = form["parameter"] as! [[String:Any]]
                for param in params {
                    
                    let value = param as! [String:Any]
                    let id = value["id"] == nil ? "" : value["id"] as! String
                    let name = value["name"] == nil ? "" : value["name"] as! String
                    let type = value["type"] == nil ? FormParamType.string : FormParamType(rawValue: value["type"] as! String)
                    
                    let formParam = FormParameter(id: id, name: name, type: type)
                    formParameter.append(formParam)
                }
                formHeader.title = form["title"] == nil ? "" : form["title"] as! String
                formHeader.description = form["description"] == nil ? "" : form["description"] as! String
            }
            
            
            self.form = FormFactory.createForm(form: self.form, formParamater: formParameter, formHeader: formHeader, formDocument: document)
            
            self.addChangeListener(formId: formId, formParameter: formParameter)
        }
        
        
        //self.tableView.tableFooterView = footer
        
        
    }
    
    var initialChange: Bool = true
    
    func addChangeListener(formId: String, formParameter:[FormParameter]) {
        
        self.changeListener = ConversationKit.shared.db.collection("conversations").document(self.conversationId).collection("forms").document(formId).addSnapshotListener({ (documentSnapshot, error) in

            let documentData = documentSnapshot?.data()
            if(documentData == nil) {
                return
            }

            if(self.initialChange == false) {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "communicator.call.intentform.changed"), object: nil, userInfo: [:])
            }
            self.initialChange = false
            
            for i in 0..<formParameter.count {
                
                let parameter = formParameter[i]
                let row = self.form.rowBy(tag: parameter.name)
                if(row != nil) {
                    switch parameter.type! {
                    case .date:
                        if(documentData != nil) {
                            if(documentData![parameter.id] != nil) {
                                (row as! DateInlineRow).value = Date(timestamp: documentData![parameter.id] as! Timestamp)
                            }
                        }
                    case .time:
                        if(documentData != nil) {
                            if(documentData![parameter.id] != nil) {
                                (row as! TimeInlineRow).value = Date(timestamp: documentData![parameter.id] as! Timestamp)
                            }
                        }
                    case .string:
                        if(documentData != nil) {
                            if(documentData![parameter.id] != nil) {
                                (row as! TextRow).value = documentData![parameter.id] as! String
                            }
                        }
                    case .bool:
                        if(documentData != nil) {
                            if(documentData![parameter.id] != nil) {
                                (row as! SwitchRow).value = documentData![parameter.id] as! Bool
                            }
                        }
                    }

                }
                
                
            }
            self.tableView.reloadData()

            
            
        })
        
    }
    
    override func insertAnimation(forSections sections: [Section]) -> UITableView.RowAnimation {
        return .none
    }
    
    override func insertAnimation(forRows rows: [BaseRow]) -> UITableView.RowAnimation {
        return .none
    }


    
}

class MessageCardHeader: UIView {
    
    var message: CKMessage! {
        didSet {
            avatar.contactWithNoStatus = message.senderDetails
            
            let intentColors = self.message.topicActionDetails.id != "" ? self.message.topicActionDetails.getColor() : self.message.topicDetails.getColor()
            self.intent.intent = self.message.topicActionDetails.id != "" ? self.message.topicActionDetails : self.message.topicDetails
            self.intent.layoutIfNeeded()
            
            self.dateLabel.text = "Sent \(self.message.date.timeAgoSinceNow)"
            
 
        }
    }
    
    var avatar: CKContactView = {
        
        let v = CKContactView(frame: .zero)
        v.translatesAutoresizingMaskIntoConstraints = false
        v.heightAnchor.constraint(equalToConstant: 60).isActive = true
        v.widthAnchor.constraint(equalToConstant: 60).isActive = true
        
        return v
        
    }()
    
    var intent: CKIntentView = {

        let v = CKIntentView(frame: .zero)
        v.translatesAutoresizingMaskIntoConstraints = false
        v.heightAnchor.constraint(equalToConstant: 60).isActive = true
        v.layer.cornerRadius = 30
        return v
    }()

    var dateLabel: UILabel = {

        let label = UILabel(frame: .zero)
        label.textColor = .gray
        label.font = UIFont.systemFont(ofSize: 15, weight: .regular)
        label.textAlignment = .center
        
        return label
        
    }()
    

    var lineView: UIView!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addSubview(self.avatar)
        self.addSubview(self.intent)
        self.addSubview(self.dateLabel)

        
        self.avatar.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        self.avatar.topAnchor.constraint(equalTo: self.topAnchor, constant: 25).isActive = true

        self.intent.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        self.intent.topAnchor.constraint(equalTo: self.avatar.bottomAnchor, constant: 5).isActive = true
        
        self.dateLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        self.dateLabel.topAnchor.constraint(equalTo: self.intent.bottomAnchor, constant: 5).isActive = true
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
    }
}

class MessageCardView: CardView {
    
    var contentView: UIView!
    var message: CKMessage! {
        didSet {
            
            self.messageCardHeader.message = message
            self.messageFormViewController.message = message
            
            self.contentView.backgroundColor = .clear //.white //colors.backgroundColor
            self.tintColor = .black

            self.dateLabel.text = "\(message.type == CKConversationType.outbound ? "Sent" : "Received") \(message.date.timeAgoSinceNow)"

            self.setNeedsUpdateConstraints()
        }
    }
    
    var dateLabel: UILabel = {
        
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .lightGray
        label.font = UIFont.systemFont(ofSize: 12, weight: .semibold)
        label.textAlignment = .right
        
        return label
        
    }()

    
    var messageCardHeader: ConversationMessageViewLarge! = {

        let v = ConversationMessageViewLarge(frame: .zero)
        v.translatesAutoresizingMaskIntoConstraints = false
        
        v.heightAnchor.constraint(equalToConstant: 55).isActive = true
        v.layer.cornerRadius = 55/2

        return v
        
    }()

    var respondButton: UIButton! = {

        let button = UIButton(type: .custom)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Respond", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: .regular)
        button.heightAnchor.constraint(equalToConstant: 40).isActive = true
        button.backgroundColor = UIButton.appearance().tintColor
        button.layer.cornerRadius = 8
        button.alpha = 0
        return button
        
        
    }()
    
    var messageFormViewController: MessageFormViewController! = MessageFormViewController()
    
    var respondClosure: ((CKMessage) -> Void)?
    var playMessageAction: ((CKMessage) -> Void)?
    
    override var presented: Bool {
        didSet {
            self.messageFormViewController.view.alpha = presented == true ? 1 : 0
            self.contentView.backgroundColor = presented == true ? UIColor.white.withAlphaComponent(1) : UIColor.clear
            self.respondButton.alpha = presented == true ? 1 : 0
        }
    }


    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    
    override init(frame: CGRect) {

        super.init(frame: frame)

        self.contentView = UIView(frame: .zero)
        self.clipsToBounds = false
        self.addSubview(self.contentView)
        self.contentView.translatesAutoresizingMaskIntoConstraints = false

        
        self.contentView.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
        self.contentView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 10).isActive = true
        self.contentView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -10).isActive = true
        self.contentView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
        self.contentView.layer.cornerRadius = 10
        
        self.contentView.backgroundColor = .clear
        self.backgroundColor = .clear
        
        self.contentView.addSubview(self.dateLabel)
        self.contentView.addSubview(self.messageCardHeader)
        //self.contentView.addSubview(self.respondButton)

        self.messageCardHeader.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 15).isActive = true
        self.messageCardHeader.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        self.messageCardHeader.attachmentButton.addTarget(self, action: #selector(self.playAttachmentAction), for: .touchDown)

        self.dateLabel.topAnchor.constraint(equalTo: self.messageCardHeader.bottomAnchor, constant: 5).isActive = true
        self.dateLabel.leftAnchor.constraint(equalTo: self.contentView.leftAnchor, constant: 15).isActive = true
        self.dateLabel.rightAnchor.constraint(equalTo: self.messageCardHeader.rightAnchor, constant: 0).isActive = true

        let stackView = UIStackView(arrangedSubviews: [self.respondButton])
        self.contentView.addSubview(stackView)
        stackView.axis = .horizontal
        stackView.alignment = .center
        stackView.distribution = .fill
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.widthAnchor.constraint(equalTo: self.contentView.widthAnchor, multiplier: 0.5).isActive = true
        stackView.heightAnchor.constraint(equalTo: self.respondButton.heightAnchor, constant: 0).isActive = true
        
        
        stackView.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -10).isActive = true
        stackView.centerXAnchor.constraint(equalTo: self.contentView.centerXAnchor, constant: 0).isActive = true

        self.respondButton.addTarget(self, action: #selector(self.respondAction), for: .touchDown)
        
        self.contentView.insertSubview(self.messageFormViewController.view, belowSubview: self.messageCardHeader)
        self.messageFormViewController.view.tag = 999
        self.messageFormViewController.view.translatesAutoresizingMaskIntoConstraints = false
        self.messageFormViewController.view.topAnchor.constraint(equalTo: self.dateLabel.bottomAnchor, constant: 10).isActive = true
        self.messageFormViewController.view.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 15).isActive = true
        self.messageFormViewController.view.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -15).isActive = true
        self.messageFormViewController.view.bottomAnchor.constraint(equalTo: stackView.topAnchor, constant: -15).isActive = true
        self.messageFormViewController.view.alpha = 0
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func playAttachmentAction() {
        if(self.playMessageAction != nil) {
            self.playMessageAction!(self.message)
        }
    }

    @objc func respondAction() {
        
        if(self.respondClosure != nil) {
            self.respondClosure!(self.message)
        }
        
    }
    
}
