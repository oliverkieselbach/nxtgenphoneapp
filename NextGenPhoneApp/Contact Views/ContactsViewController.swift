//
//  ContactsViewController.swift
//  NextGenPhoneApp
//
//  Created by Oliver Kieselbach on 28.08.18.
//  Copyright © 2018 Kieselbach, Oliver. All rights reserved.
//

import UIKit
import FirebaseStorage
import FirebaseFirestore
import FirebaseFunctions
import FirebaseUI
import LetterAvatarKit
import Contacts
import NextGenPhoneFramework



class ContactCell: UICollectionViewCell {
    
    var contact: CKContact! {
        didSet {
            
            self.name.text = contact.name
            
            //applyGradientToView(view: self.contentView, gradientStyle: contact.backgroundColorStyle)
            self.backgroundImageView.sd_setImage(with: ConversationKit.shared.storage.reference(withPath: self.contact.profileImageRef), placeholderImage: self.backgroundImageView.image) { (image, error, cacheType, ref) in
                self.backgroundImageView.isHidden = false
            }
        }
    }
    
    var backgroundImageView: UIImageView = {
        
        let imageView = UIImageView(frame: .zero)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill
        imageView.isHidden = true
        
        return imageView
    }()
    
    
    var name: UILabel = {
        let label = UILabel()
        
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .white
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.semibold)
        label.numberOfLines = 2
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let overlay = UIView(frame: .zero)
        overlay.translatesAutoresizingMaskIntoConstraints = false
        overlay.backgroundColor = UIColor.black.withAlphaComponent(0.03)
        
        self.contentView.addSubview(backgroundImageView)
        self.contentView.addSubview(overlay)
        self.contentView.addSubview(self.name)
        
        self.backgroundImageView.topAnchor.constraint(equalTo: self.contentView.topAnchor).isActive = true
        self.backgroundImageView.leftAnchor.constraint(equalTo: self.contentView.leftAnchor).isActive = true
        self.backgroundImageView.rightAnchor.constraint(equalTo: self.contentView.rightAnchor).isActive = true
        self.backgroundImageView.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor).isActive = true
        
        overlay.topAnchor.constraint(equalTo: self.contentView.topAnchor).isActive = true
        overlay.leftAnchor.constraint(equalTo: self.contentView.leftAnchor).isActive = true
        overlay.rightAnchor.constraint(equalTo: self.contentView.rightAnchor).isActive = true
        overlay.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor).isActive = true
        
        self.name.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -10).isActive = true
        self.name.leftAnchor.constraint(equalTo: self.contentView.leftAnchor, constant: 10).isActive = true
        self.name.rightAnchor.constraint(equalTo: self.contentView.rightAnchor, constant: -10).isActive = true
        
        self.contentView.layer.cornerRadius = 5
        self.clipsToBounds = true
        self.contentView.layer.masksToBounds = true
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}


class ContactsViewController: UIViewController {
    
    var collectionView: UICollectionView!
    
    var db: Firestore! = ConversationKit.shared.db
    var dataSource: FUIFirestoreCollectionViewDataSource!
    var collectionEntries: [CKContact]! = []
    var dataLoaded: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = .white //UIColor(displayP3Red: 230/255, green: 230/255, blue: 230/255, alpha: 1)
        
        let snCollectionViewLayout = SNCollectionViewLayout()
        snCollectionViewLayout.fixedDivisionCount = 3 // Columns for .vertical, rows for .horizontal
        snCollectionViewLayout.delegate = self
        snCollectionViewLayout.scrollDirection = .vertical
        snCollectionViewLayout.itemSpacing = 6
        
        let pinterestLayout = PinterestLayout()
        
        
        self.collectionView = UICollectionView(frame: .zero, collectionViewLayout: snCollectionViewLayout)
        self.collectionView.translatesAutoresizingMaskIntoConstraints = false
        self.collectionView.backgroundColor = .white
        self.view.addSubview(self.collectionView)
        self.collectionView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 20).isActive = true
        self.collectionView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 0).isActive = true
        self.collectionView.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 0).isActive = true
        self.collectionView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: 0).isActive = true
        self.collectionView.register(ContactCell.classForCoder(), forCellWithReuseIdentifier: "contactCell")
        self.collectionView.contentInset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
        self.collectionView.collectionViewLayout = pinterestLayout
        pinterestLayout.delegate = self

        // Do any additional setup after loading the view.
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.loadData()
    }
    
    /*
    func bindCollectionViewQuery(query: Query) {
        
        self.dataSource = nil
        //self.collectionView.reloadData()
        
        self.dataSource = collectionView.bind(toFirestoreQuery: query, populateCell: { (collectionView, indexPath, documentSnapshot) -> UICollectionViewCell in
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "contactCell", for: indexPath) as! ContactCell
            let data = documentSnapshot.data()
            
            if(data != nil) {
                let contact = Person(id: documentSnapshot.documentID, dictionary: data!)
                cell.contact = contact
                print("Contact = "+contact!.name)
            }
            
            return cell
            
        })

    }
 */
    
    func loadData() {

        if(self.dataLoaded) { return }
        //Contacts Query
        db.collection(ConversationKit.shared.currentUser.uid).document("data").collection("contacts").order(by: "lastContact", descending: false).getDocuments(completion: { (querySnapshot, error) in
            
            print("Load Contacts "+String((querySnapshot?.documentChanges.count)!))
            guard let documents = querySnapshot?.documents else {
                print("error loading contacts")
                return
            }
            
            self.collectionEntries = []
            for document in documents {
                let person = CKContact(id: document.documentID, dictionary: document.data())
                self.collectionEntries.append(person!)
            }
            self.collectionView.reloadData()

        })

        self.dataLoaded = true

    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func showContact(contact: CKContact) {
        CKContactDetailsViewController.open(contact: contact, modal: false)
    }
    

}

//MARK: - PINTEREST LAYOUT DELEGATE
extension ContactsViewController : PinterestLayoutDelegate {
    
    // 1. Returns the photo height
    func collectionView(_ collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath:IndexPath) -> CGFloat {
        return 100
    }
    
}


extension ContactsViewController: UICollectionViewDataSource, UICollectionViewDelegate, SNCollectionViewLayoutDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.collectionEntries.count
    }
    

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "contactCell", for: indexPath) as! ContactCell
        cell.contact = self.collectionEntries[indexPath.item]
        
        return cell

    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let contact: CKContact! = self.collectionEntries[indexPath.item]
        AppDelegateAccessor.showContact(contact: contact, modal: false)
    }
    
    func scaleForItem(inCollectionView collectionView: UICollectionView, withLayout layout: UICollectionViewLayout, atIndexPath indexPath: IndexPath) -> UInt {

        let contact = self.collectionEntries[indexPath.item]
        return contact.status == .available ? 2 : 1

    }

}





