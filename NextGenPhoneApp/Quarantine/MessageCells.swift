//
//  MessageCells.swift
//  NextGenPhoneApp
//
//  Created by Kieselbach, Oliver on 07.05.19.
//  Copyright © 2019 Kieselbach, Oliver. All rights reserved.
//

import Foundation
import UIKit
import NextGenPhoneFramework
import AVKit

class MessageContentView: UIView {
    
    var message: CKMessage! {
        didSet {
            
            //if(oldValue == nil || message.id != oldValue.id) {
            for view in self.subviews {
                view.removeFromSuperview()
            }
            if(self.fixedContentHeightConstraint != nil) {
                self.removeConstraint(fixedContentHeightConstraint)
            }
            
            if(message != nil) {
                switch message.messageType! {
                case .text:
                    self.addTextMessageContent()
                case .video:
                    self.addVideoMessageContent()
                case .voice:
                    self.addVoiceMessageContent()
                case .call:
                    self.addCallMessageContent()
                case .missedCall:
                    self.addMissedCallMessageContent()
                }
            }
            /*
             }
             else {
             if(oldValue != nil && message.isRead != oldValue.isRead) {
             self.updateIsRead(isRead: message.isRead)
             }
             }
             */
            
            
        }
    }
    
    var messageText: UILabel = {
        
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: 15, weight: .regular)
        label.numberOfLines = 0
        label.backgroundColor = .clear
        label.layer.cornerRadius = 10
        label.contentMode = .topLeft
        return label
        
    }()
    
    var defaultHeightConstraint: NSLayoutConstraint!
    var fixedContentHeightConstraint: NSLayoutConstraint!
    
    var dateLabel: UILabel = {
        
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .gray
        label.font = UIFont.systemFont(ofSize: 13, weight: .regular)
        label.textAlignment = .right
        
        return label
    }()
    
    var isReadIcon: UIImageView = {
        
        let imageView = UIImageView(frame: .zero)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        
        return imageView
        
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        defaultHeightConstraint = self.heightAnchor.constraint(equalToConstant: 5)
        defaultHeightConstraint.isActive = false
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    func addTextMessageContent() {
        self.defaultHeightConstraint.isActive = false
        
        self.messageText.font = UIFont.systemFont(ofSize: 15, weight: .regular)
        self.messageText.textColor = .black
        
        self.addSubview(self.messageText)
        self.backgroundColor = UIColor(0xF5F5F5)
        
        self.addSubview(self.dateLabel)
        
        if(message.type == .outbound) {
            self.addSubview(self.isReadIcon)
            self.isReadIcon.tintColor = message.isRead ? UIColor.actionColor(color: .blue) : .lightGray
            self.isReadIcon.heightAnchor.constraint(equalToConstant: 14).isActive = true
            self.isReadIcon.widthAnchor.constraint(equalToConstant: 14).isActive = true
            self.isReadIcon.image = UIImage(imageLiteralResourceName: "isRead").withRenderingMode(.alwaysTemplate)
            self.isReadIcon.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -10).isActive = true
            self.isReadIcon.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -10).isActive = true
        }
        
        self.dateLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -10).isActive = true
        self.dateLabel.rightAnchor.constraint(equalTo: message.type == .outbound ? self.isReadIcon.leftAnchor : self.rightAnchor, constant: -10).isActive = true
        
        self.messageText.topAnchor.constraint(equalTo: self.topAnchor, constant: 10).isActive = true
        self.messageText.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 10).isActive = true
        self.messageText.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -15).isActive = true
        
        self.messageText.bottomAnchor.constraint(equalTo: self.dateLabel.topAnchor,constant: -10).isActive = true
        
        
        self.messageText.text = message.text
        self.dateLabel.text = message.date.timeAgoSinceNow
    }
    
    var playerLayer: AVPlayerLayer!
    
    func addVideoMessageContent() {
        self.defaultHeightConstraint.isActive = false
        
        let button = UIButton(type: .custom)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1)
        button.tintColor = .black
        button.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        button.setImage(UIImage(imageLiteralResourceName: "play"), for: .normal)
        button.heightAnchor.constraint(equalToConstant: 44).isActive = true
        button.widthAnchor.constraint(equalToConstant: 44).isActive = true
        button.layer.cornerRadius = 22
        button.addTarget(self, action: #selector(self.playVideoAction), for: .touchDown)
        
        let thumbnailImageView = UIImageView(frame: .zero)
        let overlay = UIView(frame: .zero)
        self.addSubview(thumbnailImageView)
        self.addSubview(overlay)
        self.addSubview(button)
        self.addSubview(self.dateLabel)
        
        if(message.type == .outbound) {
            self.addSubview(self.isReadIcon)
            self.isReadIcon.tintColor = message.isRead ? UIColor.actionColor(color: .blue) : .white
            self.isReadIcon.heightAnchor.constraint(equalToConstant: 14).isActive = true
            self.isReadIcon.widthAnchor.constraint(equalToConstant: 14).isActive = true
            self.isReadIcon.image = UIImage(imageLiteralResourceName: "isRead").withRenderingMode(.alwaysTemplate)
            self.isReadIcon.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -10).isActive = true
            self.isReadIcon.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -10).isActive = true
        }
        
        self.dateLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -10).isActive = true
        self.dateLabel.rightAnchor.constraint(equalTo: message.type == .outbound ? self.isReadIcon.leftAnchor : self.rightAnchor, constant: -10).isActive = true
        self.dateLabel.textColor = .white
        self.dateLabel.text = message.date.timeAgoSinceNow
        
        thumbnailImageView.translatesAutoresizingMaskIntoConstraints = false
        thumbnailImageView.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
        thumbnailImageView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 0).isActive = true
        thumbnailImageView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 0).isActive = true
        thumbnailImageView.clipsToBounds = true
        thumbnailImageView.contentMode = .scaleAspectFill
        thumbnailImageView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant:0).isActive = true
        
        overlay.translatesAutoresizingMaskIntoConstraints = false
        overlay.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
        overlay.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 0).isActive = true
        overlay.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 0).isActive = true
        overlay.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant:0).isActive = true
        overlay.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        
        button.centerXAnchor.constraint(equalTo: thumbnailImageView.centerXAnchor).isActive = true
        button.centerYAnchor.constraint(equalTo: thumbnailImageView.centerYAnchor).isActive = true
        
        thumbnailImageView.heightAnchor.constraint(equalToConstant: 145).isActive = true
        
        
        thumbnailImageView.sd_setImage(with: ConversationKit.shared.storage.reference(withPath: self.message.mediaRef!+"_thumbnail"))
        /*
         self.messageContent.heightAnchor.constraint(equalToConstant: 100).isActive = true
         playerLayer = AVPlayerLayer()
         self.playerLayer.videoGravity = .resizeAspectFill
         self.messageContent.layer.addSublayer(self.playerLayer)
         
         
         let url = AppDelegateAccessor.storage.reference(withPath: message.mediaRef!).downloadURL { (url, error) in
         let playerItem:AVPlayerItem = AVPlayerItem(url: url!)
         
         let player = AVPlayer(playerItem: playerItem)
         
         self.playerLayer.player = player
         }
         */
        
    }
    
    var mediaURL: URL?
    var playButton: UIButton?
    var presenterVC: UIViewController?
    var audioSlider: UISlider = {
        
        let slider = UISlider(frame: .zero)
        slider.translatesAutoresizingMaskIntoConstraints = false
        slider.minimumTrackTintColor = .gray
        slider.maximumTrackTintColor = .gray
        slider.thumbTintColor = .black
        
        return slider
    }()
    
    func addVoiceMessageContent() {
        self.defaultHeightConstraint.isActive = false
        
        
        
        //BBDEFB
        self.backgroundColor = UIColor(0xF5F5F5) //UIColor(red: 235/255, green: 235/255, blue: 235/255, alpha: 1)
        
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: 13)
        
        self.playButton = UIButton(type: .custom)
        self.playButton!.translatesAutoresizingMaskIntoConstraints = false
        self.playButton?.backgroundColor = UIColor(0xE0E0E0)
        //self.playButton!.backgroundColor = UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1)
        self.playButton!.tintColor = .black
        self.playButton!.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        self.playButton!.setImage(UIImage(imageLiteralResourceName: "play"), for: .normal)
        self.playButton!.setImage(UIImage(imageLiteralResourceName: "pause"), for: .selected)
        self.playButton!.heightAnchor.constraint(equalToConstant: 44).isActive = true
        self.playButton!.widthAnchor.constraint(equalToConstant: 44).isActive = true
        self.playButton!.layer.cornerRadius = 22
        self.playButton!.tag = 99
        
        self.addSubview(label)
        self.addSubview(self.playButton!)
        self.addSubview(self.audioSlider)
        
        self.addSubview(self.dateLabel)
        if(message.type == .outbound) {
            self.addSubview(self.isReadIcon)
            self.isReadIcon.tintColor = message.isRead ? UIColor.actionColor(color: .blue) : .lightGray
            self.isReadIcon.heightAnchor.constraint(equalToConstant: 14).isActive = true
            self.isReadIcon.widthAnchor.constraint(equalToConstant: 14).isActive = true
            self.isReadIcon.image = UIImage(imageLiteralResourceName: "isRead").withRenderingMode(.alwaysTemplate)
            self.isReadIcon.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -10).isActive = true
            self.isReadIcon.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -10).isActive = true
        }
        
        self.dateLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -10).isActive = true
        self.dateLabel.rightAnchor.constraint(equalTo: message.type == .outbound ? self.isReadIcon.leftAnchor : self.rightAnchor, constant: -10).isActive = true
        
        self.playButton!.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        self.playButton!.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 10).isActive = true
        self.playButton!.addTarget(self, action: #selector(self.playAudioAction), for: .touchDown)
        
        label.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -10).isActive = true
        label.centerYAnchor.constraint(equalTo: self.audioSlider.centerYAnchor, constant: 0).isActive = true
        
        self.audioSlider.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        self.audioSlider.heightAnchor.constraint(equalToConstant: 22).isActive = true
        self.audioSlider.leftAnchor.constraint(equalTo: self.playButton!.rightAnchor, constant: 10).isActive = true
        self.audioSlider.rightAnchor.constraint(equalTo: label.leftAnchor, constant: -10).isActive = true
        let thumbImage = UIImage(imageLiteralResourceName: "circle_filled").withRenderingMode(.alwaysTemplate).scaleImageToFitSize(size: CGSize(width: 10, height: 10))
        self.audioSlider.setThumbImage(thumbImage, for: .normal)
        self.audioSlider.setThumbImage(thumbImage, for: .highlighted)
        self.audioSlider.addTarget(self, action: #selector(audioSliderValueChanged(slider:event:)), for: .valueChanged)
        
        self.fixedContentHeightConstraint = self.heightAnchor.constraint(equalToConstant: 80)
        self.fixedContentHeightConstraint.isActive = true
        
        self.dateLabel.text = message.date.timeAgoSinceNow
        
        let totalSeconds = Int(message.duration)
        let seconds = totalSeconds % 60
        let mediaDuration = String(format:"%2i sec", seconds)
        label.text = mediaDuration
        
        
    }
    
    func addCallMessageContent() {
        self.defaultHeightConstraint.isActive = false
        
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: 15)
        
        let icon = UIImageView(frame: .zero)
        icon.translatesAutoresizingMaskIntoConstraints = false
        icon.heightAnchor.constraint(equalToConstant: 20).isActive = true
        icon.widthAnchor.constraint(equalToConstant: 20).isActive = true
        icon.tintColor = .black
        icon.image = UIImage(imageLiteralResourceName: "call_filled").withRenderingMode(.alwaysTemplate)
        
        self.messageText.font = UIFont.systemFont(ofSize: 15, weight: .semibold)
        self.messageText.textColor = .black
        
        self.addSubview(self.messageText)
        self.addSubview(label)
        self.addSubview(icon)
        self.backgroundColor = UIColor(0xF5F5F5)
        
        self.addSubview(self.dateLabel)
        
        self.dateLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        self.dateLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -10).isActive = true
        
        
        icon.centerYAnchor.constraint(equalTo: self.messageText.centerYAnchor, constant: 0).isActive = true
        icon.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 10).isActive = true
        
        self.messageText.topAnchor.constraint(equalTo: self.topAnchor, constant: 10).isActive = true
        self.messageText.leftAnchor.constraint(equalTo: icon.rightAnchor, constant: 10).isActive = true
        self.messageText.bottomAnchor.constraint(equalTo: self.bottomAnchor,constant: -10).isActive = true
        self.messageText.text = "Call"
        
        label.leftAnchor.constraint(equalTo: self.messageText.rightAnchor, constant: 10).isActive = true
        label.centerYAnchor.constraint(equalTo: self.messageText.centerYAnchor, constant: 0).isActive = true
        
        let totalSeconds = Int(message.duration)
        let minutes = totalSeconds / 60
        let mediaDuration = String(format:"%2i min", minutes)
        label.text = mediaDuration
        self.dateLabel.text = message.date.timeAgoSinceNow
        
        //self.heightAnchor.constraint(equalToConstant: 60).isActive = true
    }
    
    func addMissedCallMessageContent() {
        self.defaultHeightConstraint.isActive = false
        
        let icon = UIImageView(frame: .zero)
        icon.translatesAutoresizingMaskIntoConstraints = false
        icon.heightAnchor.constraint(equalToConstant: 20).isActive = true
        icon.widthAnchor.constraint(equalToConstant: 20).isActive = true
        icon.tintColor = message.type == .outbound ? .black : message.isRead ? .black : UIColor(0xEF5350)
        icon.image = UIImage(imageLiteralResourceName: "call_filled").withRenderingMode(.alwaysTemplate)
        
        self.addSubview(icon)
        self.addSubview(self.messageText)
        self.backgroundColor = UIColor(0xF5F5F5)
        
        self.addSubview(self.dateLabel)
        if(message.type == .outbound) {
            self.addSubview(self.isReadIcon)
            self.isReadIcon.tintColor = message.isRead ? UIColor.actionColor(color: .blue) : .lightGray
            self.isReadIcon.heightAnchor.constraint(equalToConstant: 14).isActive = true
            self.isReadIcon.widthAnchor.constraint(equalToConstant: 14).isActive = true
            self.isReadIcon.image = UIImage(imageLiteralResourceName: "isRead").withRenderingMode(.alwaysTemplate)
            self.isReadIcon.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
            self.isReadIcon.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -10).isActive = true
        }
        
        self.dateLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        self.dateLabel.rightAnchor.constraint(equalTo: message.type == .outbound ? self.isReadIcon.leftAnchor : self.rightAnchor, constant: -10).isActive = true
        
        
        icon.centerYAnchor.constraint(equalTo: self.messageText.centerYAnchor, constant: 0).isActive = true
        icon.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 10).isActive = true
        
        self.messageText.font = UIFont.systemFont(ofSize: 15, weight: .semibold)
        self.messageText.textColor = message.type == .outbound ? .black : self.message.isRead ? .black : UIColor(0xEF5350)
        self.messageText.topAnchor.constraint(equalTo: self.topAnchor, constant: 10).isActive = true
        self.messageText.leftAnchor.constraint(equalTo: icon.rightAnchor, constant: 10).isActive = true
        self.messageText.bottomAnchor.constraint(equalTo: self.bottomAnchor,constant: -10).isActive = true
        
        self.messageText.text = "Missed Call"
        self.dateLabel.text = self.message.date.timeAgoSinceNow
        
    }
    
    func clearContent() {
        for view in self.subviews {
            view.removeFromSuperview()
        }
        self.defaultHeightConstraint.isActive = true
        //self.heightAnchor.constraint(equalToConstant: 5).isActive = true
    }
    
    @objc func audioSliderValueChanged(slider: UISlider, event: UIEvent) {
        if let touchEvent = event.allTouches?.first {
            switch touchEvent.phase {
            case .ended:
                self.audioPlayer.currentTime = TimeInterval(exactly: self.audioSlider.value)!
            default:
                break
            }
        }
        
    }
    
    @objc func playVideoAction() {
        
        ConversationKit.shared.storage.reference(withPath: message.mediaRef!).downloadURL { (url, error) in
            
            if(error == nil) {
                let player = AVPlayer(url: url!)
                let vc = AVPlayerViewController()
                vc.player = player
                self.presenterVC!.present(vc, animated: true) {
                    vc.player?.play()
                }
            }
            else {
                print(error?.localizedDescription)
            }
            
            
        }
        
        
    }
    
    var audioPlayer: AVAudioPlayer!
    var audioTimer: Timer!
    
    @objc func playAudioAction() {
        
        if(self.audioPlayer == nil) {
            let audioURL = URL(string: message.mediaURL!)
            do {
                //let session = AVAudioSession.sharedInstance()
                //try! session.setCategory(.playback, mode: .spokenAudio, options: .defaultToSpeaker)
                
                let audioData = try Data(contentsOf: audioURL!)
                self.audioPlayer = try AVAudioPlayer(data: audioData, fileTypeHint: "m4a")
                self.audioSlider.value = 0.0
                self.audioSlider.maximumValue = Float((self.audioPlayer?.duration)!)
                self.audioPlayer.delegate = self
                self.audioPlayer.prepareToPlay()
                
                
            } catch {
                print(error)
            }
        }
        
        
        if(!self.playButton!.isSelected) {
            self.playButton!.isSelected = !self.playButton!.isSelected
            self.audioPlayer.play()
            self.audioTimer = Timer.scheduledTimer(timeInterval: 0.0001, target: self, selector: #selector(self.updateAudioSlider), userInfo: nil, repeats: true)
        }
        else {
            self.playButton!.isSelected = !self.playButton!.isSelected
            self.audioTimer.invalidate()
            self.audioPlayer.pause()
        }
        
    }
    
    
    @objc func updateAudioSlider() {
        self.audioSlider.value = Float(self.audioPlayer.currentTime)
    }
    
    func updateIsRead(isRead: Bool) {
        
    }
    
    
}

extension MessageContentView: AVAudioPlayerDelegate {
    
    func audioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: Error?) {
        print(error?.localizedDescription)
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        self.audioPlayer = nil
        self.playButton?.isSelected = false
        self.audioSlider.value = 0
        self.audioTimer.invalidate()
        
    }
    
    
}

class ConversationLastMessageCell: BaseMessageCell {
    
    var conversation: CKConversation! {
        didSet {
            self.avatar.contactWithNoStatus = conversation.contactDetails
            self.contactName.text = conversation.contactDetails.name
        }
    }
    
    
    var avatar: CKContactView = {
        
        let a = CKContactView(frame: .zero)
        a.translatesAutoresizingMaskIntoConstraints = false
        a.heightAnchor.constraint(equalToConstant: 50).isActive = true
        a.widthAnchor.constraint(equalToConstant: 50).isActive = true
        
        return a
    }()
    
    var contactName: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.bold)
        label.textAlignment = .left
        
        return label
        
    }()
    
    var youLabel: UILabel = {
        
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .gray
        label.font = UIFont.systemFont(ofSize: 15, weight: .regular)
        label.numberOfLines = 0
        
        label.text = "Me"
        return label
        
    }()
    
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.contentView.addSubview(self.avatar)
        self.contentView.addSubview(self.contactName)
        self.contentView.addSubview(self.youLabel)
        
        self.intentText.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 15).isActive = true
        self.intentText.leftAnchor.constraint(equalTo: self.contentView.leftAnchor, constant: 70).isActive = true
        
        self.intentName.bottomAnchor.constraint(equalTo: self.intentText.bottomAnchor, constant: 0).isActive = true
        self.intentName.leftAnchor.constraint(equalTo: self.intentText.rightAnchor, constant: 5).isActive = true
        
        self.messageContent.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 70).isActive = true
        self.messageContent.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -15).isActive = true
        self.messageContent.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -15).isActive = true
        
        self.avatar.topAnchor.constraint(equalTo: self.intentText.bottomAnchor, constant: 5).isActive = true
        self.avatar.leftAnchor.constraint(equalTo: self.contentView.leftAnchor, constant: 15).isActive = true
        
        self.contactName.topAnchor.constraint(equalTo: self.avatar.topAnchor, constant: 3).isActive = true
        self.contactName.leftAnchor.constraint(equalTo: self.contentView.leftAnchor, constant: 70).isActive = true
        self.contactName.rightAnchor.constraint(equalTo: self.contentView.rightAnchor, constant: -15).isActive = true
        self.contactName.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        self.youLabel.topAnchor.constraint(equalTo: contactName.bottomAnchor, constant: 5).isActive = true
        self.youLabel.leftAnchor.constraint(equalTo: self.contentView.leftAnchor, constant: 70).isActive = true
        
        if(reuseIdentifier == "messageCell_outbound") {
            self.youLabel.isHidden = false
            self.messageContent.topAnchor.constraint(equalTo: youLabel.bottomAnchor, constant: 5).isActive = true
        }
        else {
            self.youLabel.isHidden = true
            self.messageContent.topAnchor.constraint(equalTo: contactName.bottomAnchor, constant: 5).isActive = true
        }
        
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

class MessageCell: BaseMessageCell {
    
    var sender: CKContactBasic! {
        didSet {
            
            self.avatar.contactWithNoStatus = sender
            
        }
    }
    
    var avatar: CKContactView = {
        
        let a = CKContactView(frame: .zero)
        a.translatesAutoresizingMaskIntoConstraints = false
        a.heightAnchor.constraint(equalToConstant: 40).isActive = true
        a.widthAnchor.constraint(equalToConstant: 40).isActive = true
        
        return a
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.contentView.addSubview(self.avatar)
        
        self.intentText.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 10).isActive = true
        self.intentText.leftAnchor.constraint(equalTo: self.contentView.leftAnchor, constant: 60).isActive = true
        
        self.intentName.bottomAnchor.constraint(equalTo: self.intentText.bottomAnchor, constant: 0).isActive = true
        self.intentName.leftAnchor.constraint(equalTo: self.intentText.rightAnchor, constant: 5).isActive = true
        
        self.avatar.topAnchor.constraint(equalTo: self.intentText.bottomAnchor, constant: 5).isActive = true
        self.avatar.leftAnchor.constraint(equalTo: self.contentView.leftAnchor, constant: 15).isActive = true
        
        self.messageContent.topAnchor.constraint(equalTo: intentText.bottomAnchor, constant: 5).isActive = true
        self.messageContent.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 60).isActive = true
        self.messageContent.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -15).isActive = true
        self.messageContent.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -15).isActive = true
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class BaseMessageCell: UITableViewCell {
    
    
    var message: CKMessage! {
        didSet {
            
            self.messageContent.removeConstraints(self.messageContent.constraints)
            
            self.messageContent.message = message
            
            self.intentText.text = message.topic != "" ? "Detected Intent" : ""
            self.intentName.text = message.topic
            
            
        }
    }
    
    var intentText: UILabel = {
        
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .gray
        label.font = UIFont.systemFont(ofSize: UIFont.systemFontSize, weight: .regular)
        label.numberOfLines = 0
        return label
        
    }()
    
    var intentName: UILabel = {
        
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .orange
        label.font = UIFont.systemFont(ofSize: UIFont.systemFontSize, weight: .semibold)
        label.numberOfLines = 0
        label.isUserInteractionEnabled = true
        return label
        
    }()
    
    var intentLabelHandler: ((CKMessage) -> Void)!
    
    
    var messageContent: MessageContentView = {
        
        let v = MessageContentView(frame: .zero)
        v.translatesAutoresizingMaskIntoConstraints = false
        //v.backgroundColor = UIColor.white
        v.layer.cornerRadius = 10
        v.clipsToBounds = true
        return v
        
    }()
    
    
    
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.contentView.addSubview(self.messageContent)
        self.contentView.addSubview(self.intentText)
        self.contentView.addSubview(self.intentName)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.intentLabelTap))
        self.intentName.addGestureRecognizer(tapGesture)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func intentLabelTap() {
        
        if(self.intentLabelHandler != nil) {
            self.intentLabelHandler(self.message)
        }
        
    }
    
}



class MessageCellContent {
    var conversation: CKConversation!
    var expanded: Bool! = false
    
    init(conversation: CKConversation, expanded: Bool) {
        self.conversation = conversation
        self.expanded = expanded
    }
}

