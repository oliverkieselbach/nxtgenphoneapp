//
//  IntentResponsePageViewController.swift
//  NextGenPhoneApp
//
//  Created by Kieselbach, Oliver on 12.03.19.
//  Copyright © 2019 Kieselbach, Oliver. All rights reserved.
//

import UIKit

/*
class MessageIntentResponsePageViewController: UIPageViewController {

    
    override init(transitionStyle style: UIPageViewController.TransitionStyle, navigationOrientation: UIPageViewController.NavigationOrientation, options: [UIPageViewController.OptionsKey : Any]? = nil) {
        super.init(transitionStyle: .scroll, navigationOrientation: navigationOrientation, options: options)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let cancelButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(self.cancelAction))
        self.navigationItem.rightBarButtonItem = cancelButton
    }

    @objc func cancelAction() {
        self.dismiss(animated: true) {
        }
    }
    
    func show(viewController: UIViewController) {
        self.title = viewController.title
        setViewControllers([viewController], direction: .forward, animated: true, completion: nil)
    }
    
}
 */
