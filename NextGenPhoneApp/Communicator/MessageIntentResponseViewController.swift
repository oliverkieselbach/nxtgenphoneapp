//
//  IntentResponseViewController.swift
//  NextGenPhoneApp
//
//  Created by Kieselbach, Oliver on 12.03.19.
//  Copyright © 2019 Kieselbach, Oliver. All rights reserved.
//

import UIKit
import Eureka
import FirebaseFirestore
import NextGenPhoneFramework

/*
class MessageIntentHeader: UIView {
    

    var avatar: CKContactView = {
        
        let a = CKContactView(frame: .zero)
        a.translatesAutoresizingMaskIntoConstraints = false
        a.heightAnchor.constraint(equalToConstant: 80).isActive = true
        a.widthAnchor.constraint(equalToConstant: 80).isActive = true
        
        return a
    }()

    var contactName: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.regular)
        label.textAlignment = .center
        label.numberOfLines = 2
        
        return label
        
    }()

    
    var intentTitle: UILabel = {
        
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 24, weight: .bold)
        
        return label
        
    }()
    
    var intentDescription: UILabel = {
        
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .gray
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 15, weight: .regular)
        label.numberOfLines = 0
        
        return label
        
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubview(self.avatar)
        self.addSubview(self.contactName)
        self.addSubview(self.intentTitle)
        //self.addSubview(self.intentDescription)
        
        self.backgroundColor = .white
        
        self.avatar.topAnchor.constraint(equalTo: self.topAnchor, constant: 40).isActive = true
        self.avatar.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        
        self.contactName.topAnchor.constraint(equalTo: self.avatar.bottomAnchor, constant: 5).isActive = true
        self.contactName.centerXAnchor.constraint(equalTo: self.avatar.centerXAnchor, constant: 0).isActive = true
        
        self.intentTitle.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -15).isActive = true
        self.intentTitle.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 15).isActive = true
        self.intentTitle.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -15).isActive = true
        
        //self.intentDescription.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 15).isActive = true
        //self.intentDescription.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -15).isActive = true
        
        //self.intentDescription.topAnchor.constraint(equalTo: self.intentTitle.bottomAnchor, constant: 15).isActive = true
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

class MessageIntentFooter: UIView {
    
    var responseButton: UIButton = {

        let button = UIButton(type: .custom)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.heightAnchor.constraint(equalToConstant: 44).isActive = true
        button.setTitle("Respond", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        button.layer.cornerRadius = 8
        button.backgroundColor = UIColor.actionColor(color: .blue)

        return button
        
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubview(self.responseButton)
        
        self.backgroundColor = .white
        
        self.responseButton.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        self.responseButton.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 15).isActive = true
        self.responseButton.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -15).isActive = true

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    
}

class MessageIntentResponseViewController: FormViewController {

    var message: CKMessage! {
        didSet {
            self.header.avatar.contactWithNoStatus = message.senderDetails
            self.header.contactName.text = message.senderDetails.name
            
            self.intent = message.topicDetails
        }
    }
    
    var intent: CKIntent! {
        didSet {
            self.header.intentTitle.text = intent.name
            self.header.intentDescription.text = intent.description
            
            self.createForm()
        }
    }
    
    var header: MessageIntentHeader = {
        
        let view = MessageIntentHeader(frame: CGRect(x: 0, y: 0, width: 100, height: 220))
        
        return view
        
    }()
    
    var footer: MessageIntentFooter = {
        
        let view = MessageIntentFooter(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.heightAnchor.constraint(equalToConstant: 100).isActive = true
        
        return view
        
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableHeaderView = header
        self.tableView.backgroundColor = .white
        
        self.view.addSubview(self.footer)
        self.footer.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 0).isActive = true
        self.footer.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 0).isActive = true
        self.footer.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: 0).isActive = true
        self.footer.responseButton.addTarget(self, action: #selector(self.respondButtonAction), for: .touchDown)
        self.footer.responseButton.isHidden = self.message.sender == ConversationKit.shared.currentUser.uid ? true : false
        
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 100, right: 0)

    }
    
    @objc func respondButtonAction() {
        
        let intentFactory = IntentHandler.instance().getResponseActionClass(id: intent.id)
        let actions = intentFactory.getResponseActions(message: self.message, pageViewController: self.parent as! MessageIntentResponsePageViewController)

        let ac = UIAlertController(title: "Respond", message: "Take one of the actions to respond to the message topic", preferredStyle: .actionSheet)
        for action in actions {
            
            ac.addAction(action)
            
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
        }
        
        ac.addAction(cancelAction)

        self.present(ac, animated: true) {
        }
        
    }
    
    func createForm() {
        
        let section = Section("")
        form.append(section)
        
        for i in 0..<intent.parameter.count {
            
            let parameter = intent.parameter[i]
            
            switch parameter.type! {
            case .date:
                section <<< DateInlineRow() {
                    $0.baseCell.isUserInteractionEnabled = false
                    $0.title = parameter.name
                    $0.cell.detailTextLabel?.textColor = UIColor.actionColor(color: .red)
                    if(parameter.value != nil) {
                        $0.value = Date(timestamp: parameter.value as! Timestamp)
                    }
                    
                    $0.onChange({ (row) in
                        self.intent.parameter[i].value = Timestamp(date: row.value!)
                    })
                }
            case .time:
                section <<< TimeInlineRow() {
                    $0.baseCell.isUserInteractionEnabled = false
                    $0.title = parameter.name
                    $0.cell.detailTextLabel?.textColor = UIColor.actionColor(color: .red)
                    if(parameter.value != nil) {
                        $0.value = Date(timestamp: parameter.value as! Timestamp)
                    }
                    
                    $0.onChange({ (row) in
                        self.intent.parameter[i].value = Timestamp(date: row.value!)
                    })
                }
            case .string:
                section <<< TextRow() {
                    $0.baseCell.isUserInteractionEnabled = false
                    $0.title = parameter.name
                    $0.cell.detailTextLabel?.textColor = UIColor.actionColor(color: .red)
                    if(parameter.value != nil) {
                        $0.value = parameter.value as! String
                    }
                    $0.onChange({ (row) in
                        self.intent.parameter[i].value = row.value
                    })
                }
            case .bool:
                section <<< SwitchRow() {
                    $0.baseCell.isUserInteractionEnabled = false
                    $0.title = parameter.name
                    if(parameter.value != nil) {
                        $0.value = parameter.value as! Bool
                    }
                    $0.onChange({ (row) in
                        self.intent.parameter[i].value = row.value
                    })
                }
            }
            
        }
        
    }

}
*/
