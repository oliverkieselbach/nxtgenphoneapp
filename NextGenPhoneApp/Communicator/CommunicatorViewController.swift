//
//  CommunicatorViewController.swift
//  NextGenPhoneApp
//
//  Created by Kieselbach, Oliver on 08.05.19.
//  Copyright © 2019 Kieselbach, Oliver. All rights reserved.
//

import UIKit
import BmoViewPager
import SmoothPicker
import NextGenPhoneFramework
import SwiftyCam
import PushKit
import CallKit


extension UILabel {
    @objc override open func setSmoothSelected(_ selected : Bool) {
        self.textColor = selected ? .black : .gray
        //self.textColor = selected ? .black : .gray
    }
}

protocol VideoCallDelegate {
    
    func callStarted()
    func callEnded()
    func remotePartnerConnected()
    func remotePartnerDisconnected()
    func localVideoStarted(completion: @escaping () -> Void)

}

class CommunicatorHeaderView: UIView {
    
    var contact: CKContact! {
        didSet {
            self.avatar.contact = contact
            self.name.text = contact.name
        }
    }
    
    var avatar: CKContactView! = {
        
        let view = CKContactView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.heightAnchor.constraint(equalToConstant: 70).isActive = true
        view.widthAnchor.constraint(equalToConstant: 70).isActive = true
        
        return view
        
    }()
    
    var name: UILabel! = {
        
        var label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 22, weight: .bold)
        label.textAlignment = .left
        label.textColor = .white
        
        label.numberOfLines = 2
        
        return label
        
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubview(self.avatar)
        self.addSubview(self.name)
        
        self.avatar.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        self.avatar.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 15).isActive = true
        
        self.name.centerYAnchor.constraint(equalTo: self.avatar.centerYAnchor, constant: 0).isActive = true
        self.name.leftAnchor.constraint(equalTo: self.avatar.rightAnchor, constant: 10).isActive = true
        self.name.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -15).isActive = true
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

class CommunicatorChannelFooterView: UIView {
    
}


class CommunicatorButtonView: UIView {

    enum Mode {
        case video
        case voice
        case live
        case form
    }
    
    var mode: Mode! {
        didSet {

            self.videoRecordButton.alpha = mode == .video ? 1 : 0
            self.voiceRecordButton.alpha = mode == .voice ? 1 : 0
            self.liveCallButton.alpha = mode == .live ? 1 : 0
            self.formSelectButton.alpha = mode == .form ? 1: 0
        }
    }
    
    
    var voiceRecordButton: RecordingButton! = {
        
        let button = RecordingButton(type: .custom)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.heightAnchor.constraint(equalToConstant: 65).isActive = true
        button.widthAnchor.constraint(equalToConstant: 65).isActive = true
        button.outlineColor = UIColor(red: 230/255, green: 230/255, blue: 230/255, alpha: 230/255)
        
        button.recordingDuration = 30
        return button
    }()
    
    var videoRecordButton: RecordingButton! = {
        
        let button = RecordingButton(type: .custom)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.heightAnchor.constraint(equalToConstant: 65).isActive = true
        button.widthAnchor.constraint(equalToConstant: 65).isActive = true
        button.outlineColor = UIColor(red: 230/255, green: 230/255, blue: 230/255, alpha: 230/255)
        
        button.recordingDuration = 30

        return button
    }()
    
    var liveCallButton: UIButton! = {

        let button = UIButton(type: .custom)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.heightAnchor.constraint(equalToConstant: 65).isActive = true
        button.widthAnchor.constraint(equalToConstant: 65).isActive = true
        button.layer.cornerRadius = 65 / 2
        button.setBackgroundColor(color: UIColor(0x2ecc71), forState: .normal)
        button.setBackgroundColor(color: .red, forState: .selected)
        
        button.tintColor = .white
        button.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        button.setImage(UIImage(named: "call_filled")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.setImage(UIImage(named: "endcall")?.withRenderingMode(.alwaysTemplate), for: .selected)

        
        return button
        
    }()

    var formSelectButton: UIButton! = {
        
        let button = UIButton(type: .custom)
        button.translatesAutoresizingMaskIntoConstraints = false
        //button.backgroundColor = UIColor.darkGray.withAlphaComponent(0.2)
        button.heightAnchor.constraint(equalToConstant: 40).isActive = true
        button.widthAnchor.constraint(equalToConstant: 150).isActive = true
        button.layer.cornerRadius = 8
        button.setTitle("Select Shortcut", for: .normal)
        button.setTitleColor(UIColor.darkGray, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
        button.setBackgroundColor(color: UIColor.darkGray.withAlphaComponent(0.2), forState: .normal)
        //button.titleLabel!.font = UIFont.systemFont(ofSize: 15, weight: .semibold)

        button.setTitle("Send", for: .selected)
        button.setBackgroundColor(color: button.tintColor, forState: .selected)
        button.setTitleColor(UIColor.white, for: .selected)

        return button
        
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addSubview(self.voiceRecordButton)
        self.addSubview(self.videoRecordButton)
        self.addSubview(self.liveCallButton)
        self.addSubview(self.formSelectButton)

        self.voiceRecordButton.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        self.voiceRecordButton.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true

        self.videoRecordButton.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        self.videoRecordButton.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true

        self.liveCallButton.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        self.liveCallButton.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true

        self.formSelectButton.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        self.formSelectButton.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true

        self.videoRecordButton.alpha = 0
        self.voiceRecordButton.alpha = 0
        self.liveCallButton.alpha = 0
        self.formSelectButton.alpha = 0
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

class FormPreviewViewController: UIViewController {
    
}

class CommunicatorPreviewViewController: SwiftyCamViewController {
    
    public var didCreateMessage: ((LeaveMessage, Bool) -> Void)?

    public var overlay: UIVisualEffectView!
    
    override func viewDidLoad() {
        defaultCamera = .front
        videoQuality = .medium
        videoGravity = .resizeAspectFill
        maximumVideoDuration = 20
        swipeToZoom = false
        cameraDelegate = self

        super.viewDidLoad()
        
        self.addCaptureSessionObserver()
        
        let effect = UIBlurEffect(style: .regular)
        self.overlay = UIVisualEffectView(effect: effect)
        self.overlay.frame = self.view.frame
        self.view.addSubview(self.overlay)
        
        


    }
    
}

extension CommunicatorPreviewViewController: RecordingButtonDelegate {
    
    func didStartCapture() {
        self.startVideoRecording()
    }
    
    func didEndCapture() {
        self.stopVideoRecording()
    }
    
}

extension CommunicatorPreviewViewController: SwiftyCamViewControllerDelegate {
    
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didFailToRecordVideo error: Error) {
        print("did fail to record video")
    }
    
    func swiftyCamSessionDidStartRunning(_ swiftyCam: SwiftyCamViewController) {
        print("did start running")
    }
    
    func swiftyCamSessionDidStopRunning(_ swiftyCam: SwiftyCamViewController) {
        print("did stop running")
    }
    
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didBeginRecordingVideo camera: SwiftyCamViewController.CameraSelection) {
        print("did begin recording video")
    }
    
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didFinishRecordingVideo camera: SwiftyCamViewController.CameraSelection) {
        print("did finish recording video")
    }
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didFinishProcessVideoAt url: URL) {
        print("did finish process video at")
        let leaveMessage = LeaveMessage(type: .video, content: nil, contentURL: url)
        
        if(self.didCreateMessage != nil) {
            self.didCreateMessage!(leaveMessage, true)
        }
        
        
    }
    
}


class CommunicatorViewController: UIViewController {
    
    var contact: CKContact! {
        didSet {
            self.partnerAvatar.contact = contact
            self.partnerName.text = contact.name
            self.videoCallVC.contact = contact
        }
    }
    var conversationId: String!
    var contextMessage: CKMessage!

    /*
    var header: CommunicatorHeaderView! = {
        let v = CommunicatorHeaderView(frame: .zero)
        v.translatesAutoresizingMaskIntoConstraints = false
        
        return v
    }()
    */
    
    var closeButton: UIButton = {

        let button = UIButton(type: .custom)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.heightAnchor.constraint(equalToConstant: 30).isActive = true
        button.widthAnchor.constraint(equalToConstant: 30).isActive = true
        
        button.tintColor = .white
        button.backgroundColor = .clear
        button.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        
        button.setImage(UIImage(imageLiteralResourceName: "x").withRenderingMode(.alwaysTemplate), for: .normal)
        
        return button
        
    }()
    
    var communicatorButton: CommunicatorButtonView! = {
        let v = CommunicatorButtonView(frame: .zero)
        v.translatesAutoresizingMaskIntoConstraints = false
        v.heightAnchor.constraint(equalToConstant: 65).isActive = true
        v.widthAnchor.constraint(equalToConstant: 65).isActive = true
        
        return v
    }()
    
    
    var picker: SmoothPickerView = {

        let v = SmoothPickerView(frame: .zero)
        v.translatesAutoresizingMaskIntoConstraints = false
        
        return v
        
        
    }()
    
    var viewPager: BmoViewPager! = {
        
        let v = BmoViewPager()
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
        
    }()
    
    
    var pageFooter: [UILabel] = {

        var pageFooter0: UILabel! = {
            
            let label = UILabel()
            label.textAlignment = .center
            label.textColor = .lightGray
            label.font = UIFont.systemFont(ofSize: 13, weight: .semibold)
            label.text = "shortcut".uppercased()
            
            return label
            
        }()

        var pageFooter1: UILabel! = {
            
            let label = UILabel()
            label.textAlignment = .center
            label.textColor = .lightGray
            label.font = UIFont.systemFont(ofSize: 13, weight: .semibold)
            label.text = "voice".uppercased()
            
            return label
            
        }()
        
        var pageFooter2: UILabel! = {
            
            let label = UILabel()
            label.textAlignment = .center
            label.textColor = .lightGray
            label.font = UIFont.systemFont(ofSize: 13, weight: .semibold)
            label.text = "video".uppercased()

            return label
            
        }()
        var pageFooter3: UILabel! = {
            
            let label = UILabel()
            label.textAlignment = .center
            label.textColor = .lightGray
            label.font = UIFont.systemFont(ofSize: 13, weight: .semibold)
            label.text = "live".uppercased()

            return label
            
        }()
        

        return [pageFooter0, pageFooter1, pageFooter2, pageFooter3]

    }()

    
    
    var voiceRecVC: VoiceMessageViewController!
    var videoCallVC: VideoCallViewController! = VideoCallViewController()
    var formPreviewVC: FormPreviewViewController!
    
    var preview: CommunicatorPreviewViewController = CommunicatorPreviewViewController()

    var partnerAvatar: CKContactView! = {
        
        let view = CKContactView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.heightAnchor.constraint(equalToConstant: 40).isActive = true
        view.widthAnchor.constraint(equalToConstant: 40).isActive = true
        
        return view
        
    }()
    
    var partnerName: UILabel! = {
        
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.bold)
        label.textAlignment = .center
        label.numberOfLines = 1
        
        return label
        
    }()

    var newMessage: CKMessage!
    
    var viewPagerBottomConstraint1: NSLayoutConstraint!
    var viewPagerBottomConstraint2: NSLayoutConstraint!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        

        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.closeAction))
        
        self.partnerAvatar.layoutIfNeeded()
        self.partnerAvatar.sizeToFit()
        self.partnerAvatar.translatesAutoresizingMaskIntoConstraints = true
        self.navigationItem.titleView = self.partnerAvatar


        self.addChild(self.preview)
        self.view.addSubview(self.preview.view)
        self.preview.view.translatesAutoresizingMaskIntoConstraints = false
        
        
        self.view.addSubview(self.viewPager)
        self.view.addSubview(self.communicatorButton)
        self.view.addSubview(self.picker)

        self.view.addSubview(self.partnerName)
        self.partnerName.translatesAutoresizingMaskIntoConstraints = false
        self.partnerName.backgroundColor = .clear
        self.partnerName.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 0).isActive = true
        self.partnerName.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 0).isActive = true
        self.partnerName.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 0).isActive = true
        self.partnerName.heightAnchor.constraint(equalToConstant: 30).isActive = true

        //self.preview.view.topAnchor.constraint(equalTo: self.partnerName.bottomAnchor, constant: 0).isActive = true
        self.preview.view.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0).isActive = true
        self.preview.view.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 0).isActive = true
        self.preview.view.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 0).isActive = true
        self.preview.view.bottomAnchor.constraint(equalTo: self.picker.topAnchor, constant: -5).isActive = true
        self.preview.view.backgroundColor = .clear

        self.picker.dataSource = self
        self.picker.delegate = self
        SmoothPickerConfiguration.setSelectionStyle(selectionStyle: .colored)
        SmoothPickerConfiguration.setColors(selectedColor: .clear, dimmedColor: .clear)

        self.viewPager.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0).isActive = true
        self.viewPager.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 0).isActive = true
        self.viewPager.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 0).isActive = true
        self.viewPagerBottomConstraint1 = self.viewPager.bottomAnchor.constraint(equalTo: self.picker.bottomAnchor, constant: 0)
        self.viewPagerBottomConstraint1.isActive = true
        self.viewPagerBottomConstraint2 = self.viewPager.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0)
        self.viewPagerBottomConstraint2.isActive = false
        self.viewPager.dataSource = self
        self.viewPager.delegate = self
        self.viewPager.layer.cornerRadius = 8
        self.viewPager.clipsToBounds = true
        self.viewPager.backgroundColor = .clear
        self.viewPager.scrollable = true

        self.communicatorButton.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0).isActive = true
        self.communicatorButton.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: -40).isActive = true
        
        self.picker.bottomAnchor.constraint(equalTo: self.communicatorButton.topAnchor, constant: -10).isActive = true
        self.picker.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 15).isActive = true
        self.picker.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -15).isActive = true
        self.picker.heightAnchor.constraint(equalToConstant: 40).isActive = true
        self.picker.backgroundColor = .clear

 
        self.voiceRecVC = VoiceMessageViewController()
        self.voiceRecVC.view.backgroundColor = .clear
        self.voiceRecVC.recordButton.removeFromSuperview()
        
        //self.videoCallVC = VideoCallViewController()
        self.videoCallVC.view.alpha = 0
        self.videoCallVC.delegate = self
        self.videoCallVC.communicatorButtonView = self.communicatorButton
        self.videoCallVC.showVideoCallControls(controls: true)

        
        self.formPreviewVC = FormPreviewViewController()
        self.formPreviewVC.view.alpha = 0
        
        
        self.closeButton.addTarget(self, action: #selector(self.closeAction), for: .touchDown)
        
        //self.preview.session.startRunning()

        self.viewPager.presentedPageIndex = 1
        self.picker.firstselectedItem = 1

        self.communicatorButton.voiceRecordButton.addTarget(self, action: #selector(self.recordVoiceMessage), for: .touchDown)
        self.communicatorButton.voiceRecordButton.delegate = self.voiceRecVC
        
        self.communicatorButton.videoRecordButton.addTarget(self, action: #selector(self.recordVideoMessage), for: .touchDown)
        self.communicatorButton.videoRecordButton.delegate = self.preview //self.videoRecVC
        
        self.communicatorButton.liveCallButton.addTarget(self, action: #selector(self.startLiveMessage), for: .touchUpInside)

        self.voiceRecVC.didCreateMessage = { (leaveMessage, success) in
            
            self.picker.alpha = 1

            if(success) {
                
                self.newMessage = CKMessage(conversation: self.conversationId, leaveMessage: leaveMessage, intent: self.contextMessage == nil ? nil : self.contextMessage.topicDetails, receiver: self.contact)
                
                AppDelegateAccessor.runAssistant(viewController: self, message: self.newMessage, completion: { (transcript, intent) in
                    self.newMessage.text = transcript
                    if(intent != nil) {
                        

                        if(self.newMessage.topicDetails != nil) {
                            self.newMessage.topicActionDetails = intent
                            self.newMessage.topicAction = intent?.name
                        }
                        else {
                            self.newMessage.topic = intent?.name
                            self.newMessage.topicDetails = intent
                        }

                        self.showForm(message: self.newMessage, intent: intent!)
                        
                    }
                    
                })


            }
            else {
            }
        }
        
        
        self.preview.didCreateMessage = { (leaveMessage, success) in
            
            UIView.animate(withDuration: 0.3, animations: {
                self.picker.alpha = 1
                self.preview.overlay.alpha = 1
            }) { (finish) in
                if(success) {
                    
                    self.newMessage = CKMessage(conversation: self.conversationId, leaveMessage: leaveMessage, intent: self.contextMessage == nil ? nil : self.contextMessage.topicDetails, receiver: self.contact)
                    
                    AppDelegateAccessor.runAssistant(viewController: self, message: self.newMessage, completion: { (transcript, intent) in
                        self.newMessage.text = transcript
                        if(intent != nil) {
                            
                            
                            if(self.newMessage.topicDetails != nil) {
                                self.newMessage.topicActionDetails = intent
                                self.newMessage.topicAction = intent?.name
                            }
                            else {
                                self.newMessage.topic = intent?.name
                                self.newMessage.topicDetails = intent
                            }
                            
                            self.showForm(message: self.newMessage, intent: intent!)
                            
                        }
                        
                    })
                    
                    
                }
                else {
                }
            }

        }
        
        
        self.communicatorButton.formSelectButton.addTarget(self, action: #selector(self.selectAction), for: .touchDown)

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    @objc func closeAction() {
        self.dismiss(animated: true) {
        }
    }
    
    @objc func recordVoiceMessage() {
        
        self.picker.alpha = 0
        self.communicatorButton.voiceRecordButton.record()
        
    }
    
    @objc func recordVideoMessage() {
        
        UIView.animate(withDuration: 0.3, animations: {
            self.picker.alpha = 0
            self.preview.overlay.alpha = 0
        }) { (finish) in
            self.communicatorButton.videoRecordButton.record()
        }
        
    }
    
    @objc func startLiveMessage() {
        
        if(communicatorButton.liveCallButton.isSelected) {
            self.videoCallVC.endCall()
            return
        }
        
        
        let actionVC = UIAlertController(title: "What's your intention", message: nil, preferredStyle: .actionSheet)
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
        }
        
        var formActions: [CKAction] = []
        if(self.contextMessage != nil) {
            formActions = self.contextMessage.topicDetails.actions
        }
        else {
            //show the possible intents to start a new conversation
            formActions = self.contact.getPersonalIntents()
        }
        
        if(formActions.count == 0) {
            return
        }
        for formAction in formActions {
            
            let a = UIAlertAction(title: formAction.name, style: .default) { (action) in
                
                //self.showForm(formAction: formAction)
                
                var leaveMessage = LeaveMessage(type: .text, content: formAction.intentQuery, contentURL: nil)
                
                self.newMessage = CKMessage(conversation: self.conversationId, leaveMessage: leaveMessage, intent: self.contextMessage == nil ? nil : self.contextMessage.topicDetails, receiver: self.contact)
                
                AppDelegateAccessor.runAssistant(viewController: self, message: self.newMessage, action: formAction, completion: { (intent) in
                    if(intent != nil) {
                        
                        if(self.newMessage.topicDetails != nil) {
                            self.newMessage.topicActionDetails = intent
                            self.newMessage.topicAction = intent?.name
                        }
                        else {
                            self.newMessage.topic = intent?.name
                            self.newMessage.topicDetails = intent
                        }
                        
                        self.startCall(intent: intent!)
                        
                    }
                    
                })
                
                
                
            }
            
            actionVC.addAction(a)
            
        }
        
        actionVC.addAction(cancel)
        
        self.present(actionVC, animated: true) {
        }

        
    }
    
    func startCall(intent: CKIntent) {
        
        self.videoCallVC.conversationId = self.conversationId
        self.videoCallVC.intent = intent
        self.videoCallVC.contextMessage = self.contextMessage
        self.videoCallVC.startCall()
        
    }
    
    func sendMessage() {

        AppDelegateAccessor.sendMessage(message: self.newMessage)
        self.dismiss(animated: true) {
        }
        
    }
    
    @objc func selectAction() {
        
        if(self.communicatorButton.formSelectButton.isSelected) {
            self.sendMessage()
            return
        }
        
        let actionVC = UIAlertController(title: "Select a Shortcut", message: nil, preferredStyle: .actionSheet)
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
        }

        var formActions: [CKAction] = []
        if(self.contextMessage != nil) {
            formActions = self.contextMessage.topicDetails.actions
        }
        else {
            //show the possible intents to start a new conversation
            formActions = self.contact.getPersonalIntents()
        }

        if(formActions.count == 0) {
            return
        }
        for formAction in formActions {
            
            let a = UIAlertAction(title: formAction.name, style: .default) { (action) in

                //self.showForm(formAction: formAction)
                
                var leaveMessage = LeaveMessage(type: .text, content: formAction.intentQuery, contentURL: nil)
                
                self.newMessage = CKMessage(conversation: self.conversationId, leaveMessage: leaveMessage, intent: self.contextMessage == nil ? nil : self.contextMessage.topicDetails, receiver: self.contact)
                
                AppDelegateAccessor.runAssistant(viewController: self, message: self.newMessage, action: formAction, completion: { (intent) in
                    if(intent != nil) {
                        
                        if(self.newMessage.topicDetails != nil) {
                            self.newMessage.topicActionDetails = intent
                            self.newMessage.topicAction = intent?.name
                        }
                        else {
                            self.newMessage.topic = intent?.name
                            self.newMessage.topicDetails = intent
                        }
                        
                        self.showForm(message: self.newMessage, intent: intent!)
                        
                    }
                    
                })

                
                
            }
            
            actionVC.addAction(a)
            
        }

        actionVC.addAction(cancel)
        
        self.present(actionVC, animated: true) {
        }


    }
    
    func showForm(message: CKMessage, intent: CKIntent) {
        print("Intent: "+intent.name)
        print("Form Document Id: "+intent.formDocumentId)
        self.communicatorButton.mode = .form
        self.communicatorButton.formSelectButton.isSelected = true
        
        //hide channel seletor
        self.picker.isHidden = true
        
        var newMessageVC = self.getNewMessageViewController()
        newMessageVC.title = intent.name
        newMessageVC.intent = intent
        newMessageVC.message = message
    }
    
    
    func getNewMessageViewController() -> NewMessageViewController {

        let controller = NewMessageViewController()
        controller.recipient = self.contact
        
        self.addChild(controller)
        self.view.insertSubview(controller.view, belowSubview: self.communicatorButton)
        controller.view.translatesAutoresizingMaskIntoConstraints = false
        controller.view.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 30).isActive = true
        controller.view.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 0).isActive = true
        controller.view.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 0).isActive = true
        controller.view.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
        

        self.navigationController?.navigationBar.barTintColor = .white
        self.navigationController?.navigationBar.isTranslucent = false
        self.partnerName.backgroundColor = .white
        
        
        return controller
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension CommunicatorViewController: SmoothPickerViewDelegate {
    func didSelectItem(index: Int, view: UIView, pickerView: SmoothPickerView) {
        print("item selected")
        self.viewPager.presentedPageIndex = index
        //self.partnerName.textColor = index < 2 ? .darkGray : .white
    }
}

extension CommunicatorViewController: SmoothPickerViewDataSource {
    func numberOfItems(pickerView: SmoothPickerView) -> Int {
        return self.pageFooter.count
    }
    
    func itemForIndex(index: Int, pickerView: SmoothPickerView) -> UIView {
        let label = self.pageFooter[index]
        label.sizeToFit()
        label.frame.size.width = label.frame.size.width * 1.3
        
        return label
        
    }
    
    
}


extension CommunicatorViewController: BmoViewPagerDelegate {
    
    func bmoViewPagerDelegate(_ viewPager: BmoViewPager, pageChanged page: Int) {
        
        self.picker.scrollToItem(index: page)
        self.picker.currentSelectedIndex = page
        
        if page == 0 {
            UIView.animate(withDuration: 0.3, animations: {
                self.preview.view.alpha = 0
                self.formPreviewVC.view.alpha = 1
            }) { (success) in
                self.communicatorButton.mode = .form
            }
        }
        else if page == 1 {
            UIView.animate(withDuration: 0.3, animations: {
                self.preview.view.alpha = 0
            }) { (success) in
                self.communicatorButton.mode = .voice
            }
        }
        else if page == 2 {
            UIView.animate(withDuration: 0.3, animations: {
                self.preview.view.alpha = 1
            }) { (success) in
                self.communicatorButton.mode = .video
            }

        }
        else if page == 3 {
            UIView.animate(withDuration: 0.3, animations: {
                self.preview.view.alpha = 1
            }) { (success) in
                self.communicatorButton.mode = .live
            }
        }
    }
    
    
    func bmoViewPagerDelegate(_ viewPager: BmoViewPager, shouldSelect page: Int) -> Bool {
        return true
    }
    
    
    
}

extension CommunicatorViewController: BmoViewPagerDataSource {
    func bmoViewPagerDataSourceNumberOfPage(in viewPager: BmoViewPager) -> Int {
        return 4
    }
    
    func bmoViewPagerDataSource(_ viewPager: BmoViewPager, viewControllerForPageAt page: Int) -> UIViewController {
        if(page == 0) {
            //let vc = UIViewController()
            //vc.view.backgroundColor = .green
            //return vc
            return self.formPreviewVC
        }
        else if(page == 1) {
            //let vc = UIViewController()
            //vc.view.backgroundColor = .green
            //return vc
            return self.voiceRecVC
        }
        else if(page == 2) {
            let vc = UIViewController()
            vc.view.backgroundColor = .clear
            return vc
            //return self.preview
        }
        else if(page == 3) {
            //let vc = UIViewController()
            //vc.view.backgroundColor = .orange
            //return vc
            return self.videoCallVC
        }
        

        return UIViewController()
    }
    
    func bmoViewPagerDataSourceNaviagtionBarItemTitle(_ viewPager: BmoViewPager, navigationBar: BmoViewPagerNavigationBar, forPageListAt page: Int) -> String? {
        return "" //Default \(page)"
    }
    
    func bmoViewPagerDataSourceNaviagtionBarItemSize(_ viewPager: BmoViewPager, navigationBar: BmoViewPagerNavigationBar, forPageListAt page: Int) -> CGSize {
        return CGSize.zero
    }

    func bmoViewPagerDataSourceNaviagtionBarItemNormalBackgroundView(_ viewPager: BmoViewPager, navigationBar: BmoViewPagerNavigationBar, forPageListAt page: Int) -> UIView? {
        
        return nil
        
    }

    func bmoViewPagerDataSourceNaviagtionBarItemHighlightedBackgroundView(_ viewPager: BmoViewPager, navigationBar: BmoViewPagerNavigationBar, forPageListAt page: Int) -> UIView? {

        return nil
     
    }

 
    
}

extension CommunicatorViewController: VideoCallDelegate {
    func localVideoStarted(completion: @escaping () -> Void) {
        DispatchQueue.main.async {
            self.preview.session.stopRunning()
            completion()

            UIView.animate(withDuration: 0.3, animations: {
                self.videoCallVC.view.alpha = 1
            }, completion: { (finish) in
                self.viewPagerBottomConstraint1.isActive = false
                self.viewPagerBottomConstraint2.isActive = true
                
                self.view.layoutIfNeeded()
            })

        }
    }
    
    func remotePartnerDisconnected() {
        
        self.videoCallVC.showVideoCallControls(controls: true)

    }
    
    func remotePartnerConnected() {

        self.videoCallVC.showVideoCallControls(controls: false)

    }
    
    
    func callStarted() {
        UIView.animate(withDuration: 0.2, animations: {
            self.communicatorButton.liveCallButton.isSelected = true
            self.picker.alpha = 0
            self.navigationItem.rightBarButtonItem = nil
        }) { (finish) in
        }

    }
    
    func callEnded() {
        self.communicatorButton.liveCallButton.isSelected = false

        
        DispatchQueue.main.async {
            self.preview.session.startRunning()
        }
        

        UIView.animate(withDuration: 0.3, animations: {
            self.picker.alpha = 1
            self.videoCallVC.view.alpha = 0
            self.preview.view.alpha = 1
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(self.closeAction))

        }) { (finish) in
            self.viewPagerBottomConstraint1.isActive = true
            self.viewPagerBottomConstraint2.isActive = false
            self.videoCallVC.showVideoCallControls(controls: true)

        }

    }
    
}

extension CommunicatorViewController {
    
    @objc func longTapAction(gesture: UIGestureRecognizer) {
        
        if(gesture.state == .ended) {
            print("CallViewController longTapAction")
            
            
        }
        
        
    }

    
    func reportIncomingCall(uuid: UUID, call: Call, completion: ((NSError?) -> Void)? = nil) {

        self.picker.scrollToItem(index: 3)
        self.picker.currentSelectedIndex = 3
        self.viewPager.presentedPageIndex = 3
        


        self.videoCallVC.roomName = call.room
        self.videoCallVC.activeCall = call
        self.videoCallVC.conversationId = call.conversationId
        self.videoCallVC.intent = call.intent
        
        let callHandle = CXHandle(type: .phoneNumber, value: call.room ?? "")
        
        let callUpdate = CXCallUpdate()
        callUpdate.remoteHandle = callHandle
        callUpdate.supportsDTMF = false
        callUpdate.supportsHolding = true
        callUpdate.supportsGrouping = false
        callUpdate.supportsUngrouping = false
        callUpdate.hasVideo = true
        
        self.videoCallVC.callKitProvider.reportNewIncomingCall(with: uuid, update: callUpdate) { error in
            if error == nil {
                NSLog("Incoming call successfully reported.")
            } else {
                NSLog("Failed to report incoming call successfully: \(String(describing: error?.localizedDescription)).")
            }
            completion?(error as NSError?)
        }
    }

    static func handleIncomingCallFromPush(payload: PKPushPayload, pushEventCompletion: @escaping () -> Void) {
        
        print("CommunicatorViewController:handleIncomingCallFromPush")
        
        let payloadData = payload.dictionaryPayload
        print(payloadData)
        
        let callId = payloadData["callId"] as! String
        let conversationId = payloadData["conversationId"] as! String

        if(callId != "") {
            
            ConversationKit.shared.db.collection("calls").document(callId).getDocument { (documentSnapshot, error) in

                if(error != nil || documentSnapshot?.data() == nil) {
                    print(error?.localizedDescription ?? "Error reading Call Data")
                }
                else {

                    let call = Call(id: callId, dictionary: (documentSnapshot?.data())!)
                    let contactBasic = call?.callerDetails
                    let contact = CKContact(personEmbedded: contactBasic!)

                    if(call?.contextMessageId != "") {
                        ConversationKit.shared.db.collection(ConversationKit.shared.appUser.id).document("data").collection("conversations").document(conversationId).collection("messages").document(call!.contextMessageId).getDocument { (documentSnapshot, error) in
                            
                            if(error != nil || documentSnapshot?.data() == nil) {
                                print(error?.localizedDescription ?? "Error reading Call Context Message")
                            }
                            else {
                                
                                let contextMessage = CKMessage(id: documentSnapshot!.documentID, dictionary: (documentSnapshot?.data())!)
                                
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "communicator.communicate.action"), object: nil, userInfo: ["incomingCall": true,"call": call, "contact": contact, "conversationId": conversationId, "contextMessage": contextMessage])
                                
                            }
                            pushEventCompletion()
                            
                            
                        }
                        
                    }
                    else {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "communicator.communicate.action"), object: nil, userInfo: ["incomingCall": true,"call": call, "contact": contact, "conversationId": conversationId])
                        pushEventCompletion()
                    }

                }

            }
            
        }
        
        
        
        
        /*
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "communicator.communicate.action"), object: nil, userInfo: ["incomingCall": true, "roomId": room, "contact" : CKContact(personEmbedded: contact!), "intent": intent, "conversationId": conversationId])
        */
        
        
    }

    
}

