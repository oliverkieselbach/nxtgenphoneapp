//
//  MessageIntentResponseActions.swift
//  NextGenPhoneApp
//
//  Created by Kieselbach, Oliver on 12.03.19.
//  Copyright © 2019 Kieselbach, Oliver. All rights reserved.
//

import Foundation
import EventKit
import EventKitUI
import FirebaseFirestore
import NextGenPhoneFramework

/*
class IntentFactory: NSObject {

    override required init() {
        super.init()
    }
    
    func getResponseActions(message: CKMessage, pageViewController: MessageIntentResponsePageViewController) -> [UIAlertAction] {
        return []
    }

    func createIntent(id: String, name: String, parameters: [String: Any]) -> CKIntent? {
        return nil
    }
        
}

class NoneIntentFactoy: IntentFactory {
    
    override func getResponseActions(message: CKMessage, pageViewController: MessageIntentResponsePageViewController) -> [UIAlertAction] {
        return []
    }

}

class CallMeBackIntentFactory: IntentFactory {
    
    required override init() {
        super.init()
    }

    override func createIntent(id: String, name: String, parameters: [String: Any]) -> CKIntent? {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        var params: [CKIntentParameter] = [] as! [CKIntentParameter]
        
        var preferredDate: Timestamp = Timestamp(date: Date())
        if(parameters["preferredDate"] != nil) {
            
            let paramValue = (parameters["preferredDate"] as! [String: Any])["stringValue"] as! String
            if(paramValue != "") {
                let date = dateFormatter.date(from: paramValue)
                preferredDate = Timestamp(date: date!)
                
            }
        }
        
        var preferredTime: Timestamp = Timestamp(date: Date())
        if(parameters["preferredTime"] != nil) {
            
            let paramValue = (parameters["preferredTime"] as! [String: Any])["stringValue"] as! String
            if(paramValue != "") {
                
                let date = dateFormatter.date(from: paramValue)
                preferredTime = Timestamp(date: date!)
                
            }
            
        }
        
        params.append(CKIntentParameter(id: "preferredDate", name: "Preferred Date", type: .date, value: preferredDate)!)
        params.append(CKIntentParameter(id: "preferredTime", name: "Preferred Time", type: .time, value: preferredTime)!)
        
        var intent = CKIntent(id: id, name: name, description: "You can provide some more information about when you want me to call you back", confidenceLevel: 99.999, parameter: params)
        
        
        return intent
    }

    override func getResponseActions(message: CKMessage, pageViewController: MessageIntentResponsePageViewController) -> [UIAlertAction] {

        let callBackAction = UIAlertAction(title: "Call Back", style: .default) { (action) in
            
            pageViewController.dismiss(animated: true, completion: {
                AppDelegateAccessor.showContact(embeddedContact: message.senderDetails)
            })
            
        }
        
        let setReminderAction = UIAlertAction(title: "Set a Reminder to call back", style: .default) { (action) in
            
            let vc = CreateReminderActionViewController()
            vc.title = "Create Reminder"

            vc.message = message
            let preferredTimeParam = message.topicDetails.parameter.index {$0.id == "preferredTime"}
            if(preferredTimeParam != nil) {
                vc.dueDate = Date(timestamp: message.topicDetails.parameter[preferredTimeParam!].value as! Timestamp)
            }
            
            pageViewController.show(viewController: vc)
        }
        
        return [callBackAction, setReminderAction]

    }
        
}
*/

