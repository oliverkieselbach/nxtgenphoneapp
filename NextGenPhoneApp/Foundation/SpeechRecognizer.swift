//
//  SpeechRecognizer.swift
//  NextGenPhoneApp
//
//  Created by Kieselbach, Oliver on 27.11.18.
//  Copyright © 2018 Kieselbach, Oliver. All rights reserved.
//

import Foundation
import Speech
import UIKit

class SpeechRecognizer {
    
    public enum SFButtonError: Error {
        public enum AuthorizationReason {
            case denied, restricted, usageDescription(missing: UsageDescriptionKey)
        }
        public enum CancellationReason {
            case user, notFound
        }
        case authorization(reason: AuthorizationReason), cancelled(reason: CancellationReason), recording, invalid(locale: Locale), notAvailable, unknown(error: Error?)
    }
    
    public enum AuthorizationErrorHandling {
        case none, openSettings(completion: BoolClosure?), custom(handler: ErrorClosure)
    }
    
    public typealias UsageDescriptionKey = String
    public typealias BoolClosure = (Bool) -> ()
    public typealias ErrorClosure = (SFButtonError?) -> ()
    public typealias ResultClosure = (URL, SFSpeechRecognitionResult?) -> ()
    
    public var authorizationErrorHandling = AuthorizationErrorHandling.none
    public var resultHandler: ResultClosure?
    public var errorHandler: ErrorClosure?
    public var audioSession = AVAudioSession.sharedInstance()
    public var audioFormatSettings: [String : Any] = [AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
                                                      AVSampleRateKey: 12000,
                                                      AVNumberOfChannelsKey: 1,
                                                      AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue]
    @IBInspectable public var maxDuration: Double = 60
    public var locale = Locale(identifier: "de_DE")  //Locale.autoupdatingCurrent
    public var taskHint = SFSpeechRecognitionTaskHint.unspecified
    public var queue = OperationQueue.main
    public var contextualStrings = [String]()
    public var interactionIdentifier: String?
    
    fileprivate var speechRecognizer: SFSpeechRecognizer?
    fileprivate var speechRecognitionTask: SFSpeechRecognitionTask?
    private let microphoneUsageDescriptionKey: UsageDescriptionKey = UsageDescriptionKey("NSMicrophoneUsageDescription")
    private let speechRecognitionUsageDescriptionKey: UsageDescriptionKey = UsageDescriptionKey("NSSpeechRecognitionUsageDescription")
    
    public var speechRecognition: Bool = true
    /****/

    public func checkSpeechRecognizerAuthorization(_ handler: ErrorClosure? = nil) {
        //if Bundle.main.object(forInfoDictionaryKey: speechRecognitionUsageDescriptionKey) != nil {
        if speechRecognition {
            switch SFSpeechRecognizer.authorizationStatus() {
            case .authorized: handler?(nil)
            case .denied: handler?(.authorization(reason: .denied))
            case .restricted: handler?(.authorization(reason: .restricted))
            case .notDetermined:
                SFSpeechRecognizer.requestAuthorization { _ in
                    self.checkSpeechRecognizerAuthorization(handler)
                }
            }
        } else {
            handler?(nil)
        }
        /*
         } else {
         let error = SFButtonError.authorization(reason: .usageDescription(missing: speechRecognitionUsageDescriptionKey))
         queue.addOperation {
         self.errorHandler?(error)
         }
         handler?(error)
         }
         */
    }
    
    fileprivate func handleAuthorizationError(_ error: SFButtonError, _ handling: AuthorizationErrorHandling) {
        switch handling {
        case .none: break
        case .openSettings(let completion): openSettings(completion)
        case .custom(let handler): handler(error)
        }
    }
    
    public func openSettings(_ completion: BoolClosure? = nil) {
        if let url = URL(string: UIApplication.openSettingsURLString) {
            UIApplication.shared.open(url, completionHandler: completion)
        } else {
            completion?(false)
        }
    }
    
    public func run(url: URL) {
        
        do {
            try self.audioSession.setActive(false, options: .notifyOthersOnDeactivation)
        } catch {
            queue.addOperation {
                self.errorHandler?(.unknown(error: error))
            }
        }
        //activityIndicatorView?.startAnimating()
        checkSpeechRecognizerAuthorization {
            if let error = $0 {
                self.queue.addOperation {
                    //self.activityIndicatorView?.stopAnimating()
                    self.resultHandler?(url, nil)
                    self.handleAuthorizationError(error, self.authorizationErrorHandling)
                }
            } else if self.speechRecognition {
                if self.speechRecognizer == nil {
                    guard let speechRecognizer = SFSpeechRecognizer(locale: self.locale) else {
                        self.queue.addOperation {
                            //self.activityIndicatorView?.stopAnimating()
                            self.resultHandler?(url, nil)
                            self.errorHandler?(.invalid(locale: self.locale))
                        }
                        return
                    }
                    speechRecognizer.defaultTaskHint = self.taskHint
                    speechRecognizer.queue = self.queue
                    self.speechRecognizer = speechRecognizer
                }
                guard self.speechRecognizer?.isAvailable == true else {
                    self.queue.addOperation {
                        //self.activityIndicatorView?.stopAnimating()
                        self.resultHandler?(url, nil)
                        self.errorHandler?(.notAvailable)
                    }
                    return
                }
                if self.speechRecognitionTask == nil {
                    /*
                     guard FileManager.default.fileExists(atPath: (self.audioURL?.absoluteString)!) else {
                     self.queue.addOperation {
                     self.errorHandler?(.cancelled(reason: .user))
                     }
                     return
                     }
                     */
                    let speechRecognitionRequest = SFSpeechURLRecognitionRequest(url: url)
                    speechRecognitionRequest.contextualStrings = self.contextualStrings
                    speechRecognitionRequest.interactionIdentifier = self.interactionIdentifier
                    self.speechRecognitionTask = self.speechRecognizer?.recognitionTask(with: speechRecognitionRequest, resultHandler: { result, error in
                        if let result = result, result.isFinal {
                            self.queue.addOperation {
                                //self.activityIndicatorView?.stopAnimating()
                                print(result.bestTranscription.formattedString)
                                self.resultHandler?(url, result)
                            }
                            self.speechRecognitionTask = nil
                        } else if let error = error {
                            self.queue.addOperation {
                                //self.activityIndicatorView?.stopAnimating()
                                self.resultHandler?(url, nil)
                                self.errorHandler?(.unknown(error: error))
                            }
                            self.speechRecognitionTask = nil
                        }
                    })
                }
            } else {
                self.queue.addOperation {
                    //self.activityIndicatorView?.stopAnimating()
                    self.resultHandler?(url, nil)
                }
            }
        }

    }
    
}
