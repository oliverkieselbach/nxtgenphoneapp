//
//  TwilioVideoRecorder.swift
//  NextGenPhoneApp
//
//  Created by Kieselbach, Oliver on 19.02.19.
//  Copyright © 2019 Kieselbach, Oliver. All rights reserved.
//

import AVFoundation
import Foundation
import TwilioVideo

class TwilioVideoRecorder : NSObject, TVIVideoRenderer, TVIAudioSink {
    var identifier : String!
    var videoTrack : TVIVideoTrack!
    var audioTrack : TVIAudioTrack!

    var recorderTimestamp = CMTime.invalid
    var videoFormatDescription: CMFormatDescription?
    var videoRecorder : AVAssetWriter?
    var videoRecorderInput : AVAssetWriterInput?
    var audioRecorderInput : AVAssetWriterInput?

    var videoURL: URL!
    
    // Register pixel formats that are known to work with AVAssetWriterInput.
    var optionalPixelFormats: [NSNumber] = [NSNumber(value: TVIPixelFormat.formatYUV420BiPlanarFullRange.rawValue),
                                            NSNumber(value: TVIPixelFormat.formatYUV420BiPlanarVideoRange.rawValue),
                                            NSNumber(value: TVIPixelFormat.format32BGRA.rawValue),
                                            NSNumber(value: TVIPixelFormat.format32ARGB.rawValue)]
    
    init(videoTrack: TVIVideoTrack, audioTrack: TVIAudioTrack, identifier: String) {
        self.videoTrack = videoTrack
        self.audioTrack = audioTrack
        self.identifier = identifier
        
        super.init()
        
        initRecording()
    }

    init(audioTrack: TVIAudioTrack, identifier: String) {
        
        super.init()

        self.audioTrack = audioTrack
        self.identifier = identifier
        
        
        initRecording()
    }

    private func initRecording() {
        do {
            self.videoRecorder = try AVAssetWriter.init(url:TwilioVideoRecorder.recordingURL(identifier: identifier),
                                                        fileType: AVFileType.mov)
        } catch {
            print("Could not create AVAssetWriter with error: \(error)")
            return
        }
        
        // The recorder will determine the asset's format as frames arrive.
        videoTrack.addRenderer(self)
        audioTrack.addSink(self)
        
        // This example does not support backgrounding. Now might be a good point to consider kicking off a background
        // task, and handling failures.
    }
    
    private func startRecording(frame: TVIVideoFrame) {
        self.recorderTimestamp = frame.timestamp
        
        // Determine width and height dynamically (based upon the first frame). This works well for local content.
        let outputSettings: [String : Any]
        if #available(iOS 11.0, *) {
            outputSettings = [
                AVVideoCodecKey : AVVideoCodecType.h264,
                AVVideoWidthKey : frame.width,
                AVVideoHeightKey : frame.height,
                AVVideoScalingModeKey : AVVideoScalingModeResizeAspect] as [String : Any]
        } else {
            outputSettings = [
                AVVideoWidthKey : frame.width,
                AVVideoHeightKey : frame.height,
                AVVideoScalingModeKey : AVVideoScalingModeResizeAspect] as [String : Any]
        }
        
        videoRecorderInput = AVAssetWriterInput(mediaType: AVMediaType.video, outputSettings: outputSettings)
        videoRecorderInput?.expectsMediaDataInRealTime = true
        videoRecorderInput?.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi * 0.5))
        
        audioRecorderInput = AVAssetWriterInput(mediaType: .audio, outputSettings: [AVSampleRateKey: 44100, AVFormatIDKey: kAudioFormatLinearPCM, AVNumberOfChannelsKey: 1, AVLinearPCMBitDepthKey: 16, AVLinearPCMIsFloatKey: false, AVLinearPCMIsBigEndianKey: false, AVLinearPCMIsNonInterleaved: false])
        audioRecorderInput?.expectsMediaDataInRealTime = true
        
        if let videoRecorder = self.videoRecorder,
            let audioRecorderInput = self.audioRecorderInput,
            let videoRecorderInput = self.videoRecorderInput,
            videoRecorder.canAdd(videoRecorderInput) {
            videoRecorder.add(videoRecorderInput)
            videoRecorder.add(audioRecorderInput)
            
            if (videoRecorder.startWriting()) {
                self.videoRecorder?.startSession(atSourceTime: self.recorderTimestamp)
            } else {
                print("Could not start writing!")
            }
        } else {
            print("Could not add AVAssetWriterInput!")
        }
    }

    private func startRecording(timestamp: CMTime) {
        self.recorderTimestamp = timestamp
        
        // Determine width and height dynamically (based upon the first frame). This works well for local content.
        audioRecorderInput = AVAssetWriterInput(mediaType: .audio, outputSettings: [AVSampleRateKey: 44100, AVFormatIDKey: kAudioFormatLinearPCM, AVNumberOfChannelsKey: 1, AVLinearPCMBitDepthKey: 16, AVLinearPCMIsFloatKey: false, AVLinearPCMIsBigEndianKey: false, AVLinearPCMIsNonInterleaved: false])
        audioRecorderInput?.expectsMediaDataInRealTime = true
        
        if let videoRecorder = self.videoRecorder,
            let audioRecorderInput = self.audioRecorderInput,
            videoRecorder.canAdd(audioRecorderInput) {
            videoRecorder.add(audioRecorderInput)
            
            if (videoRecorder.startWriting()) {
                self.videoRecorder?.startSession(atSourceTime: self.recorderTimestamp)
            } else {
                print("Could not start writing!")
            }
        } else {
            print("Could not add AVAssetWriterInput!")
        }
    }

    public func stopRecording(completion: @escaping (URL) -> Void) {
        videoTrack.removeRenderer(self)
        videoRecorderInput?.markAsFinished()
        audioRecorderInput?.markAsFinished()
        videoRecorder?.finishWriting {
            if (self.videoRecorder?.status == AVAssetWriter.Status.failed) {
                print("Failed to write asset.")
            } else if (self.videoRecorder?.status == AVAssetWriter.Status.completed) {
                print("Completed asset with URL:", self.videoRecorder?.outputURL.absoluteString)
                self.videoURL = self.videoRecorder?.outputURL
            }
            self.videoRecorder = nil
            self.videoRecorderInput = nil
            self.recorderTimestamp = CMTime.invalid
            completion(self.videoURL)
        }
    }
    
    class func recordingURL(identifier: String) -> URL {
        guard let documentsDirectory = FileManager.default.urls(for: FileManager.SearchPathDirectory.documentDirectory,
                                                                in: FileManager.SearchPathDomainMask.userDomainMask).last else {
                                                                    return URL(fileURLWithPath: "")
        }
        
        // Choose a filename which will be unique if the `identifier` is reused (Append RFC3339 formatted date).
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "HHmmss"
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        
        let dateComponent = dateFormatter.string(from: Date())
        let filename = identifier + "-" + dateComponent + ".mov"
        
        return documentsDirectory.appendingPathComponent(filename)
    }

    func renderSample(_ audioSample: CMSampleBuffer!) {
        
        let timestamp: CMTime = CMSampleBufferGetPresentationTimeStamp(audioSample)
        
        if (CMTIME_IS_INVALID(recorderTimestamp)) {
            print("Received first video sample at: \(timestamp). Starting recording session.")
            //startRecording(timestamp: timestamp)
        }
        if let buffer = audioSample,
            let input = audioRecorderInput,
            input.isReadyForMoreMediaData,
            input.append(buffer) {
            
            // Success.
        } else {
            print("Couldn't append a SampleBuffer.")
        }
        
    }

}

extension TwilioVideoRecorder {
    

    func renderFrame(_ frame: TVIVideoFrame) {
        // Frames are delivered with presentation timestamps. We will make do with this for our recorder.
        let timestamp = frame.timestamp
        
        // Defer creating and configuring the input until a frame has arrived.
        if (CMTIME_IS_INVALID(recorderTimestamp)) {
            print("Received first video sample at: \(timestamp). Starting recording session.")
            startRecording(frame: frame)
        }
        
        detectFormatChange(imageBuffer: frame.imageBuffer)
        
        // Our uncompressed buffers do not need to be decoded.
        // TODO: Assuming the duration might not be a good idea.
        var sampleTiming = CMSampleTimingInfo.init(duration: CMTime(value: 1, timescale: 30),
                                                   presentationTimeStamp: timestamp,
                                                   decodeTimeStamp: CMTime.invalid)
        
        // Append a CMSampleBuffer to the recorder's input.
        // TODO: Support I420 inputs for recording of remote content.
        var sampleBuffer: CMSampleBuffer?
        let status = CMSampleBufferCreateReadyWithImageBuffer(allocator: kCFAllocatorDefault,
                                                              imageBuffer: frame.imageBuffer,
                                                              formatDescription: self.videoFormatDescription!,
                                                              sampleTiming: &sampleTiming,
                                                              sampleBufferOut: &sampleBuffer)
        
        if (status != kCVReturnSuccess) {
            print("Couldn't create a SampleBuffer. Status=\(status)")
        } else if let buffer = sampleBuffer,
            let input = videoRecorderInput,
            input.isReadyForMoreMediaData,
            input.append(buffer) {
            // Success.
        } else {
            print("Couldn't append a SampleBuffer.")
        }
    }
    
    func detectFormatChange(imageBuffer: CVPixelBuffer) {
        if (self.videoFormatDescription == nil ||
            CMVideoFormatDescriptionMatchesImageBuffer(self.videoFormatDescription!, imageBuffer: imageBuffer) == false) {
            let status = CMVideoFormatDescriptionCreateForImageBuffer(allocator: kCFAllocatorDefault, imageBuffer: imageBuffer, formatDescriptionOut: &self.videoFormatDescription)
            
            if let format = self.videoFormatDescription {
                let dimensions = CMVideoFormatDescriptionGetDimensions(format)
                let pixelFormat = CVPixelBufferGetPixelFormatType(imageBuffer)
                let utf16 = [
                    UInt16((pixelFormat >> 24) & 0xFF),
                    UInt16((pixelFormat >> 16) & 0xFF),
                    UInt16((pixelFormat >> 8) & 0xFF),
                    UInt16((pixelFormat & 0xFF)) ]
                let pixelFormatString = String(utf16CodeUnits: utf16, count: 4)
                print("Detected format change: \(dimensions.width) x \(dimensions.height) - \(pixelFormatString)")
            } else {
                print("Failed to create output format description with status: \(status)")
            }
        }
    }
    
    func updateVideoSize(_ videoSize: CMVideoDimensions, orientation: TVIVideoOrientation) {
        // The recorder inspects individual frames (including pixel format). As a result, there is nothing to do here.
    }
}
