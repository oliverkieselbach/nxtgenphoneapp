//
//  VideoCallViewController.swift
//  NextGenPhoneApp
//
//  Created by Kieselbach, Oliver on 07.02.19.
//  Copyright © 2019 Kieselbach, Oliver. All rights reserved.
//

import UIKit
import TwilioVideo
import CallKit
import PushKit
import FirebaseFunctions
import FirebaseFirestore
import FirebaseStorage
import FirebaseUI
import NextGenPhoneFramework


struct PlatformUtils {
    static let isSimulator: Bool = {
        var isSim = false
        #if arch(i386) || arch(x86_64)
        isSim = true
        #endif
        return isSim
    }()
}

struct TokenUtils {
    static var baseURLString = "https://silver-cougar-5778.twil.io"
    // If your token server is written in PHP, accessTokenEndpoint needs .php extension at the end. For example : /accessToken.php
    static var accessTokenEndpoint = "/accessToken"
    
    static func fetchAccessToken() throws -> String {
        let endpointWithIdentity = String(format: "%@?identity=%@", TokenUtils.accessTokenEndpoint, PlatformUtils.isSimulator ? "Simulator" : ConversationKit.shared.currentUser.uid)
        let accessTokenURL = URL(string: baseURLString + endpointWithIdentity)
        
        return try! String.init(contentsOf: accessTokenURL!, encoding: .utf8)
    }
}



class VideoCallSettings: NSObject {
    
    let supportedAudioCodecs: [TVIAudioCodec] = [TVIIsacCodec(),
                                                 TVIOpusCodec(),
                                                 TVIPcmaCodec(),
                                                 TVIPcmuCodec(),
                                                 TVIG722Codec()]
    
    let supportedVideoCodecs: [TVIVideoCodec] = [TVIVp8Codec(),
                                                 TVIVp8Codec(simulcast: true),
                                                 TVIH264Codec(),
                                                 TVIVp9Codec()]
    
    var audioCodec: TVIAudioCodec?
    var videoCodec: TVIVideoCodec?
    
    var maxAudioBitrate = UInt()
    var maxVideoBitrate = UInt()
    
    func getEncodingParameters() -> TVIEncodingParameters?  {
        if maxAudioBitrate == 0 && maxVideoBitrate == 0 {
            return nil;
        } else {
            return TVIEncodingParameters(audioBitrate: maxAudioBitrate,
                                         videoBitrate: maxVideoBitrate)
        }
    }
    
    private override init() {
        // Can't initialize a singleton
    }
    
    // MARK: Shared Instance
    static let shared = VideoCallSettings()
}


class ContactCallHeaderView: UIView {
    
    var contact: CKContactBasic! {
        didSet {
            self.avatar.contactWithNoStatus = contact
            self.name.text = contact.name
        }
    }
    
    var avatar: CKContactView = {
        
        let a = CKContactView(frame: .zero)
        a.translatesAutoresizingMaskIntoConstraints = false
        a.heightAnchor.constraint(equalToConstant: 80).isActive = true
        a.widthAnchor.constraint(equalToConstant: 80).isActive = true
        
        return a
    }()
    
    var name: UILabel = {
        
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .white
        label.textAlignment = .left
        label.font = UIFont.systemFont(ofSize: 22, weight: UIFont.Weight.semibold)
        
        return label
        
    }()
    
    var status: UILabel = {
        
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .white
        label.textAlignment = .left
        label.font = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.regular)
        label.text = "Inactive"
        return label
        
    }()
    
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubview(self.avatar)
        self.addSubview(self.name)
        self.addSubview(self.status)
        
        self.avatar.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        self.avatar.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 0).isActive = true
        
        self.name.bottomAnchor.constraint(equalTo: self.centerYAnchor, constant: -2).isActive = true
        self.name.leftAnchor.constraint(equalTo: self.avatar.rightAnchor, constant: 5).isActive = true
        self.name.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 0).isActive = true
        
        self.status.topAnchor.constraint(equalTo: self.centerYAnchor, constant: 2).isActive = true
        self.status.leftAnchor.constraint(equalTo: self.avatar.rightAnchor, constant: 5).isActive = true
        self.status.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 0).isActive = true
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
}

class VideoCallViewController: UIViewController {

    var accessToken: String = ""
    
    // CallKit components
    var callKitProvider: CXProvider!
    var callKitCallController: CXCallController!
    var callKitCompletionHandler: ((Bool)->Swift.Void?)? = nil

    
    var userInitiatedDisconnect: Bool = false
    
    var remoteParticipant: TVIRemoteParticipant?
    var roomName: String!
    
    // Video SDK components
    var room: TVIRoom?
    /**
     * We will create an audio device and manage it's lifecycle in response to CallKit events.
     */
    var localVideoTrack: TVILocalVideoTrack?
    var localAudioTrack: TVILocalAudioTrack?
    
    var localVideoRecorder: TwilioVideoRecorder!
    public var didCreateMessage: ((LeaveMessage, Bool) -> Void)?


    
    var activeCall: Call!
    var callTimer: Timer!
    var callStarted: Date!
    var callStatus: Call.CallStatus! {
        didSet {

            switch callStatus! {
            case .active:
                self.muteAudio(isMuted: false)
                self.callStarted = Date()
                self.callTimer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { (t) in
                    
                    let timePeriod = self.callStarted.chunkBetween(date: t.fireDate)
                    
                    let s = String(format: "%02d:%02d:%02d", timePeriod.hours, timePeriod.minutes, timePeriod.seconds)
                    
                    //self.videoCallVC.callTime.text = s
                })
                //self.navigationItem.rightBarButtonItem = self.callTimeItem
                //self.navigationItem.rightBarButtonItem = nil
                //self.navigationItem.leftBarButtonItem = collapseItem
                
                
            case .calling:
                print("CallViewController - calling...")
                
            case .ended:
                self.muteAudio(isMuted: false)
                self.callStarted = nil
                if(self.callTimer != nil) {
                    self.callTimer.invalidate()
                    self.callTimer = nil
                }
                //self.navigationItem.rightBarButtonItem = nil
                
            case .rejected:
                self.muteAudio(isMuted: false)
                self.callStarted = nil
                if(self.callTimer != nil) {
                    self.callTimer.invalidate()
                    self.callTimer = nil
                }
                //self.navigationItem.rightBarButtonItem = nil
                
                
            default:
                break
            }
            
        }
    }
    
    var sentImages: Dictionary<String, UIImage>! = [:]


    var remoteView: TVIVideoView?
    var previewView: TVIVideoView!
    
    var camera: TVICameraSource?

    
    var contextMessage: CKMessage? {
        didSet {
            
        }
    }
    
    var intent: CKIntent! {
        didSet {
            if(self.messageForm != nil) {
                self.messageForm.intent = intent
            }
            if(self.messageCardHeader != nil) {
                self.messageCardHeader.intent = intent
            }
        }
    }
    
    var contact: CKContact! {
        didSet {
        }
    }
    
    var conversationId: String! {
        didSet {
            if(self.messageForm != nil) {
                self.messageForm.conversationId = conversationId
            }
        }
    }
    
    var muted: Bool = false {
        didSet {
            self.muteAudio(isMuted: muted)
            
        }
    }
    
    var callonHold: Bool = false {
        didSet {
            self.holdCall(onHold: callonHold)
        }
    }
    
    var videoEnabled: Bool = true {
        didSet {
            self.enableVideo(enabled: videoEnabled)
        }
    }
    
    
    
    override func loadView() {
        super.loadView()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.intentFormChanged(notification:)), name: NSNotification.Name(rawValue: "communicator.call.intentform.changed"), object: nil)

    }
    
    
    deinit {
        // CallKit has an odd API contract where the developer must call invalidate or the CXProvider is leaked.
        print("VideoCallViewController - deinit")
        callKitProvider.invalidate()
    }
    
    var previewHeightConstraint: NSLayoutConstraint!
    var previewWidthConstraint: NSLayoutConstraint!
    var previewLeftConstraint: NSLayoutConstraint!
    var previewBottomConstraint: NSLayoutConstraint!

    var remoteHeightConstraint: NSLayoutConstraint!
    var remoteWidthConstraint: NSLayoutConstraint!
    var remoteRightConstraint: NSLayoutConstraint!
    var remoteBottomConstraint: NSLayoutConstraint!

    
    var delegate: VideoCallDelegate!
    var communicatorButtonView: CommunicatorButtonView!
    
    
    var messageCardHeader: ConversationMessageViewLarge! = {
        
        let v = ConversationMessageViewLarge(frame: .zero)
        v.translatesAutoresizingMaskIntoConstraints = false
        
        v.heightAnchor.constraint(equalToConstant: 55).isActive = true
        v.layer.cornerRadius = 55/2
        
        return v
        
    }()
    
    var messageForm: MessageFormViewController! = MessageFormViewController()
    var badgeHub: BadgeHub!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = .white
        
        self.addChild(self.messageForm)
        self.view.addSubview(self.messageForm.view)
        self.messageForm.view.backgroundColor = .white
        self.messageForm.tableView.backgroundColor = .white
        self.messageForm.view.alpha = 0
        
        self.messageForm.view.translatesAutoresizingMaskIntoConstraints = false
        self.messageForm.view.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 30).isActive = true
        self.messageForm.view.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 0).isActive = true
        self.messageForm.view.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 0).isActive = true
        self.messageForm.view.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: 0).isActive = true

        
        self.remoteView = TVIVideoView(frame: .zero)
        self.remoteView!.contentMode = .scaleAspectFill
        self.remoteView?.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(self.remoteView!)
        /*
        self.remoteView?.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0).isActive = true
        self.remoteView?.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 0).isActive = true
        self.remoteView?.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 0).isActive = true
        self.remoteView?.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
        */
 
        self.remoteHeightConstraint = self.remoteView?.heightAnchor.constraint(equalToConstant: self.view.frame.height)
        self.remoteHeightConstraint.isActive = true // 160
        
        self.remoteWidthConstraint = self.remoteView?.widthAnchor.constraint(equalToConstant: self.view.frame.width)
        self.remoteWidthConstraint.isActive = true  //120
        
        self.remoteRightConstraint = self.remoteView?.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 0)
        self.remoteRightConstraint.isActive = true
        
        self.remoteBottomConstraint = self.remoteView?.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0)
        self.remoteBottomConstraint.isActive = true

        self.remoteView?.alpha = 1
        self.remoteView?.layer.cornerRadius = 10
        self.remoteView?.backgroundColor = .clear
        
        self.remoteView?.delegate = self
        
        self.previewView = TVIVideoView(frame: .zero)
        self.previewView!.contentMode = .scaleAspectFill
        self.previewView?.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(self.previewView)
        self.previewHeightConstraint = self.previewView?.heightAnchor.constraint(equalToConstant: 120)
        self.previewHeightConstraint.isActive = true // 160

        self.previewWidthConstraint = self.previewView?.widthAnchor.constraint(equalToConstant: 90)
        self.previewWidthConstraint.isActive = true  //120

        self.previewLeftConstraint = self.previewView?.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 5)
        self.previewLeftConstraint.isActive = true
        
        self.previewBottomConstraint = self.previewView?.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -30)
        self.previewBottomConstraint.isActive = true

        self.previewView.delegate = self
        self.previewView.layer.cornerRadius = 5
        self.previewView.alpha = 0
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(VideoCallViewController.flipCamera))
        self.previewView.addGestureRecognizer(tap)

        
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(self.tapRemoteView))
        self.remoteView?.addGestureRecognizer(tap2)
        
        
        self.view.addSubview(self.messageCardHeader)
        self.messageCardHeader.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0).isActive = true
        self.messageCardHeader.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -30).isActive = true
        let tap3 = UITapGestureRecognizer(target: self, action: #selector(self.tapOnIntent))
        self.messageCardHeader.addGestureRecognizer(tap3)


        
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.badgeHub = BadgeHub(view: self.messageCardHeader)
        self.badgeHub.hideCount()
        self.badgeHub.scaleCircleSize(by: 0.5)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)

        
    }

    var intentVisible: Bool = false
    
    @objc func intentFormChanged(notification: Notification) {
        
        self.badgeHub.increment()
        
    }
    
    @objc func tapOnIntent() {
        
        UIView.animate(withDuration: 0.5, animations: {
            self.badgeHub.setCount(0)
            self.intentVisible = !self.intentVisible
            self.remoteRightConstraint.constant = self.intentVisible == true ? -5 : 0
            self.remoteBottomConstraint.constant = self.intentVisible == true ? -30 : 0
            self.remoteWidthConstraint.constant = self.intentVisible == true ? self.view.frame.width * 0.3 : self.view.frame.width
            self.remoteHeightConstraint.constant = self.intentVisible == true ? self.view.frame.height * 0.3 : self.view.frame.height
            self.messageCardHeader.alpha = self.intentVisible == true ? 0 : 1
            self.messageForm.view.alpha = self.intentVisible == true ? 1 : 0
            self.view.layoutIfNeeded()
        }) { (finish) in
        }

        
    }
    
    @objc func tapRemoteView() {
        if(intentVisible) {
            self.tapOnIntent()
        }
        else {
            self.showVideoCallControls(controls: self.messageCardHeader.alpha == 1 ? true : false)
        }
    }
    
    @objc func showVideoCallControls(controls: Bool) {
        
        UIView.animate(withDuration: 0.2, animations: {
            self.messageCardHeader.alpha = controls == true ? 0 : 1
            self.communicatorButtonView.alpha = controls == true ? 1 : 0
            self.previewView.alpha = controls == true ? 1 : 0
        }) { (finish) in
        }
    }
    
    var frontCamera: AVCaptureDevice!
    var backCamera: AVCaptureDevice!

    
    
    func stopPreview() {
        
        
        self.camera?.stopCapture(completion: { (error) in
            
            self.localAudioTrack = nil
            self.localVideoTrack = nil

        })
        
    }
    
    
    func startPreview() {
        
        if PlatformUtils.isSimulator {
            return
        }

        if(self.frontCamera == nil) {
            self.frontCamera = TVICameraSource.captureDevice(for: .front)
        }
        if(self.backCamera == nil) {
            self.backCamera = TVICameraSource.captureDevice(for: .back)
        }
        
        
        if (frontCamera != nil || backCamera != nil) {
            // Preview our local camera track in the local video preview view.

            //if(self.camera == nil) {
                camera = TVICameraSource(delegate: self)
                self.localVideoTrack = TVILocalVideoTrack.init(source: camera!, enabled: true, name: "Camera")
                
                // Add renderer to video track for local preview
                self.localVideoTrack!.addRenderer(self.previewView)
                logMessage(messageText: "Video track created")
                
                if (frontCamera != nil && backCamera != nil) {
                    // We will flip camera on tap.
                    let tap = UITapGestureRecognizer(target: self, action: #selector(VideoCallViewController.flipCamera))
                    self.previewView.addGestureRecognizer(tap)
                }
            //}

            self.delegate.localVideoStarted {
                self.camera!.startCapture(with: self.frontCamera != nil ? self.frontCamera! : self.backCamera!) { (captureDevice, videoFormat, error) in
                    if let error = error {
                        self.logMessage(messageText: "Capture failed with error.\ncode = \((error as NSError).code) error = \(error.localizedDescription)")
                    } else {
                        self.previewView.shouldMirror = (captureDevice.position == .front)
                    }
                    
                }
            }
            

        }
        else {
            self.logMessage(messageText:"No front or back capture device found!")
        }
    }
    
    @objc func flipCamera() {
        var newDevice: AVCaptureDevice?
        
        if let camera = self.camera, let captureDevice = camera.device {
            if captureDevice.position == .front {
                newDevice = TVICameraSource.captureDevice(for: .back)
            } else {
                newDevice = TVICameraSource.captureDevice(for: .front)
            }
            
            if let newDevice = newDevice {
                camera.select(newDevice) { (captureDevice, videoFormat, error) in
                    if let error = error {
                        self.logMessage(messageText: "Error selecting capture device.\ncode = \((error as NSError).code) error = \(error.localizedDescription)")
                    } else {
                        self.previewView.shouldMirror = (captureDevice.position == .front)
                    }
                }
            }
        }
    }
    
    
    func cleanupRemoteParticipant() {
        if ((self.remoteParticipant) != nil) {
            if ((self.remoteParticipant?.videoTracks.count)! > 0) {
                let remoteVideoTrack = self.remoteParticipant?.remoteVideoTracks[0].remoteTrack
                remoteVideoTrack?.removeRenderer(self.remoteView!)
                
            }
        }
        self.remoteParticipant = nil
        self.saveCall()
    }
    
    func logMessage(messageText: String) {
        NSLog(messageText)
    }
    

    func connectedToRoom(inRoom: Bool) {
        
        //Update any video call controls for if connected or not

        // Show / hide the automatic home indicator on modern iPhones.
        if #available(iOS 11.0, *) {
            self.setNeedsUpdateOfHomeIndicatorAutoHidden()
        }
        
        if(inRoom == true) {
            self.callStatus = .active
            self.observeCall()
        }
        else {
            self.room = nil
        }

    }
    
    
    
    func enableVideo(enabled: Bool) {
        self.localVideoTrack?.isEnabled = enabled
    }
    
    @objc func holdCallAction() {
        self.callonHold = !self.callonHold
    }
    
    func holdCall(onHold: Bool) {
        localAudioTrack?.isEnabled = !onHold
        localVideoTrack?.isEnabled = !onHold
    }
    
    
    func muteAudio(isMuted: Bool) {
        if let localAudioTrack = self.localAudioTrack {
            localAudioTrack.isEnabled = !isMuted
        }
    }
    
    func createCallContext(id: UUID) {
        
        let db  = ConversationKit.shared.db
       
        self.activeCall = Call(callee: CKContactBasic(person: self.contact)!, caller: CKContactBasic(person: ConversationKit.shared.appUser)!, conversationId: self.conversationId )
        self.activeCall.id = id.uuidString
        self.activeCall.contextMessageId = self.contextMessage == nil ? "" : self.contextMessage!.id
        self.activeCall.intent = self.intent
        self.activeCall.room = self.roomName
        
        ConversationKit.shared.db.collection("calls").document(self.activeCall.id).setData(self.activeCall.dictionary, merge: true)
        
        
        
    }
    
    
    
    func logVideoCallEvent(type: CallEntry.CallEntryType) {
        let entry = CallEntry.create(type: type, sender: ConversationKit.shared.appUser.id)
        ConversationKit.shared.db.collection("calls").document((self.room?.uuid!.uuidString)!).collection("events").document().setData(entry.dictionary)
    }
    
    
    @objc func callAction() {
        
        if(self.activeCall != nil) {
            self.endCall()
        }
        else {
            self.startCall()
        }
        
    }
    
    func startCall() {
        
        self.callStatus = .calling //this must be executed first in order to reset values in CallViewController
        self.performStartCallAction(uuid: UUID(), roomName: ConversationKit.shared.appUser.phoneNumber)


    }

    
    func endCall() {
        
        if let room = room, let uuid = room.uuid {
            logMessage(messageText: "Attempting to disconnect from room \(room.name)")
            userInitiatedDisconnect = true
            
            //self.stopPreview()
            
            performEndCallAction(uuid: uuid)
            self.delegate.callEnded()
            
        }
    }
    
    func rejectCall(callId: String) {
        
        ConversationKit.shared.db.collection("calls").document(callId).setData(["status" : Call.CallStatus.rejected.rawValue], merge: true)
        
    }
    
    func saveCall() {
        
        self.stopObservingCall()
        
        if(self.activeCall != nil) {
            ConversationKit.shared.db.collection("calls").document(self.activeCall.id).setData(["status" : Call.CallStatus.ended.rawValue], merge: true)
            
            if(self.activeCall != nil) {
                
                self.activeCall.ended = Date()
                
                let message = CKMessage(phoneCall: self.activeCall)
                
                self.activeCall = nil
                self.callStatus = .ended
                
                message!.write { (success) in
                    self.callStatus = .inactive
                }
                
            }
        }
        
    }
    
    func send(image: UIImage, call: Call?) {
        let localImageRef = UUID().uuidString
        self.sentImages[localImageRef] = image
        
        var entry = CallEntry.create(type: .image, sender: ConversationKit.shared.appUser.id)
        entry.content = ["localRef": localImageRef]
        let docRef = ConversationKit.shared.db.collection("calls").document(call == nil ? self.activeCall.id : call!.id).collection("events").document()
        docRef.setData(entry.dictionary)
        
        let data = image.compressImage()!.pngData()
        
        let imagePath = "calls/"+docRef.documentID+"/"+UUID().uuidString
        let storageMetadata = StorageMetadata(dictionary: ["contentType" : "image/png"])
        let ref = ConversationKit.shared.storage.reference(withPath: imagePath)
        ref.putData(data!, metadata: storageMetadata) { (metadata, error) in
            if(error != nil) {
                print(error?.localizedDescription)
            }
            else {
                entry.content = ["imageRef": imagePath]
                docRef.setData(entry.dictionary)
            }
            
        }
    }
    
    var activeCallObserver: ListenerRegistration!
    
    func stopObservingCall() {
        
        if(self.activeCallObserver != nil) {
            self.activeCallObserver.remove()
            self.activeCallObserver = nil
        }
        
    }
    
    func observeCall() {

        if(self.activeCallObserver != nil) {
            return
        }
        
        let callId = self.room?.uuid?.uuidString
        print("Observe Call "+callId!)
        
        
        self.activeCallObserver = ConversationKit.shared.db.collection("calls").document(callId!).addSnapshotListener { (snapshot, error) in
            if(error == nil) {
                
                if(snapshot!.data() != nil) {
                    
                    let call = Call(id: (snapshot?.documentID)!, dictionary: (snapshot?.data())!)

                    if(self.activeCall == nil) {
                        self.activeCall = call
                    }

                    if(call!.status == .ended) {
                        //self.handleEndCall()
                    }
                    else if(call!.status == .active) {
                        //self.handleAcceptCall()
                    }
                    else if(call!.status == .rejected) {
                        self.handleRejection()
                    }
                    
                }
                
            }
        }
        
    }
    
    var rejectVC: DJSemiModalViewController!
    
    func handleRejection() {
        
        let rejectVC = DJSemiModalViewController()
        rejectVC.title = "Call Rejected"
        let avatar = CKContactView(frame: .zero)
        avatar.translatesAutoresizingMaskIntoConstraints = false
        avatar.heightAnchor.constraint(equalToConstant: 60).isActive = true
        avatar.widthAnchor.constraint(equalToConstant: 60).isActive = true
        avatar.contact = contact
        rejectVC.addArrangedSubview(view: avatar)
        
        let nameLabel = UILabel()
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        nameLabel.widthAnchor.constraint(equalToConstant: self.view.frame.width * 0.75).isActive = true
        nameLabel.text = contact?.name
        nameLabel.textAlignment = .center
        nameLabel.font = UIFont.systemFont(ofSize: 17, weight: .regular)
        nameLabel.textColor = UIColor.black
        rejectVC.addArrangedSubview(view: nameLabel)

        let outerLabelView = UIView()
        outerLabelView.translatesAutoresizingMaskIntoConstraints = false
        outerLabelView.layer.cornerRadius = 10
        outerLabelView.backgroundColor = UIColor(red: 230/255, green: 230/255, blue: 230/255, alpha: 1)
        let rejectMessageLabel = UILabel()
        rejectMessageLabel.translatesAutoresizingMaskIntoConstraints = false
        rejectMessageLabel.numberOfLines = 0
        rejectMessageLabel.textColor = .black
        rejectMessageLabel.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
        outerLabelView.addSubview(rejectMessageLabel)
        rejectMessageLabel.topAnchor.constraint(equalTo: outerLabelView.topAnchor, constant: 10).isActive = true
        rejectMessageLabel.leftAnchor.constraint(equalTo: outerLabelView.leftAnchor, constant: 10).isActive = true
        rejectMessageLabel.rightAnchor.constraint(equalTo: outerLabelView.rightAnchor, constant: -10).isActive = true
        rejectMessageLabel.bottomAnchor.constraint(equalTo: outerLabelView.bottomAnchor, constant: -10).isActive = true
        rejectMessageLabel.text = self.contact.statusContent != nil ? "I am not able to accept your call right now. Please check my message for further information. You can also leave me a Message." : "I am not able to accept your call right now but you can leave me a Message."

        rejectVC.addArrangedSubview(view: outerLabelView)
        
        rejectVC.presentOn(presentingViewController: self, animated: true) {
            self.endCall()
        }
        
    }
    

    /*
    var dataSource: FUIFirestoreTableViewDataSource!
    func observeCallEntries() {

        let callId = self.room?.uuid?.uuidString
        
        let db  = AppDelegateAccessor.db
        let query = db?.collection("calls").document(callId!).collection("events").order(by: "date", descending: true)
        
        if(self.dataSource != nil) {
            self.dataSource.unbind()
        }
        self.dataSource = nil
        
        
        self.dataSource = self.tableView.bind(toFirestoreQuery: query!) { (tableView, indexPath, snapshot) -> UITableViewCell in
            
            
            let data = snapshot.data()
            let id = snapshot.documentID
            
            if(data != nil) {
                
                let callEntry: CallEntry! = CallEntry(id: id, dictionary: data!)
                let content = callEntry.content
                
                
                var eventText = ""
                switch callEntry.type! {
                case .startCall:
                    eventText = "started call"
                case .acceptCall:
                    eventText = "accepeted call"
                case .endCall:
                    eventText = "ended call"
                case .rejectCall:
                    eventText = "rejected call"
                case .appointment:
                    eventText = "sent an appointment proposal"
                case .image:
                    eventText = content!["imageRef"] != nil ? "sent a picture" : "is sending a picture"
                case .location:
                    eventText = "sent a location"
                case .liveLocation:
                    eventText = "shares his location"
                case .unknown:
                    eventText = "unknown"
                }
                
                
                if(callEntry.type == CallEntry.CallEntryType.rejectCall) {
                    let cell = self.tableView.dequeueReusableCell(withIdentifier: "rejectCell", for: indexPath) as! CallRejectedEntryCell
                    cell.sender = callEntry.sender == call.caller ? call.callerDetails : call.calleeDetails
                    cell.leaveMessageButton.addTarget(self, action: #selector(self.sendMessageAction), for: .touchDown)
                    cell.playMessageButton.addTarget(self, action: #selector(self.playMessageAction), for: .touchDown)
                    cell.selectionStyle = .none
                    cell.eventLabel.text = eventText.uppercased()
                    cell.eventLabel.textAlignment = callEntry.sender == AppDelegateAccessor.currentUser.uid ? .left : .right
                    cell.stackView.semanticContentAttribute = callEntry.sender == AppDelegateAccessor.currentUser.uid ? .forceLeftToRight : .forceRightToLeft
                    
                    return cell
                }
                else if(callEntry.type == CallEntry.CallEntryType.image) {
                    let cell = self.tableView.dequeueReusableCell(withIdentifier: "imageCell", for: indexPath) as! ImageEntryCell
                    cell.sender = callEntry.sender == call.caller ? call.callerDetails : call.calleeDetails
                    
                    if(content!["imageRef"] != nil) {
                        cell.picture?.sd_setImage(with: AppDelegateAccessor.storage.reference(withPath: content!["imageRef"] as! String), placeholderImage: cell.picture?.image)
                        
                    }
                    else if(content!["localRef"] != nil) {
                        cell.picture?.image = (self.callManager as! CallManagerMock).sentImages![content!["localRef"] as! String]
                    }
                    else {
                        cell.picture?.image = UIImage()
                    }
                    
                    cell.selectionStyle = .none
                    cell.eventLabel.text = eventText.uppercased()
                    cell.eventLabel.textAlignment = callEntry.sender == AppDelegateAccessor.currentUser.uid ? .left : .right
                    cell.stackView.semanticContentAttribute = callEntry.sender == AppDelegateAccessor.currentUser.uid ? .forceLeftToRight : .forceRightToLeft
                    
                    return cell
                }
                else  {
                    let cell = self.tableView.dequeueReusableCell(withIdentifier: "baseCallCell", for: indexPath) as! BaseCallEntryCall
                    cell.sender = callEntry.sender == call.caller ? call.callerDetails : call.calleeDetails
                    cell.eventLabel.text = eventText.uppercased()
                    cell.selectionStyle = .none
                    cell.eventLabel.textAlignment = callEntry.sender == AppDelegateAccessor.currentUser.uid ? .left : .right
                    cell.stackView.semanticContentAttribute = callEntry.sender == AppDelegateAccessor.currentUser.uid ? .forceLeftToRight : .forceRightToLeft
                    
                    return cell
                }
            }
            
            return UITableViewCell(frame: .zero)
            
        }
        
    }
    */

    
    
    func sendVoipPush() {
        
        let functions = Functions.functions()
        
        let callId = self.room?.uuid?.uuidString
        self.roomName = self.room?.name
        
        
        functions.httpsCallable("sendVoipPush").call(["callId": self.activeCall.id, "conversationId": self.conversationId, "calleeId": self.contact.id]) { (result, error) in
            
            print(result)
            
        }
        
        /*
        functions.httpsCallable("sendVoipPush").call(["conversationId": self.message.conversationId, "intent": intent?.dictionary, "room": self.roomName, "caller": caller?.dictionary ?? [:], "callee": callee?.dictionary ?? [:], "calleeId": callee!.id, "callerId": caller!.id, "callId": callId ?? "" ]) { (result, error) in
            
            print(result)
            
        }
        */
        
        
    }
    
    func initCallKit() {
        let configuration = CXProviderConfiguration(localizedName: "NextGenPhoneApp")
        configuration.maximumCallGroups = 1
        configuration.maximumCallsPerCallGroup = 1
        configuration.supportsVideo = true
        configuration.supportedHandleTypes = [.phoneNumber, .generic]
        
        /*
         if let callKitIcon = UIImage(named: "iconMask80") {
         configuration.iconTemplateImageData = callKitIcon.pngData()
         }
         */
        
        
        callKitProvider = CXProvider(configuration: configuration)
        callKitCallController = CXCallController()
        
        callKitProvider.setDelegate(self, queue: nil)
        
    }

    
}

// MARK: Call Kit Actions
extension VideoCallViewController {
    
    
    func performStartCallAction(uuid: UUID, roomName: String?) {
        
        self.roomName = roomName
        
        self.createCallContext(id: uuid)

        let callHandle = CXHandle(type: .generic, value: roomName ?? "")
        let startCallAction = CXStartCallAction(call: uuid, handle: callHandle)
        
        startCallAction.isVideo = true
        
        let transaction = CXTransaction(action: startCallAction)
        
        callKitCallController.request(transaction)  { error in
            if let error = error {
                NSLog("StartCallAction transaction request failed: \(error.localizedDescription)")
                return
            }
            NSLog("StartCallAction transaction request successful")
        }
    }
    
    
    func performEndCallAction(uuid: UUID) {
        let endCallAction = CXEndCallAction(call: uuid)
        let transaction = CXTransaction(action: endCallAction)
        
        callKitCallController.request(transaction) { error in
            if let error = error {
                NSLog("EndCallAction transaction request failed: \(error.localizedDescription).")
                return
            }
            
            NSLog("EndCallAction transaction request successful")
        }
    }
    
    func performRoomConnect(uuid: UUID, roomName: String? , completionHandler: @escaping (Bool) -> Swift.Void) {
        // Configure access token either from server or manually.
        // If the default wasn't changed, try fetching from server.
        if (accessToken == "") {
            do {
                accessToken = try TokenUtils.fetchAccessToken()
            } catch {
                let message = "Failed to fetch access token"
                logMessage(messageText: message)
                return
            }
        }
        
        self.delegate.callStarted()
        // Prepare local media which we will share with Room Participants.
        self.prepareLocalMedia()
        self.startPreview()

        
        // Preparing the connect options with the access token that we fetched (or hardcoded).
        let connectOptions = TVIConnectOptions.init(token: accessToken) { (builder) in
            
            // Use the local media that we prepared earlier.
            builder.audioTracks = self.localAudioTrack != nil ? [self.localAudioTrack!] : [TVILocalAudioTrack]()
            builder.videoTracks = self.localVideoTrack != nil ? [self.localVideoTrack!] : [TVILocalVideoTrack]()
            
            // Use the preferred audio codec
            if let preferredAudioCodec = VideoCallSettings.shared.audioCodec {
                builder.preferredAudioCodecs = [preferredAudioCodec]
            }
            
            // Use the preferred video codec
            if let preferredVideoCodec = VideoCallSettings.shared.videoCodec {
                builder.preferredVideoCodecs = [preferredVideoCodec]
            }
            
            // Use the preferred encoding parameters
            if let encodingParameters = VideoCallSettings.shared.getEncodingParameters() {
                builder.encodingParameters = encodingParameters
            }
            
            // The name of the Room where the Client will attempt to connect to. Please note that if you pass an empty
            // Room `name`, the Client will create one for you. You can get the name or sid from any connected Room.
            builder.roomName = roomName
            
            // The CallKit UUID to assoicate with this Room.
            builder.uuid = uuid
        }
        
        // Connect to the Room using the options we provided.
        self.room = TwilioVideo.connect(with: connectOptions, delegate: self)
        
        
        AppDelegateAccessor.twilioAudioDevice.block = {
            do {
                kDefaultAVAudioSessionConfigurationBlock()
                
                let audioSession = AVAudioSession.sharedInstance()
                try audioSession.setMode(AVAudioSession.Mode.voiceChat)
            } catch let error as NSError {
                print("Fail: \(error.localizedDescription)")
            }
        }
        
        AppDelegateAccessor.twilioAudioDevice.block();
        
        
        logMessage(messageText: "Attempting to connect to room \(String(describing: roomName))")
        
        self.connectedToRoom(inRoom: true)
        
        self.callKitCompletionHandler = completionHandler
    }
    
    
}

extension VideoCallViewController : TVIRoomDelegate {
    func didConnect(to room: TVIRoom) {
        
        // At the moment, this example only supports rendering one Participant at a time.
        
        logMessage(messageText: "Connected to room \(room.name) as \(String(describing: room.localParticipant?.identity))")
        
        if (room.remoteParticipants.count > 0) {
            self.remoteParticipant = room.remoteParticipants[0]
            self.remoteParticipant?.delegate = self
        }
        
        let cxObserver = callKitCallController.callObserver
        let calls = cxObserver.calls
        
        // Let the call provider know that the outgoing call has connected
        if let uuid = room.uuid, let call = calls.first(where:{$0.uuid == uuid}) {
            if call.isOutgoing {
                callKitProvider.reportOutgoingCall(with: uuid, connectedAt: nil)
                self.sendVoipPush()
                //self.createCallContext(id: room.uuid!)
            }
        }
        self.callKitCompletionHandler!(true)
        
    }
    
    func room(_ room: TVIRoom, didDisconnectWithError error: Error?) {
        logMessage(messageText: "Disconncted from room \(room.name), error = \(String(describing: error))")
        
        if !self.userInitiatedDisconnect, let uuid = room.uuid, let error = error {
            var reason = CXCallEndedReason.remoteEnded
            
            if (error as NSError).code != TVIError.roomRoomCompletedError.rawValue {
                reason = .failed
            }
            
            self.callKitProvider.reportCall(with: uuid, endedAt: nil, reason: reason)
        }
        
        self.cleanupRemoteParticipant()
        self.connectedToRoom(inRoom: false)
        
        self.room = nil
        self.callKitCompletionHandler = nil
        self.userInitiatedDisconnect = false
    }
    
    func room(_ room: TVIRoom, didFailToConnectWithError error: Error) {
        logMessage(messageText: "Failed to connect to room with error: \(error.localizedDescription)")
        
        self.callKitCompletionHandler!(false)
        self.room = nil
        self.connectedToRoom(inRoom: false)
        
    }
    
    func room(_ room: TVIRoom, participantDidConnect participant: TVIRemoteParticipant) {
        if (self.remoteParticipant == nil) {
            self.remoteParticipant = participant
            self.remoteParticipant?.delegate = self
        }
        
        logMessage(messageText: "Participant \(participant.identity) connected with \(participant.remoteAudioTracks.count) audio and \(participant.remoteVideoTracks.count) video tracks")
        
    }
    
    func room(_ room: TVIRoom, participantDidDisconnect participant: TVIRemoteParticipant) {
        if (self.remoteParticipant == participant) {
            self.cleanupRemoteParticipant()
            self.endCall()
        }
        logMessage(messageText: "Room \(room.name), Participant \(participant.identity) disconnected")
    }
}

// MARK: TVIRemoteParticipantDelegate
extension VideoCallViewController : TVIRemoteParticipantDelegate {
    
    func remoteParticipant(_ participant: TVIRemoteParticipant,
                           publishedVideoTrack publication: TVIRemoteVideoTrackPublication) {
        
        // Remote Participant has offered to share the video Track.
        
        logMessage(messageText: "Participant \(participant.identity) published video track")
    }
    
    func remoteParticipant(_ participant: TVIRemoteParticipant,
                           unpublishedVideoTrack publication: TVIRemoteVideoTrackPublication) {
        
        // Remote Participant has stopped sharing the video Track.
        
        logMessage(messageText: "Participant \(participant.identity) unpublished video track")
    }
    
    func remoteParticipant(_ participant: TVIRemoteParticipant,
                           publishedAudioTrack publication: TVIRemoteAudioTrackPublication) {
        
        // Remote Participant has offered to share the audio Track.
        
        logMessage(messageText: "Participant \(participant.identity) published audio track")
    }
    
    func remoteParticipant(_ participant: TVIRemoteParticipant,
                           unpublishedAudioTrack publication: TVIRemoteAudioTrackPublication) {
        
        // Remote Participant has stopped sharing the audio Track.
        
        logMessage(messageText: "Participant \(participant.identity) unpublished audio track")
    }
    
    func subscribed(to videoTrack: TVIRemoteVideoTrack,
                    publication: TVIRemoteVideoTrackPublication,
                    for participant: TVIRemoteParticipant) {
        
        // We are subscribed to the remote Participant's audio Track. We will start receiving the
        // remote Participant's video frames now.
        
        logMessage(messageText: "Subscribed to video track for Participant \(participant.identity)")
        
        if (self.remoteParticipant == participant) {
            videoTrack.addRenderer(self.remoteView!)
            //self.localVideoTrack!.addRenderer(self.previewView)

            self.delegate.remotePartnerConnected()

        }
    }
    
    func unsubscribed(from videoTrack: TVIRemoteVideoTrack,
                      publication: TVIRemoteVideoTrackPublication,
                      for participant: TVIRemoteParticipant) {
        
        // We are unsubscribed from the remote Participant's video Track. We will no longer receive the
        // remote Participant's video.
        
        logMessage(messageText: "Unsubscribed from video track for Participant \(participant.identity)")
        
        if (self.remoteParticipant == participant) {
            videoTrack.removeRenderer(self.remoteView!)
            self.stopPreview()
            self.delegate.remotePartnerDisconnected()
        }
    }
    
    func subscribed(to audioTrack: TVIRemoteAudioTrack,
                    publication: TVIRemoteAudioTrackPublication,
                    for participant: TVIRemoteParticipant) {
        
        // We are subscribed to the remote Participant's audio Track. We will start receiving the
        // remote Participant's audio now.
        
        logMessage(messageText: "Subscribed to audio track for Participant \(participant.identity)")
    }
    
    func unsubscribed(from audioTrack: TVIRemoteAudioTrack,
                      publication: TVIRemoteAudioTrackPublication,
                      for participant: TVIRemoteParticipant) {
        
        // We are unsubscribed from the remote Participant's audio Track. We will no longer receive the
        // remote Participant's audio.
        
        logMessage(messageText: "Unsubscribed from audio track for Participant \(participant.identity)")
    }
    
    func remoteParticipant(_ participant: TVIRemoteParticipant,
                           enabledVideoTrack publication: TVIRemoteVideoTrackPublication) {
        logMessage(messageText: "Participant \(participant.identity) enabled video track")
    }
    
    func remoteParticipant(_ participant: TVIRemoteParticipant,
                           disabledVideoTrack publication: TVIRemoteVideoTrackPublication) {
        logMessage(messageText: "Participant \(participant.identity) disabled video track")
    }
    
    func remoteParticipant(_ participant: TVIRemoteParticipant,
                           enabledAudioTrack publication: TVIRemoteAudioTrackPublication) {
        logMessage(messageText: "Participant \(participant.identity) enabled audio track")
    }
    
    func remoteParticipant(_ participant: TVIRemoteParticipant,
                           disabledAudioTrack publication: TVIRemoteAudioTrackPublication) {
        logMessage(messageText: "Participant \(participant.identity) disabled audio track")
    }
    
    func failedToSubscribe(toAudioTrack publication: TVIRemoteAudioTrackPublication,
                           error: Error,
                           for participant: TVIRemoteParticipant) {
        logMessage(messageText: "FailedToSubscribe \(publication.trackName) audio track, error = \(String(describing: error))")
    }
    
    func failedToSubscribe(toVideoTrack publication: TVIRemoteVideoTrackPublication,
                           error: Error,
                           for participant: TVIRemoteParticipant) {
        logMessage(messageText: "FailedToSubscribe \(publication.trackName) video track, error = \(String(describing: error))")
    }
}


// MARK: TVIVideoViewDelegate
extension VideoCallViewController : TVIVideoViewDelegate {
    func videoView(_ view: TVIVideoView, videoDimensionsDidChange dimensions: CMVideoDimensions) {
        self.view.setNeedsLayout()
    }
}

// MARK: TVICameraSourceDelegate
extension VideoCallViewController : TVICameraSourceDelegate {
    func cameraSource(_ source: TVICameraSource, didFailWithError error: Error) {
        logMessage(messageText: "Camera source failed with error: \(error.localizedDescription)")
    }
}

extension VideoCallViewController : CXProviderDelegate {
    
    func providerDidReset(_ provider: CXProvider) {
        logMessage(messageText: "providerDidReset:")
        
        // AudioDevice is enabled by default
        AppDelegateAccessor.twilioAudioDevice.isEnabled = true
        
        room?.disconnect()
    }
    
    func providerDidBegin(_ provider: CXProvider) {
        logMessage(messageText: "providerDidBegin")
    }
    
    func provider(_ provider: CXProvider, didActivate audioSession: AVAudioSession) {
        logMessage(messageText: "provider:didActivateAudioSession:")
        
        AppDelegateAccessor.twilioAudioDevice.isEnabled = true
    }
    
    func provider(_ provider: CXProvider, didDeactivate audioSession: AVAudioSession) {
        logMessage(messageText: "provider:didDeactivateAudioSession:")
    }
    
    func provider(_ provider: CXProvider, timedOutPerforming action: CXAction) {
        logMessage(messageText: "provider:timedOutPerformingAction:")
    }
    
    func provider(_ provider: CXProvider, perform action: CXStartCallAction) {
        logMessage(messageText: "provider:performStartCallAction:")
        
        /*
         * Configure the audio session, but do not start call audio here, since it must be done once
         * the audio session has been activated by the system after having its priority elevated.
         */
        
        // Stop the audio unit by setting isEnabled to `false`.
        AppDelegateAccessor.twilioAudioDevice.isEnabled = false;
        
        // Configure the AVAudioSession by executign the audio device's `block`.
        AppDelegateAccessor.twilioAudioDevice.block()
        
        callKitProvider.reportOutgoingCall(with: action.callUUID, startedConnectingAt: nil)
        
        performRoomConnect(uuid: action.callUUID, roomName: action.handle.value) { (success) in
            if (success) {
                provider.reportOutgoingCall(with: action.callUUID, connectedAt: Date())
                action.fulfill()
            } else {
                action.fail()
            }
        }
    }
    
    func provider(_ provider: CXProvider, perform action: CXAnswerCallAction) {
        logMessage(messageText: "provider:performAnswerCallAction:")
        
        /*
         * Configure the audio session, but do not start call audio here, since it must be done once
         * the audio session has been activated by the system after having its priority elevated.
         */
        
        // Stop the audio unit by setting isEnabled to `false`.
        AppDelegateAccessor.twilioAudioDevice.isEnabled = false;
        
        // Configure the AVAudioSession by executign the audio device's `block`.
        AppDelegateAccessor.twilioAudioDevice.block()
        
        
        performRoomConnect(uuid: action.callUUID, roomName: self.roomName) { (success) in
            if (success) {
                action.fulfill(withDateConnected: Date())
            } else {
                action.fail()
            }
        }
    }
    
    func provider(_ provider: CXProvider, perform action: CXEndCallAction) {
        NSLog("provider:performEndCallAction:")

        // AudioDevice is enabled by default
        AppDelegateAccessor.twilioAudioDevice.isEnabled = true
        if(self.room != nil && self.room?.state == TVIRoomState.connected) {
            room?.disconnect()
        }
        else {
            self.rejectCall(callId: action.callUUID.uuidString)
        }
        
        
        action.fulfill()
    }
    
    func provider(_ provider: CXProvider, perform action: CXSetMutedCallAction) {
        NSLog("provier:performSetMutedCallAction:")
        
        muteAudio(isMuted: action.isMuted)
        
        action.fulfill()
    }
    
    func provider(_ provider: CXProvider, perform action: CXSetHeldCallAction) {
        NSLog("provier:performSetHeldCallAction:")
        
        let cxObserver = callKitCallController.callObserver
        let calls = cxObserver.calls
        
        guard let call = calls.first(where:{$0.uuid == action.callUUID}) else {
            action.fail()
            return
        }
        
        if call.isOnHold {
            holdCall(onHold: false)
        } else {
            holdCall(onHold: true)
        }
        action.fulfill()
    }
    
    func prepareLocalMedia() {
        // We will share local audio and video when we connect to the Room.

        // Create an audio track.
        if (localAudioTrack == nil) {
            localAudioTrack = TVILocalAudioTrack.init()
            
            if (localAudioTrack == nil) {
                logMessage(messageText: "Failed to create audio track")
            }
        }
        
        // Create a video track which captures from the camera.
    }
    
}

extension VideoCallViewController: RecordingButtonDelegate {
    // RecordingButtonDelegate Methods
    func didStartCapture() {
        // call when capturing starts.
        print("Start Capture")
        
        if (localAudioTrack == nil) {
            localAudioTrack = TVILocalAudioTrack.init()
            
            if (localAudioTrack == nil) {
                logMessage(messageText: "Failed to create audio track")
            }
        }
        
        if self.localVideoRecorder == nil {
            self.localVideoRecorder = TwilioVideoRecorder(videoTrack: self.localVideoTrack!, audioTrack: self.localAudioTrack!, identifier: (self.localAudioTrack?.name)!)
        }

    }
    
    func didEndCapture() {
        // call when capturing end.
        print("Stop Capture")
        
        if let recorder = self.localVideoRecorder {
            recorder.stopRecording { (videoURL) in

                self.room?.disconnect()
                
                var leaveMessage = LeaveMessage()
                leaveMessage.type = LeaveMessage.LeaveMessageType.video
                leaveMessage.contentURL = recorder.videoURL
                
                self.localVideoRecorder = nil

                if(self.didCreateMessage != nil) {
                    DispatchQueue.main.async {
                        self.didCreateMessage!(leaveMessage, true)
                    }
                }

            }
            
        }
        
    }

}





