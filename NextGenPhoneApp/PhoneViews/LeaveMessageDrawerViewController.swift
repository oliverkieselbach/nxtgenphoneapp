//
//  LeaveMessageDrawerViewController.swift
//  NextGenPhoneApp
//
//  Created by Kieselbach, Oliver on 14.11.18.
//  Copyright © 2018 Kieselbach, Oliver. All rights reserved.
//

import UIKit
import Pulley
import ScrollableSegmentedControl
import SwiftyCam
import AVKit
import NextGenPhoneFramework


struct LeaveMessage {
    enum LeaveMessageType: String {
        case video = "video/mp4"
        case voice = "audio/m4a"
        case text = "text"
    }
    
    var type: LeaveMessageType!
    var content: String?
    var contentURL: URL?
    
}

class AssistantViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .blue
    }

    
}

class LeaveAMessageViewController: UIPageViewController {
    
    var messageVC: UIViewController!
    var assistantVC: AssistantViewController! = AssistantViewController()
    public var didCancel: (() -> Void)?

    
    override init(transitionStyle style: UIPageViewController.TransitionStyle, navigationOrientation: UIPageViewController.NavigationOrientation, options: [UIPageViewController.OptionsKey : Any]? = nil) {
        super.init(transitionStyle: .scroll, navigationOrientation: navigationOrientation, options: options)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let cancelButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(self.cancelAction))
        self.navigationItem.rightBarButtonItem = cancelButton

        self.dataSource = self
        
        setViewControllers([self.messageVC], direction: .forward, animated: true, completion: nil)
                
        
    }
    
    @objc func cancelAction() {
        if(self.didCancel != nil) {
            self.didCancel!()
        }
        self.dismiss(animated: true) {
        }
    }
    
}

extension LeaveAMessageViewController: UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        return viewController == self.messageVC ? self.assistantVC : self.messageVC
     
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController?
    {
        return viewController == self.messageVC ? self.assistantVC : self.messageVC
    }
}


protocol VoiceMessageViewDelegate {
    
    func didFinishAudioRecording(url: URL)
    
}
class VoiceMessageViewController: VoiceRecorderViewController {
    
    public var didCreateMessage: ((LeaveMessage, Bool) -> Void)?

    var videoRecorderSession: AVCaptureSession!
    
    var delegate: VoiceMessageViewDelegate?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        
        super.audioRecorderDidFinishRecording(recorder, successfully: flag)

        let leaveMessage = LeaveMessage(type: .voice, content: nil, contentURL: self.audioURL!)
        if(self.didCreateMessage != nil) {
            self.didCreateMessage!(leaveMessage, true)
        }
    }
    
}


class VideoMessageViewController: SwiftyCamViewController, RecordingButtonDelegate {
    
    public var didCreateMessage: ((LeaveMessage, Bool) -> Void)?


    
    var recordButton: RecordingButton = {
        
        let button = RecordingButton(type: .custom)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.heightAnchor.constraint(equalToConstant: 100).isActive = true
        button.widthAnchor.constraint(equalToConstant: 100).isActive = true
        button.outlineColor = UIColor(red: 230/255, green: 230/255, blue: 230/255, alpha: 230/255)
        button.isHidden = true
        return button
    }()
    
    var closeButton: UIButton = {
        
        var button = UIButton(type: .custom)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = UIColor(red: 230/255, green: 230/255, blue: 230/255, alpha: 1)
        button.heightAnchor.constraint(equalToConstant: 30).isActive = true
        button.widthAnchor.constraint(equalToConstant: 30).isActive = true
        button.layer.cornerRadius = 15
        button.setImage(UIImage(imageLiteralResourceName: "x").withRenderingMode(UIImage.RenderingMode.alwaysTemplate), for: .normal)
        button.tintColor = .black
        button.imageEdgeInsets = UIEdgeInsets.init(top: 10, left: 10, bottom: 10, right: 10)
        
        return button
        
    }()

    
    override func viewDidLoad() {
        self.view.backgroundColor = .white
        defaultCamera = .front
        videoQuality = .medium
        videoGravity = .resizeAspectFill
        maximumVideoDuration = 20
        swipeToZoom = false
        cameraDelegate = self


        super.viewDidLoad()

        self.view.addSubview(self.closeButton)
        self.closeButton.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 7).isActive = true
        self.closeButton.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -15).isActive = true
        self.closeButton.isHidden = true
                
        self.view.addSubview(self.recordButton)
        self.recordButton.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0).isActive = true
        self.recordButton.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -30).isActive = true
        self.recordButton.recordingDuration = 20;
        self.recordButton.delegate = self
        self.recordButton.addTarget(self, action: #selector(self.startRecording), for: .touchUpInside)
        
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("Video Recorder will appear")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        print("Video Recorder will disappear")
    }
    
    @objc func startRecording() {
        self.recordButton.record()
    }

    
    // RecordingButtonDelegate Methods
    func didStartCapture() {
        // call when capturing starts.
        print("Start Capture")
        if(!self.isVideoRecording) {
            startVideoRecording()
        }

    }
    
    func didEndCapture() {
        // call when capturing end.
        print("Stop Capture")
        if(self.isVideoRecording) {
            stopVideoRecording()
        }

    }

}
extension VideoMessageViewController: SwiftyCamViewControllerDelegate {
    
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didFailToRecordVideo error: Error) {
        print("did fail to record video")
    }
    
    func swiftyCamSessionDidStartRunning(_ swiftyCam: SwiftyCamViewController) {
        print("did start running")
    }
    
    func swiftyCamSessionDidStopRunning(_ swiftyCam: SwiftyCamViewController) {
        print("did stop running")
    }

    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didBeginRecordingVideo camera: SwiftyCamViewController.CameraSelection) {
        print("did begin recording video")
    }
    
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didFinishRecordingVideo camera: SwiftyCamViewController.CameraSelection) {
        print("did finish recording video")
    }
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didFinishProcessVideoAt url: URL) {
        let leaveMessage = LeaveMessage(type: .video, content: nil, contentURL: url)
        if(self.didCreateMessage != nil) {
            self.didCreateMessage!(leaveMessage, true)
        }
        
    }
    
}



class TextMessageAccessoryView: UIView {
    
    var sendMessageButton: UIButton = {
        
        let b = UIButton(type: .custom)
        b.tag = 1
        b.translatesAutoresizingMaskIntoConstraints = false
        b.layer.cornerRadius = 5
        b.backgroundColor = .white
        b.tintColor = UIColor.white

        b.setAttributedTitle(NSAttributedString(string: "", attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 18, weight: UIFont.Weight.bold), NSAttributedString.Key.foregroundColor: UIColor.black]), for: .normal)
        
        b.setAttributedTitle(NSAttributedString(string: "Send", attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.bold), NSAttributedString.Key.foregroundColor: UIColor.white]), for: .selected)

        b.contentEdgeInsets = UIEdgeInsets(top: 10, left: 20, bottom: 10, right: 20)
        //b.setImage(UIImage(imageLiteralResourceName: "unmuted").withRenderingMode(UIImage.RenderingMode.alwaysTemplate), for: .normal)
        //b.titleEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
        //b.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10)
        return b
        
    }()

    

    func setup() {
        
        self.backgroundColor = .white
        
        self.addSubview(self.sendMessageButton)
        
        self.sendMessageButton.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        self.sendMessageButton.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -15).isActive = true

        
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
}

class TextMessageViewController: UIViewController {
    
    var textAccessoryView: TextMessageAccessoryView!
    
    var scrollView: UIScrollView!
    
    var textView: UITextView = {

        let v = UITextView(frame: .zero)
        v.translatesAutoresizingMaskIntoConstraints = false
        v.backgroundColor = UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1)
        v.textColor = .black
        v.contentInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        v.layer.cornerRadius = 10
        v.font = UIFont.systemFont(ofSize: 17)
        
        v.text = "Hallo, ich würde gerne für morgen oder übermorgen einen Termin haben. Ich weiss aber leider nicht genau wann ich kann und ob ich überhaupt erscheinen werde. Bis dahin verbleibe ich mit freundlichen Grüssen"
        
        
        
        return v
        
    }()
    
    public var didCreateMessage: ((LeaveMessage, Bool) -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white

        self.view.addSubview(self.textView)
        self.textView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 15).isActive = true
        self.textView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 15).isActive = true
        self.textView.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -15).isActive = true
        self.textView.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.4).isActive = true

        self.textAccessoryView = TextMessageAccessoryView(frame: CGRect(x: 0, y: 0, width: 100, height: 50))
        self.textAccessoryView.setup()
        self.textView.inputAccessoryView = self.textAccessoryView
        self.textView.delegate = self
        
        self.textAccessoryView.sendMessageButton.addTarget(self, action: #selector(self.sendTextMessage), for: .touchDown)


    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.textView.becomeFirstResponder()
    }
    
    @objc func closeAction() {
        self.dismiss(animated: true) {
        }
    }
    
    @objc func sendTextMessage() {
        self.textView.resignFirstResponder()
        
        let leaveMessage = LeaveMessage(type: .text, content: self.textView.text, contentURL: nil)
        if(self.didCreateMessage != nil) {
            self.didCreateMessage!(leaveMessage, true)
        }
    }

    
}

extension TextMessageViewController: UITextViewDelegate {
    
    func textViewDidChange(_ textView: UITextView) {
        self.textAccessoryView.sendMessageButton.isSelected = textView.text.count != 0 ? true : false
        self.textAccessoryView.sendMessageButton.backgroundColor = textView.text.count != 0 ? UIColor(0x3498db) : .white
    }
    
}

class CreateMessageView: UIView {
    
    public var didCreateMessage: ((LeaveMessage, Bool) -> Void)?

    
    var scrollSegmentControl: ScrollableSegmentedControl!
    var scrollView: UIScrollView! = {
        
        let v = UIScrollView(frame: .zero)
        v.isPagingEnabled = true
        v.isScrollEnabled = true
        v.translatesAutoresizingMaskIntoConstraints = false
        
        return v
    }()
    
    var videoMessageVC: VideoMessageViewController = VideoMessageViewController()
    var textMessageVC: TextMessageViewController = TextMessageViewController()
    var voiceMessageVC: VoiceMessageViewController = VoiceMessageViewController()

    
    func setup() {
        
        self.scrollSegmentControl = ScrollableSegmentedControl(frame: CGRect.zero)
        scrollSegmentControl.scrollView = self.scrollView
        
        self.addSubview(self.scrollSegmentControl)
        self.addSubview(self.scrollView)
        self.scrollView.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
        self.scrollView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 0).isActive = true
        self.scrollView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 0).isActive = true
        self.scrollView.bottomAnchor.constraint(equalTo: self.scrollSegmentControl.topAnchor, constant: 0).isActive = true
        //self.scrollView.delegate = self
        
        
        self.scrollSegmentControl.translatesAutoresizingMaskIntoConstraints = false
        self.scrollSegmentControl.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 0).isActive = true
        self.scrollSegmentControl.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 0).isActive = true
        self.scrollSegmentControl.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -80).isActive = true
        self.scrollSegmentControl.heightAnchor.constraint(equalToConstant: 50).isActive = true
        scrollSegmentControl.segmentStyle = .textOnly
        scrollSegmentControl.insertSegment(withTitle: "Video", at: 0)
        scrollSegmentControl.insertSegment(withTitle: "Voice", at: 1)
        scrollSegmentControl.insertSegment(withTitle: "Text", at: 2)
        scrollSegmentControl.backgroundColor = .white
        scrollSegmentControl.segmentContentColor = .lightGray
        scrollSegmentControl.selectedSegmentContentColor = .black
        
        
        scrollSegmentControl.setTitleTextAttributes([NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16), NSAttributedString.Key.foregroundColor: UIColor.lightGray], for: .normal)
        scrollSegmentControl.setTitleTextAttributes([NSAttributedString.Key.font: UIFont.systemFont(ofSize: 18, weight: UIFont.Weight.bold), NSAttributedString.Key.foregroundColor: UIColor.black], for: .selected)
        
        scrollSegmentControl.addTarget(self, action: #selector(self.scrollSegmentChanged(sender:)), for: .valueChanged)

        self.configureViewController()

    }

    func configureViewController() {
        videoMessageVC.title = "Video"
        videoMessageVC.view.backgroundColor = .white
        videoMessageVC.cameraDelegate = self

        voiceMessageVC.title = "Voice"
        voiceMessageVC.delegate = self
        voiceMessageVC.videoRecorderSession = videoMessageVC.session

        textMessageVC.title = "Text"
        textMessageVC.view.backgroundColor = .white
        
        
        self.textMessageVC.textAccessoryView.sendMessageButton.addTarget(self, action: #selector(self.sendTextMessage), for: .touchDown)
    }
    
    @objc func sendTextMessage() {
        self.textMessageVC.textView.resignFirstResponder()
        
        let leaveMessage = LeaveMessage(type: .text, content: self.textMessageVC.textView.text, contentURL: nil)
        if(self.didCreateMessage != nil) {
            self.didCreateMessage!(leaveMessage, true)
        }
    }
    
    @objc func selectMessageTypeButtonTapped(sender: UIButton) {
        self.moveToPage(page: sender.tag)
    }

    
    @objc func scrollSegmentChanged(sender: ScrollableSegmentedControl) {
        
        if(sender.selectedSegmentIndex == 2) {
            self.textMessageVC.textView.becomeFirstResponder()
        }
        else {
            self.textMessageVC.textView.resignFirstResponder()
        }
        
    }
    
    func moveToPage(page: Int) {

        self.scrollSegmentControl.selectedSegmentIndex = page
        self.scrollView.contentOffset.x = self.scrollView.frame.width * CGFloat(page)

        /*
        if(page != 0) {
            self.videoMessageVC.session.stopRunning()
        }
        else {
            self.videoMessageVC.session.startRunning()
        }
        */
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.scrollView.contentSize = CGSize(width: self.frame.width * 3, height: self.scrollView.frame.size.height)
        
        var controllerArray : [UIViewController] = [videoMessageVC, voiceMessageVC, textMessageVC]

        for i in 0 ..< controllerArray.count {
            controllerArray[i].view.frame = CGRect(x: self.frame.width * CGFloat(i), y: 0, width: self.frame.width, height: self.scrollView.frame.size.height)
            scrollView.addSubview(controllerArray[i].view)
        }
        
    }
}

extension CreateMessageView: SwiftyCamViewControllerDelegate {
    
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didFinishProcessVideoAt url: URL) {
        let leaveMessage = LeaveMessage(type: .video, content: nil, contentURL: url)
        if(self.didCreateMessage != nil) {
            self.didCreateMessage!(leaveMessage, true)
        }

    }
    
}

extension CreateMessageView: VoiceMessageViewDelegate {
    
    func didFinishAudioRecording(url: URL) {
        let leaveMessage = LeaveMessage(type: .voice, content: nil, contentURL: url)
        if(self.didCreateMessage != nil) {
            self.didCreateMessage!(leaveMessage, true)
        }
    }
    
}


