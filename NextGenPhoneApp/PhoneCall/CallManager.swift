//
//  CallManager.swift
//  NextGenPhoneApp
//
//  Created by Kieselbach, Oliver on 31.07.18.
//  Copyright © 2018 Kieselbach, Oliver. All rights reserved.
//

import Foundation
import CallKit
import PushKit
//import TwilioVoice
import AVFoundation
import NextGenPhoneFramework

let baseURLString = "https://silver-cougar-5778.twil.io"
// If your token server is written in PHP, accessTokenEndpoint needs .php extension at the end. For example : /accessToken.php
let accessTokenEndpoint = "/accessToken"
let identity = "x4915112345678"
let twimlParamTo = "To"

/*
class Call: NSObject {
    
    var uuid: UUID!
    var handle: String!
    
    init(uuid: UUID, handle: String) {
        super.init()
        self.uuid = uuid
        self.handle = handle
    }
    
}
*/

class CallManager: NSObject {
    

    var cxProvider: CXProvider!
    var cxCallController: CXCallController!
    
    var accessToken: String! = nil
    var deviceToken: String! = nil {

        didSet {
            
            
        }
        
    }
        
    var pushNotificationCompletion: () -> Void = {}
    var callAudioSession: AVAudioSession!
    var callKitCompletionCallback: ((Bool)->Swift.Void?)? = nil
    var callInitBlock: (()->Swift.Void?)? = nil

    static func transformCallerHandle(handle: String) -> String {
        var caller = handle
        if(handle.contains("client:")) {
            let index = handle.index((handle.startIndex), offsetBy: "client:".count)
            caller = String(handle[index...])
            if(caller.first == "x") {
                caller = caller.replacingOccurrences(of: "x", with: "+")
            }
        }
        else {
            if(caller.first == "+") {
                caller = caller.replacingOccurrences(of: "+", with: "x")
            }
        }

        
        return caller
    }
    
    override init() {
        super.init()

        let localizedName = NSLocalizedString("NextGenPhoneApp", comment: "Name of application")
        let cxProviderConfig = CXProviderConfiguration(localizedName: localizedName)
        cxProviderConfig.supportsVideo = false
        cxProviderConfig.supportedHandleTypes = [.phoneNumber, .generic]
        
        self.cxProvider = CXProvider.init(configuration: cxProviderConfig)
        self.cxProvider.setDelegate(self, queue: nil)
        
        self.cxCallController = CXCallController()
        
    }
    
    func configureAudioSession() {
        // See https://forums.developer.apple.com/thread/64544
        let session = AVAudioSession.sharedInstance()
        try? session.setCategory(AVAudioSession.Category(rawValue: convertFromAVAudioSessionCategory(AVAudioSession.Category.playAndRecord)), mode: .spokenAudio)
        try? session.setMode(AVAudioSession.Mode.voiceChat)
        try? session.setPreferredSampleRate(44100.0)
        try? session.setPreferredIOBufferDuration(0.005)
    }
    
    func fetchAccessToken() -> String? {
        if(self.accessToken == nil) {
            let endpointWithIdentity = String(format: "%@?identity=%@", accessTokenEndpoint, identity)
            guard let accessTokenURL = URL(string: baseURLString + endpointWithIdentity) else {
                return nil
            }
            
            self.accessToken = try? String.init(contentsOf: accessTokenURL, encoding: .utf8)
        }
        
        return self.accessToken
    }
    
    func registerDevice() {

        if(self.deviceToken != nil) {
            ConversationKit.shared.db.collection("registereddevices").document(ConversationKit.shared.currentUser.uid).setData(["deviceToken_ios" : deviceToken])
        }

    }
    
    func toggleSpeaker(on: Bool) {

        NSLog("CallManager.toggleSpeaker")
        do {
            if(on) {
                try callAudioSession.overrideOutputAudioPort(.speaker)
            }
            else {
                try callAudioSession.overrideOutputAudioPort(.none)
                
            }
        } catch {
            NSLog(error.localizedDescription)
        }
        
    }
    
    
    func handleIncomingCallFromPush(payload: PKPushPayload, pushEventCompletion: @escaping () -> Void) {
        
        self.pushNotificationCompletion = pushEventCompletion

        self.configureAudioSession()
        self.reportIncomingCall(uuid: UUID(), handle: "Oliver")
        
    }
    
    func reportIncomingCall(uuid: UUID, handle: String) {
        
        let update = CXCallUpdate()
        update.remoteHandle = CXHandle(type: .generic, value: handle)
        update.hasVideo = true
        update.supportsDTMF = false
        update.supportsHolding = true
        update.supportsGrouping = false
        update.supportsUngrouping = false
        

        self.cxProvider.reportNewIncomingCall(with: uuid, update: update) { (error) in
            if(error == nil) {
            }
            else {
                NSLog((error?.localizedDescription)!)
            }
        }
    }
    
    func performEndCallAction(uuid: UUID) {
        NSLog("CallManager.performEndCallAction")
        let endCallAction = CXEndCallAction(call: uuid)
        let transaction = CXTransaction(action: endCallAction)
        
        self.cxCallController.request(transaction) { error in
            if let error = error {
                NSLog("EndCallAction transaction request failed: \(error.localizedDescription).")
                return
            }
            
            NSLog("EndCallAction transaction request successful")
        }

    }
    
    
    func XanswerCall(action: CXAnswerCallAction, initCallHandler: @escaping () -> Swift.Void, completionHandler: @escaping (Bool) -> Swift.Void) {
        NSLog("CallManager.answerCall")
        
        self.callInitBlock = initCallHandler
        self.callKitCompletionCallback = completionHandler

    }
    
    func performStartCallAction(handle: String) {

        self.configureAudioSession()

        let startCallAction = CXStartCallAction(call: UUID(), handle: CXHandle(type: .phoneNumber, value: handle))
        let transaction = CXTransaction(action: startCallAction)

        self.cxCallController.request(transaction) { (error) in
            if(error != nil) {
                NSLog((error?.localizedDescription)!)
            }
        }
    }

    func XstartCall(action: CXStartCallAction, initCallHandler: @escaping () -> Swift.Void, completionHandler: @escaping (Bool) -> Swift.Void) {
        NSLog("CallManager.startCall")

        self.callInitBlock = initCallHandler
        self.callKitCompletionCallback = completionHandler
        
        
    }
    
    func endCall(action: CXEndCallAction) {
        NSLog("CallManager.endCall")
        
        action.fulfill(withDateEnded: Date())

    }
    
    func startAudio() {
        NSLog("CallManager.startAudio")
    }
    
    func stopAudio() {
        NSLog("CallManager.stopAudio")
    }
}

// MARK: CXProviderDelegate
extension CallManager: CXProviderDelegate {
    
    func providerDidReset(_ provider: CXProvider) {
        NSLog("CallManager.providerDidReset")
    }
    
    func provider(_ provider: CXProvider, perform action: CXAnswerCallAction) {
        NSLog("CallManager.CXAnswerCallAction")

        self.pushNotificationCompletion()
        action.fulfill()

    }
    
    func provider(_ provider: CXProvider, perform action: CXStartCallAction) {
        NSLog("CallManager.CXStartCallAction")

        self.cxProvider.reportOutgoingCall(with: action.callUUID, connectedAt: Date())
        action.fulfill()

    }
    
    func provider(_ provider: CXProvider, perform action: CXEndCallAction) {
        NSLog("CallManager.CXEndCallAction")

        self.endCall(action: action)
    }

    func provider(_ provider: CXProvider, didActivate audioSession: AVAudioSession) {
        NSLog("CallManager:didActivateAudioSession:")
        self.callAudioSession = audioSession

        print("AudioSession Category: " + convertFromAVAudioSessionCategory(self.callAudioSession.category))
        print("AudioSession Mode: " + convertFromAVAudioSessionMode(self.callAudioSession.mode))
        print("AudioSession SampleRate: " + String(self.callAudioSession.preferredSampleRate))
        print("AudioSession IOBufferDuration: " + String(self.callAudioSession.preferredIOBufferDuration))
        //print("AudioSession OutputDataSource: " + (self.callAudioSession.outputDataSource?.dataSourceName) ?? "")

        
        self.startAudio()
    }
    
    func provider(_ provider: CXProvider, didDeactivate audioSession: AVAudioSession) {
        NSLog("CallManager:didDeactivateAudioSession:")

        self.stopAudio()
        self.callAudioSession = nil
        
    }
    
    func provider(_ provider: CXProvider, timedOutPerforming action: CXAction) {
        NSLog("provider:timedOutPerformingAction:")
    }
}


// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromAVAudioSessionCategory(_ input: AVAudioSession.Category) -> String {
	return input.rawValue
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromAVAudioSessionMode(_ input: AVAudioSession.Mode) -> String {
	return input.rawValue
}
