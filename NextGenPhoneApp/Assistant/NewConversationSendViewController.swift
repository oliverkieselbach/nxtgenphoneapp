//
//  AssistantIntentParameterViewController.swift
//  NextGenPhoneApp
//
//  Created by Kieselbach, Oliver on 29.01.19.
//  Copyright © 2019 Kieselbach, Oliver. All rights reserved.
//

import UIKit
import Eureka
import Lottie
import FirebaseFirestore
import NextGenPhoneFramework



class IntentFooter: UIView {
    
    var sendButton: UIButton = {
        
        let button = UIButton(type: .custom)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Send", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = UIColor.actionColor(color: .blue)
        button.heightAnchor.constraint(equalToConstant: 40).isActive = true
        button.titleLabel!.font = UIFont.systemFont(ofSize: 15, weight: .regular)
        button.alpha = 1
        button.layer.cornerRadius = 10
        
        return button
        
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        
        self.addSubview(self.sendButton)
        //self.sendButton.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        self.sendButton.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 15).isActive = true
        self.sendButton.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -15).isActive = true
        self.sendButton.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        //self.sendButton.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.5).isActive = true
        self.sendButton.backgroundColor = self.tintColor
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

class MessageSendHeaderView: UIView {
    
    var message: CKMessage! {
        didSet {
            
            self.contact = CKContact(personEmbedded: message.receiverDetails)
            
        }
    }
    

    
    var contact: CKContact! {
        didSet {
            self.avatar.contact = contact
            self.name.text = contact.name
        }
    }
    
    var avatar: CKContactView! = {
        
        let view = CKContactView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.heightAnchor.constraint(equalToConstant: 80).isActive = true
        view.widthAnchor.constraint(equalToConstant: 80).isActive = true
        
        return view
        
    }()
    
    var name: UILabel! = {
        
        var label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 20, weight: .bold)
        label.textAlignment = .center
        label.textColor = .black
        
        label.numberOfLines = 2
        
        return label
        
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubview(self.avatar)
        self.addSubview(self.name)
        
        self.avatar.bottomAnchor.constraint(equalTo: self.centerYAnchor, constant: 2).isActive = true
        self.avatar.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        
        self.name.topAnchor.constraint(equalTo: self.avatar.bottomAnchor, constant: 5).isActive = true
        self.name.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 15).isActive = true
        self.name.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -15).isActive = true
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}


class NewConversationSendViewController: FormViewController {
    
    var message: CKMessage! {
        didSet {
            self.header.message = message
        }
    }
    
    var header: MessageSendHeaderView = {

        let view = MessageSendHeaderView(frame: CGRect(x: 0, y: 0, width: UIScreenWidth, height: 190))
        //view.backgroundColor = .green
        return view
        
    }()

    var footer: IntentFooter = {
        
        let view = IntentFooter(frame: CGRect(x: 0, y: 0, width: UIScreenWidth, height: 100))
        view.translatesAutoresizingMaskIntoConstraints = false
        
        
        return view
        
    }()
    
    public typealias ResultClosure = (Bool, CKMessage) -> ()
    
    public var resultHandler: ResultClosure!


    

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "New Conversation"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelAction))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Send", style: .plain, target: self, action: #selector(self.sendAction))

        
        self.tableView.tableHeaderView = header
        self.tableView.backgroundColor = .white
        

        self.createForm()
        
        // Do any additional setup after loading the view.
    }
    
    override func insertAnimation(forSections sections: [Section]) -> UITableView.RowAnimation {
        return .none
    }
    
    override func insertAnimation(forRows rows: [BaseRow]) -> UITableView.RowAnimation {
        return .none
    }
    
    @objc func cancelAction() {
        let docId = self.message.topicDetails.formDocumentId
        if(docId != "") {
            self.dismiss(animated: true) {
            }
        }
        ConversationKit.shared.db.collection("conversations").document(self.message.conversationId).collection("forms").document(docId).delete { (error) in
            self.dismiss(animated: true) {
            }
        }
        
    }
    
    func createForm() {
        
        let formDocRef = ConversationKit.shared.db.collection("conversations").document(self.message.conversationId).collection("forms").document(self.message.topicDetails.formDocumentId).getDocument { (documentSnapshot, error) in
            
            guard let document = documentSnapshot else {
                print(error?.localizedDescription ?? "Error reading form data")
                return
            }
            
            let data = document.data()
            var formParameter: [FormParameter] = []
            var formHeader = FormHeader(title: "", description: "")
            if(data != nil && data!["form"] != nil) {
                let form = data!["form"] as! [String:Any]
                let params = form["parameter"] as! [[String:Any]]
                for param in params {
                    
                    let value = param as! [String:Any]
                    let id = value["id"] == nil ? "" : value["id"] as! String
                    let name = value["name"] == nil ? "" : value["name"] as! String
                    let type = value["type"] == nil ? FormParamType.string : FormParamType(rawValue: value["type"] as! String)
                    
                    let formParam = FormParameter(id: id, name: name, type: type)
                    formParameter.append(formParam)
                }
                formHeader.title = form["title"] == nil ? "" : form["title"] as! String
                formHeader.description = form["description"] == nil ? "" : form["description"] as! String
            }

            
            self.form = FormFactory.createForm(form: self.form, formParamater: formParameter, formHeader: formHeader, formDocument: document)
            
        }


        //self.tableView.tableFooterView = footer

        
    }
    
    @objc func sendAction() {
        
        self.resultHandler(true, self.message)
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
