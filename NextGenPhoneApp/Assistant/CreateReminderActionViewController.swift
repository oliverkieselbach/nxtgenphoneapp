//
//  CreateReminderActionViewController.swift
//  NextGenPhoneApp
//
//  Created by Kieselbach, Oliver on 12.03.19.
//  Copyright © 2019 Kieselbach, Oliver. All rights reserved.
//

import UIKit
import Eureka
import EventKit
import NextGenPhoneFramework


class CreateReminderActionViewController: FormViewController {

    var message: CKMessage! {
        didSet {
            self.createForm()
        }
    }
    
    var dueDate: Date! = Date() {
        didSet {
            let row = form.rowBy(tag: "dueDate") as! DateTimeInlineRow
            row.value = dueDate
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.tableView.backgroundColor = .white
        
    }
    
    func createForm() {

        let section = Section("Reminder Subject")

        section <<< TextAreaRow("subject", { (row) in
            row.value = "Call back "+message.senderDetails.name
        }) <<< DateTimeInlineRow("dueDate", { (row) in
            row.title = "Due Date"
            row.value = Date()
        }) <<< ButtonRow("save", { (row) in
            row.title = "Save As Reminder"
        }).onCellSelection({ (cell, row) in
            self.createReminder()
        })
        

        form.append(section)

    }
    
    func createReminder() {
        let s = EKEventStore.init()
        s.requestAccess(to: EKEntityType.reminder) { (comp, e) -> Void in
            print(comp,e)
            let r = EKReminder.init(eventStore: s)
            r.title = (self.form.rowBy(tag: "subject") as! TextAreaRow).value
            let d = (self.form.rowBy(tag: "dueDate") as! DateTimeInlineRow).value
            let a = EKAlarm.init(absoluteDate: d!)
            r.calendar = s.defaultCalendarForNewReminders()
            r.notes = self.message.senderDetails.getOpenURL().absoluteString
            
            r.addAlarm(a)
            do{
                try s.save(r, commit: true)
                print(r, "saved")
                DispatchQueue.main.async {
                    //(self.parent as! MessageIntentResponsePageViewController).cancelAction()
                }
            } catch {
                print("couldnt save")
                DispatchQueue.main.async {
                    //(self.parent as! MessageIntentResponsePageViewController).cancelAction()
                }
            }
        }
    }
    

}
