//
//  IntentDetector.swift
//  NextGenPhoneApp
//
//  Created by Kieselbach, Oliver on 28.01.19.
//  Copyright © 2019 Kieselbach, Oliver. All rights reserved.
//

import Foundation
import Eureka
import FirebaseFirestore
import FirebaseFunctions
import NextGenPhoneFramework



class IntentDetector {
    
    public typealias ResultClosure = (Bool, CKIntent?) -> ()
    
    public var resultHandler: ResultClosure?
    


    func detect(transcript: String, conversationId: String, originalIntentId: String) {
        
        let functions = Functions.functions()
        functions.httpsCallable("detectIntent").call(["query": transcript, "conversationId": conversationId, "sessionId": "123456", "originalIntentId": originalIntentId.lowercased()]) { (result, error) in
            
            if let error = error as NSError? {
                if error.domain == FunctionsErrorDomain {
                    let code = FunctionsErrorCode(rawValue: error.code)
                    let message = error.localizedDescription
                    let details = error.userInfo[FunctionsErrorDetailsKey]
                    print(message)
                }
            }
            
            let data = result?.data as! [String: Any]
            let detectedIntent = data["intent"] == nil ? "" : data["intent"] as! String
            
            if(detectedIntent == "" || detectedIntent == "Default") {
                self.resultHandler!(false, nil)
            }
            else {

                var intent = CKIntent(id: detectedIntent, dictionary: data)
                self.resultHandler!(true, intent)
            }
            

        }

        
    }

    
    
    
    
}

/*
class IntentHandler {
    
    var intentDict: [String : Any]!
    
    static var handler: IntentHandler?
    
    static func instance() -> IntentHandler {
        
        if(handler == nil) {
            handler = IntentHandler()
        }
        
        return handler!
    }
    
    init() {
        
        self.intentDict = [:]
        
        self.intentDict["none"] = NoneIntentFactoy.classForCoder()
        self.intentDict["callmeback"] = CallMeBackIntentFactory.classForCoder()
        
    }
    
    func createIntent(id: String, name: String, parameters: [String: Any]) -> CKIntent? {
        let intentClass = self.intentDict[id] as! IntentFactory.Type
        let intent = intentClass.init()
        
        return intent.createIntent(id: id, name: name, parameters: parameters)
        
    }
    
    func getResponseActionClass(id: String) -> IntentFactory {
        
        let intentClass = self.intentDict[id] == nil ? self.intentDict["none"] as! IntentFactory.Type : self.intentDict[id] as! IntentFactory.Type
        
        return intentClass.init()
        
    }

    
}
 */
