//
//  AssistantIntentParameterViewController.swift
//  NextGenPhoneApp
//
//  Created by Kieselbach, Oliver on 29.01.19.
//  Copyright © 2019 Kieselbach, Oliver. All rights reserved.
//

import UIKit
import Eureka
import Lottie
import FirebaseFirestore
import NextGenPhoneFramework


class IntentHeader: UIView {
    
    var assistantAnimation: LOTAnimationView = {
        
        let animation = LOTAnimationView(name: "assistant")
        animation.translatesAutoresizingMaskIntoConstraints = false
        animation.heightAnchor.constraint(equalToConstant: 100).isActive = true
        animation.widthAnchor.constraint(equalToConstant: 220).isActive = true
        animation.contentMode = .scaleAspectFit
        
        animation.loopAnimation = false
        
        return animation
    }()

    
    var intentTitle: UILabel = {

        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 24, weight: .bold)
        
        return label
        
    }()

    var intentDescription: UILabel = {
        
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .gray
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 15, weight: .regular)
        label.numberOfLines = 0
        
        return label
        
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubview(self.assistantAnimation)

        self.addSubview(self.intentTitle)
        self.addSubview(self.intentDescription)
        
        self.backgroundColor = .white
        
        self.assistantAnimation.topAnchor.constraint(equalTo: self.topAnchor, constant: 40).isActive = true
        self.assistantAnimation.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true

        self.intentTitle.topAnchor.constraint(equalTo: self.assistantAnimation.bottomAnchor, constant: 15).isActive = true
        self.intentTitle.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 15).isActive = true
        self.intentTitle.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -15).isActive = true
        
        self.intentDescription.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 15).isActive = true
        self.intentDescription.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -15).isActive = true

        self.intentDescription.topAnchor.constraint(equalTo: self.intentTitle.bottomAnchor, constant: 15).isActive = true
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class IntentFooter: UIView {
    
    var sendButton: UIButton = {
        
        let button = UIButton(type: .custom)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Send", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = UIColor.actionColor(color: .blue)
        button.heightAnchor.constraint(equalToConstant: 40).isActive = true
        button.titleLabel!.font = UIFont.systemFont(ofSize: 15, weight: .regular)
        button.alpha = 1
        button.layer.cornerRadius = 10
        
        return button
        
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        
        self.addSubview(self.sendButton)
        self.sendButton.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        self.sendButton.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        self.sendButton.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.5).isActive = true

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

class MessageSendViewController: FormViewController {
    
    var message: LeaveMessage!

    var intent: CKIntent! {
        didSet {
            self.header.intentTitle.text = intent.name
            self.header.intentDescription.text = intent.description
        }
    }
    
    var header: IntentHeader = {

        let view = IntentHeader(frame: CGRect(x: 0, y: 0, width: 100, height: 270))
        
        return view
        
    }()

    var footer: IntentFooter = {
        
        let view = IntentFooter(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        
        return view
        
    }()
    
    public typealias ResultClosure = (Bool, CKIntent) -> ()
    
    public var resultHandler: ResultClosure!


    

    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.tableHeaderView = header
        self.tableView.backgroundColor = .white
        
        self.footer.sendButton.addTarget(self, action: #selector(self.sendAction), for: .touchDown)
        
    
        
        //self.createForm()
        
        // Do any additional setup after loading the view.
    }
    
    func createForm() {

        let section = Section("")
        form.append(section)
        
        for i in 0..<intent.parameter.count {

            let parameter = intent.parameter[i]
            
            switch parameter.type! {
            case .date:
                section <<< DateInlineRow() {
                    $0.title = parameter.name
                    $0.cell.detailTextLabel?.textColor = UIColor.actionColor(color: .red)
                    if(parameter.value != nil) {
                        $0.value = Date(timestamp: parameter.value as! Timestamp)
                    }
                    
                    $0.onChange({ (row) in
                        self.intent.parameter[i].value = Timestamp(date: row.value!)
                    })
                }
            case .time:
                section <<< TimeInlineRow() {
                    $0.title = parameter.name
                    $0.cell.detailTextLabel?.textColor = UIColor.actionColor(color: .red)
                    if(parameter.value != nil) {
                        $0.value = Date(timestamp: parameter.value as! Timestamp)
                    }

                    $0.onChange({ (row) in
                        self.intent.parameter[i].value = Timestamp(date: row.value!)
                    })
                }
            case .string:
                section <<< TextRow() {
                    $0.title = parameter.name
                    $0.cell.detailTextLabel?.textColor = UIColor.actionColor(color: .red)
                    if(parameter.value != nil) {
                        $0.value = parameter.value as! String
                    }
                    $0.onChange({ (row) in
                        self.intent.parameter[i].value = row.value
                    })
                }
            case .bool:
                section <<< SwitchRow() {
                    $0.title = parameter.name
                    if(parameter.value != nil) {
                        $0.value = parameter.value as! Bool
                    }
                    $0.onChange({ (row) in
                        self.intent.parameter[i].value = row.value
                    })
                }
            }

        }

        self.tableView.tableFooterView = footer

        
    }
    
    @objc func sendAction() {
        
        self.resultHandler(true, self.intent)
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
