//
//  AssistantDetectIntentionViewController.swift
//  NextGenPhoneApp
//
//  Created by Kieselbach, Oliver on 28.01.19.
//  Copyright © 2019 Kieselbach, Oliver. All rights reserved.
//

import UIKit
import Speech
import Lottie
import NextGenPhoneFramework

class AssistantDetectIntentionViewController: UIViewController {
    
    var assistantAnimation: LOTAnimationView = {
        
        let animation = LOTAnimationView(name: "assistant")
        animation.translatesAutoresizingMaskIntoConstraints = false
        animation.heightAnchor.constraint(equalToConstant: 100).isActive = true
        animation.widthAnchor.constraint(equalToConstant: 220).isActive = true
        animation.contentMode = .scaleAspectFit
        
        animation.loopAnimation = true
        
        return animation
    }()
    
    
    var assistantIcon: UIImageView = {

        let imageView = UIImageView(frame: .zero)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.heightAnchor.constraint(equalToConstant: 90).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: 90).isActive = true
        imageView.image = UIImage(imageLiteralResourceName: "siri_colored").withRenderingMode(.alwaysOriginal)
        
        return imageView
        
    }()
    
    var assistantLabel: UILabel = {

        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.textColor = .black
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 15, weight: .regular)
        label.text = "Based on your message, my assistant detected the following topic"
        label.alpha = 0

        return label
        
    }()

    var intentLabel: UILabel = {
        
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.textColor = .black
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 24, weight: .bold)
        label.alpha = 0
        return label
        
    }()

    var activityIndicator: InstagramActivityIndicator = {
        
        let v = InstagramActivityIndicator(frame: CGRect.zero)
        v.translatesAutoresizingMaskIntoConstraints = false
        v.heightAnchor.constraint(equalToConstant: 91).isActive = true
        v.widthAnchor.constraint(equalToConstant: 91).isActive = true
        v.strokeColor = UIColor.actionColor(color: .blue)
        v.lineWidth = 3
        //v.rotationDuration = 30
        //v.animationDuration = 30
        v.numSegments = 10
        
        return v
        
    }()

    var intentCorrectButton: UIButton = {
        
        let button = UIButton(type: .custom)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Yes, correct", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = UIView().tintColor
        button.heightAnchor.constraint(equalToConstant: 40).isActive = true
        button.titleLabel!.font = UIFont.systemFont(ofSize: 15, weight: .regular)
        button.alpha = 0
        button.layer.cornerRadius = 10
        return button
        
    }()

    var intentNotCorrectButton: UIButton = {
        
        let button = UIButton(type: .custom)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("No, that's not correct", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.backgroundColor = UIColor(red: 230/255, green: 230/255, blue: 230/255, alpha: 1)
        button.heightAnchor.constraint(equalToConstant: 40).isActive = true
        button.titleLabel!.font = UIFont.systemFont(ofSize: 15, weight: .regular)
        button.alpha = 0
        button.layer.cornerRadius = 10

        return button
        
    }()

    public typealias ResultClosure = (Bool, CKIntent?) -> ()

    public var resultHandler: ResultClosure!

    var message: CKMessage!
    
    var animationTopConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = .clear
        self.view.layer.cornerRadius = 20
        
        self.view.addSubview(self.assistantAnimation)
        self.animationTopConstraint = self.assistantAnimation.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 0)
        self.animationTopConstraint.isActive = true
        
        self.assistantAnimation.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0).isActive = true

        //self.view.addSubview(self.activityIndicator)
        //self.activityIndicator.addSubview(self.assistantIcon)
        self.view.addSubview(self.intentLabel)
        self.view.addSubview(self.assistantLabel)
        self.view.addSubview(self.intentCorrectButton)
        self.view.addSubview(self.intentNotCorrectButton)
        
        /*
        self.activityIndicator.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 60).isActive = true
        self.activityIndicator.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0).isActive = true
        
        self.assistantIcon.centerXAnchor.constraint(equalTo: self.activityIndicator.centerXAnchor, constant: 0).isActive = true
        self.assistantIcon.centerYAnchor.constraint(equalTo: self.activityIndicator.centerYAnchor, constant: 0).isActive = true
        */
 
        self.assistantLabel.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 15).isActive = true
        self.assistantLabel.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -15).isActive = true
        self.assistantLabel.topAnchor.constraint(equalTo: self.assistantAnimation.bottomAnchor, constant: 10).isActive = true
        
        self.intentLabel.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 15).isActive = true
        self.intentLabel.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -15).isActive = true
        self.intentLabel.topAnchor.constraint(equalTo: self.assistantLabel.bottomAnchor, constant: 20).isActive = true
        
        self.intentCorrectButton.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0).isActive = true
        self.intentCorrectButton.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.75).isActive = true
        //self.intentCorrectButton.topAnchor.constraint(equalTo: self.intentLabel.bottomAnchor, constant: 35).isActive = true
        self.intentCorrectButton.bottomAnchor.constraint(equalTo: self.intentNotCorrectButton.topAnchor, constant: -15).isActive = true
        
        
        self.intentNotCorrectButton.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0).isActive = true
        self.intentNotCorrectButton.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.75).isActive = true
        //self.intentNotCorrectButton.topAnchor.constraint(equalTo: self.intentCorrectButton.bottomAnchor, constant: 15).isActive = true
        self.intentNotCorrectButton.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: -10).isActive = true
        
        
        self.intentCorrectButton.addTarget(self, action: #selector(self.intentCorrectAction), for: .touchDown)
        self.intentNotCorrectButton.addTarget(self, action: #selector(self.intentNotCorrectAction), for: .touchDown)
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.animationTopConstraint.constant = 20
        self.view.layoutIfNeeded()
    }
    
    func detect(action: CKAction) {
        DispatchQueue.main.async {
            //self.activityIndicator.startAnimating()
            self.assistantAnimation.play()
        }

        if(action != nil && action.intentQuery != nil) {
            self.detectIntent(action: action)
        }
    }
    
    func detect() {

        DispatchQueue.main.async {
            //self.activityIndicator.startAnimating()
            self.assistantAnimation.play()
        }
        
        if(message.messageType == CKMessageType.text) {

            self.detectIntent(transcript: message.text!)
            return
            
        }
        else {
            //self.detectIntent(transcript: "")
            if(message.text != "") {
                self.detectIntent(transcript: message.text!)
                return
            }

            let speechRecognizer = SpeechRecognizer()
            speechRecognizer.resultHandler = {(url, result) in
                if(result != nil) {
                    print("speech2text = "+(result?.bestTranscription.formattedString)!)
                }
                self.message.text = result == nil ? "No transcript of Message available" : (result?.bestTranscription.formattedString)!
                self.detectIntent(transcript: result == nil ? "" : (result?.bestTranscription.formattedString)!)
                
            }
            speechRecognizer.run(url: URL(string: message.mediaURL!)!)

        }
        
        
    }
    
    var detectedIntent: CKIntent!
    
    func detectIntent(action: CKAction) {
        let intentDetector = IntentDetector()
        intentDetector.resultHandler = {(success, intent) in
            
            //self.activityIndicator.stopAnimating()
            self.assistantAnimation.stop()
            UIView.animate(withDuration: 0.3, animations: {
                self.view.backgroundColor = .white
            }, completion: { (finish) in
                if(success) {
                    self.detectedIntent = intent
                    self.intentCorrectAction()
                }
                else {
                    self.noIntentDetected()
                }
            })
            
            
        }
        intentDetector.detect(transcript: action.intentQuery, conversationId: self.message.conversationId, originalIntentId: message.topicDetails == nil ? "none" : message.topicDetails.id)
    }

    func detectIntent(transcript: String) {
        let intentDetector = IntentDetector()
        intentDetector.resultHandler = {(success, intent) in
            
            //self.activityIndicator.stopAnimating()
            self.assistantAnimation.stop()
            
            if(success) {
                self.detectedIntent = intent
                self.intentLabel.text = intent!.name
                self.animationTopConstraint.constant = 20

                UIView.animate(withDuration: 0.3, animations: {
                    
                    self.assistantLabel.alpha = 1
                    self.intentLabel.alpha = 1
                    self.intentCorrectButton.alpha = 1
                    self.intentNotCorrectButton.alpha = 1
                    
                    self.view.layoutIfNeeded()
                })
            }
            else {
                self.noIntentDetected()
            }
            
        }
        intentDetector.detect(transcript: transcript, conversationId: self.message.conversationId, originalIntentId: message.topicDetails == nil ? "none" : message.topicDetails.id)

    }
    
    @objc func intentCorrectAction() {
                
        self.resultHandler(true, self.detectedIntent)
        
    }
    
    @objc func intentNotCorrectAction() {
        
        let action1 = UIAlertAction(title: "Just send my message", style: .default) { (action) in

            self.noIntentDetected()

        }
        let action2 = UIAlertAction(title: "Select another topic", style: .default) { (action) in
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
        }
        
        let actionSheet = UIAlertController(title: "Assistant", message: nil, preferredStyle: .actionSheet)
        actionSheet.popoverPresentationController?.sourceView = self.intentNotCorrectButton
        actionSheet.popoverPresentationController?.sourceRect = self.intentNotCorrectButton.bounds
        actionSheet.addAction(action1)
        actionSheet.addAction(action2)
        actionSheet.addAction(cancel)
        self.present(actionSheet, animated: true) {
        }

    }
    
    func noIntentDetected() {
        
        self.resultHandler(false, nil)
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
