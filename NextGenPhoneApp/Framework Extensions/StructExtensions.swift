//
//  StructExtensions.swift
//  NextGenPhoneApp
//
//  Created by Kieselbach, Oliver on 05.05.19.
//  Copyright © 2019 Kieselbach, Oliver. All rights reserved.
//

import Foundation
import NextGenPhoneFramework
import AVKit

extension CKMessage {
    static func simuluateUnasweredCall(from: CKContact, to: CKContact) -> CKMessage {
        
        var message: CKMessage! = CKMessage(id: UUID().uuidString, dictionary: [:])
        message.conversationId = UUID().uuidString
        message.date = Date()
        message.inReplyId = ""
        message.messageType = CKMessageType.missedCall
        message.receiver = to.id
        message.sender = from.id
        message.type = CKConversationType.outbound
        message.mediaType = .none
        message.senderDetails = CKContactBasic(id: from.id, dictionary: from.dictionary)
        message.receiverDetails = CKContactBasic(id: to.id, dictionary: to.dictionary)
        
        return message
        
    }
    
    static func simuluateCall(from: CKContact, to: CKContact) -> CKMessage {
        
        var message: CKMessage! = CKMessage(id: UUID().uuidString, dictionary: [:])
        message.conversationId = UUID().uuidString
        message.date = Date()
        message.inReplyId = ""
        message.messageType = CKMessageType.call
        message.duration = TimeInterval(240)
        message.receiver = to.id
        message.sender = from.id
        message.type = CKConversationType.outbound
        message.mediaType = .none
        message.senderDetails = CKContactBasic(id: from.id, dictionary: from.dictionary)
        message.receiverDetails = CKContactBasic(id: to.id, dictionary: to.dictionary)
        
        return message
        
    }
    
    static func simulateTextMessage(from: CKContact, to: CKContact ) -> CKMessage {
        
        var message: CKMessage! = CKMessage(id: UUID().uuidString, dictionary: [:])
        message.conversationId = UUID().uuidString
        message.id = UUID().uuidString
        message.date = Date()
        message.inReplyId = ""
        message.messageType = CKMessageType.text
        message.text = "Hallo, diese ist eine maschinell generierte Message zu Simulationszwecken - Gruss IhreApp"
        message.receiver = to.id
        message.sender = from.id
        message.type = CKConversationType.outbound
        message.mediaType = .text
        message.senderDetails = CKContactBasic(id: from.id, dictionary: from.dictionary)
        message.receiverDetails = CKContactBasic(id: to.id, dictionary: to.dictionary)
        
        
        return message
        
    }
    
    static func simulateVoiceMessage(from: CKContact, to: CKContact, text: String ) -> CKMessage {
        
        var message: CKMessage! = CKMessage(id: UUID().uuidString, dictionary: ["type": CKConversationType.outbound.rawValue, "sender": from.id, "receiver": to.id,  "inReplyId": "", "messageType": CKMessageType.voice.rawValue, "text": "", "mediaRef": "", "date": Date().toFirebaseTimestamp()])
        message.conversationId = UUID().uuidString
        message.id = UUID().uuidString
        message.date = Date()
        message.inReplyId = ""
        message.messageType = CKMessageType.voice
        message.text = text
        message.receiver = to.id
        message.sender = from.id
        message.type = CKConversationType.outbound
        message.mediaType = .audio
        message.senderDetails = CKContactBasic(id: from.id, dictionary: from.dictionary)
        message.receiverDetails = CKContactBasic(id: to.id, dictionary: to.dictionary)
        
        
        return message
        
    }
    
}

extension CKMessage {
    
    init?(phoneCall: Call) {
        
        let duration = TimeInterval(exactly: phoneCall.ended.secondsLater(than: phoneCall.started))
        
        self.init(conversationId: phoneCall.conversationId, id: phoneCall.id, inReplyId: "", type: phoneCall.type(), messageType: .call, receiver: phoneCall.callee, sender: phoneCall.caller, date: phoneCall.ended, text: "", mediaRef: "", isRead: false, topic: "", mediaURL: "", mediaType: .none, duration: duration!, senderDetails: phoneCall.callerDetails, receiverDetails: phoneCall.calleeDetails, topicDetails: nil, topicAction: "", topicActionDetails: nil)
        
    }
    
    init?(conversation: String, leaveMessage: LeaveMessage, intent: CKIntent?, receiver: CKContact) {
        let messageMediaType = CKMessageMediaType(rawValue: leaveMessage.type!.rawValue)
        
        var messageType: CKMessageType = .text
        switch messageMediaType! {
        case .text:
            messageType = .text
        case .video:
            messageType = .video
        case .audio:
            messageType = .voice
        case .none:
            messageType = .text
        }
        
        let senderDetails = CKContactBasic(id: ConversationKit.shared.appUser.id, dictionary: ConversationKit.shared.appUser.dictionary)
        let receiverDetails = CKContactBasic(id: receiver.id, dictionary: receiver.dictionary)
        
        self.init(conversationId: conversation, id: UUID().uuidString, inReplyId: "", type: .outbound, messageType: messageType, receiver: receiver.id, sender: ConversationKit.shared.currentUser.uid, date: Date(), text: leaveMessage.content == nil ? "" : leaveMessage.content, mediaRef: "", isRead: false, topic: intent == nil ? "" : intent!.name, mediaURL:"", mediaType: messageMediaType!, duration: 0, senderDetails: senderDetails!, receiverDetails: receiverDetails!, topicDetails: intent, topicAction: "", topicActionDetails: CKIntent(dictionary: [:]))
        
        if(messageMediaType != .text) {
            let asset = AVAsset(url: leaveMessage.contentURL!)
            let path = "users/"+receiver.id+"/messages/"+UUID().uuidString
            
            self.mediaRef = path
            self.mediaURL = leaveMessage.contentURL?.absoluteString //url?.absoluteString
            self.duration = CMTimeGetSeconds(asset.duration)
        }
        
    }

    
    
}

