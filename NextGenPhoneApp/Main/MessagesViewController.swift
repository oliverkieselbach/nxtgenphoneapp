//
//  MessagesViewController.swift
//  NextGenPhoneApp
//
//  Created by Kieselbach, Oliver on 10.12.18.
//  Copyright © 2018 Kieselbach, Oliver. All rights reserved.
//

import UIKit
import FirebaseStorage
import FirebaseFirestore
import FirebaseUI
import AVFoundation
import AVKit
import Pulley
import Eureka
import NextGenPhoneFramework
import BonsaiController


class ContactVIPCell: UICollectionViewCell {
    
    var contact: CKContact! {
        didSet {
            self.avatar.contact = contact
            self.contactName.text = contact.name
        }
    }
    
    var avatar: CKContactView = {
        
        let a = CKContactView(frame: .zero)
        a.translatesAutoresizingMaskIntoConstraints = false
        a.heightAnchor.constraint(equalToConstant: 60).isActive = true //100
        a.widthAnchor.constraint(equalToConstant: 60).isActive = true //100
        
        return a
    }()
    
    var contactName: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: 13, weight: UIFont.Weight.regular)
        label.textAlignment = .center
        label.numberOfLines = 2
        
        return label
        
    }()
    
    var longTapHandler: ((CKContact) -> Void)!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .clear
        self.contentView.backgroundColor = .clear
        self.contentView.addSubview(self.avatar)
        self.contentView.addSubview(self.contactName)
        //self.avatar.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 3).isActive = true
        self.avatar.centerYAnchor.constraint(equalTo: self.contentView.centerYAnchor, constant: -10).isActive = true
        self.avatar.centerXAnchor.constraint(equalTo: self.contentView.centerXAnchor, constant: 0).isActive = true
        
        self.contactName.topAnchor.constraint(equalTo: self.avatar.bottomAnchor, constant: 3).isActive = true
        self.contactName.leftAnchor.constraint(equalTo: self.contentView.leftAnchor, constant: 0).isActive = true
        self.contactName.rightAnchor.constraint(equalTo: self.contentView.rightAnchor, constant: 0).isActive = true
    
        let longTapGesture = UILongPressGestureRecognizer(target: self, action: #selector(self.longTapped))
        self.addGestureRecognizer(longTapGesture)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func longTapped() {
        if(self.longTapHandler != nil) {
            self.longTapHandler(self.contact)
        }
    }
    
    
    
}


class ContactVIPView: UIView {
    
    var collectionView: UICollectionView!
    var headerLabel: UILabel! = {

        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .gray
        label.font = UIFont.systemFont(ofSize: 13, weight: .regular)
        label.text = "YOUR VIPs"
        
        return label
        
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor(0xE3F2FD).withAlphaComponent(0.5) //UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 0)
        //#ececec
        self.backgroundColor = UIColor(0xececec).withAlphaComponent(0.5)
        self.backgroundColor = UIColor(0xFAFAFA)
        
        let collectionViewLayout = UICollectionViewFlowLayout()
        //collectionViewLayout.itemSize = CGSize(width: 100, height: 120)
        collectionViewLayout.itemSize = CGSize(width: 60, height: 120)
        collectionViewLayout.scrollDirection = .horizontal
        collectionViewLayout.minimumInteritemSpacing = 15
        
        self.collectionView = UICollectionView(frame: .zero, collectionViewLayout: collectionViewLayout)
        self.collectionView.backgroundColor = .clear
        self.addSubview(self.collectionView)
        self.collectionView.translatesAutoresizingMaskIntoConstraints = false
        self.collectionView.heightAnchor.constraint(equalToConstant: 120).isActive = true
        self.collectionView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 0).isActive = true
        self.collectionView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 0).isActive = true
        self.collectionView.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        self.collectionView.backgroundColor = .clear
        
        self.collectionView.register(ContactVIPCell.classForCoder(), forCellWithReuseIdentifier: "vipCell")
        self.collectionView.contentInset = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)

        self.addSubview(self.headerLabel)
        self.headerLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 15).isActive = true
        self.headerLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 15).isActive = true
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        /*
        let topLine = CALayer()
        topLine.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: 0.8)
        topLine.backgroundColor = UIColor(red: 210/255, green: 210/255, blue: 210/255, alpha: 1).cgColor
        self.layer.addSublayer(topLine)

        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x: 0, y: self.frame.height - 0.8, width: self.frame.width, height: 0.8)
        bottomLine.backgroundColor = UIColor(red: 210/255, green: 210/255, blue: 210/255, alpha: 1).cgColor
        self.layer.addSublayer(bottomLine)
        */
    }
    
    
}

class ForYouHeaderView: UIView {
    
    var contact: CKContact! {
        didSet {
            self.avatar.contact = contact
        }
    }
    
    var avatar: CKContactView = {
        
        let a = CKContactView(frame: .zero)
        a.translatesAutoresizingMaskIntoConstraints = false
        a.heightAnchor.constraint(equalToConstant: 44).isActive = true
        a.widthAnchor.constraint(equalToConstant: 44).isActive = true
        
        return a
    }()

    var titleLabel: UILabel = {

        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        label.textAlignment = .left
        label.font = UIFont.systemFont(ofSize: 30, weight: .bold)
        label.heightAnchor.constraint(equalToConstant: 43).isActive = true
        return label
        
    }()

    var subTitleLabel: UILabel = {
        
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .gray
        label.textAlignment = .left
        label.font = UIFont.systemFont(ofSize: 12, weight: .regular)
        label.heightAnchor.constraint(equalToConstant: 14.5).isActive = true

        return label
        
    }()
    
    var vipCollectionView: ContactVIPView = {

        let v = ContactVIPView(frame: .zero)
        v.translatesAutoresizingMaskIntoConstraints = false
        v.heightAnchor.constraint(equalToConstant: 170).isActive = true
        
        return v
        
        
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addSubview(self.titleLabel)
        self.addSubview(self.subTitleLabel)
        self.addSubview(self.vipCollectionView)
        self.addSubview(self.avatar)
        //self.backgroundColor = .red
        
        self.subTitleLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 20).isActive = true
        self.titleLabel.topAnchor.constraint(equalTo: self.subTitleLabel.bottomAnchor, constant: 3).isActive = true
        
        self.subTitleLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 15).isActive = true
        self.subTitleLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -15).isActive = true

        self.titleLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 15).isActive = true
        self.titleLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -15).isActive = true
        
        self.vipCollectionView.topAnchor.constraint(equalTo: self.titleLabel.bottomAnchor, constant: 20).isActive = true
        self.vipCollectionView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 0).isActive = true
        self.vipCollectionView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 0).isActive = true

        self.avatar.bottomAnchor.constraint(equalTo: self.titleLabel.bottomAnchor, constant: 0).isActive = true
        self.avatar.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -15).isActive = true

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

class MessagesViewController: UIViewController, UINavigationControllerDelegate {
    
    var contact: CKContact! {
        didSet {
            self.headerView.contact = contact
        }
    }

    var tableView: UITableView = {
        
        var t = UITableView(frame: CGRect.zero, style: .plain)
        t.translatesAutoresizingMaskIntoConstraints = false
        t.rowHeight = UITableView.automaticDimension
        t.estimatedRowHeight = 100
        t.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
        t.allowsSelection = true
        t.backgroundColor = .clear
        
        return t
        
    }()
    
    var headerView: ForYouHeaderView = {
        let v = ForYouHeaderView(frame: CGRect(x: 0, y: 0, width: 300, height: 275))
        v.titleLabel.text = "Communicator"
        //v.titleLabel.textColor = v.tintColor
        v.subTitleLabel.text = "This is the Subtitle"
        let formatter = DateFormatter()
        formatter.dateStyle = .full
        formatter.timeStyle = .none
        
        let dateString = formatter.string(from: Date())
        v.subTitleLabel.text = dateString.uppercased()

        return v
    }()

    var search: UISearchBar = {
        
        var s = UISearchBar(frame: CGRect.zero)
        s.translatesAutoresizingMaskIntoConstraints = false
        s.placeholder = "Search for Messages"
        s.searchBarStyle = .minimal
        s.isTranslucent = true
        
        
        return s
        
    }()
    
    //var conversations: [MessageCellContent]! = []
    
    var queue: DispatchQueue = DispatchQueue(label: "worker")

    var presentedVC: AnyClass!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //self.view.addBlurView()
        
        self.view.backgroundColor = UIColor.white.withAlphaComponent(1)

        self.view.addSubview(self.tableView)

        self.tableView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 0).isActive = true
        self.tableView.widthAnchor.constraint(equalTo: self.view.widthAnchor).isActive = true
        self.tableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -80).isActive = true
        self.tableView.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        self.tableView.delegate = self
        self.tableView.tableHeaderView = self.headerView
        self.tableView.sectionHeaderHeight = 44
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.headerView.vipCollectionView.collectionView.delegate = self
        
        self.headerView.avatar.addTapTarget(target: self, action: #selector(self.myStatusAction))
        
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongPress(gesture:)))
        self.view.addGestureRecognizer(longPress)
        
        
        

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    @objc func myStatusAction() {
        let meStatusVC = MeStatusViewController()
        meStatusVC.contact = self.contact
        meStatusVC.title = "My Status"
        
        let transitionDelegate = SPStorkTransitioningDelegate()
        //transitionDelegate.customHeight = self.view.frame.height * 0.75
        meStatusVC.transitioningDelegate = transitionDelegate
        meStatusVC.modalPresentationStyle = .custom
        self.present(meStatusVC, animated: true, completion: nil)

    }
    
    @objc func handleLongPress(gesture : UILongPressGestureRecognizer!) {
        if gesture.state != .ended {
            return
        }
        let p = gesture.location(in: self.tableView)
        
        if let indexPath = self.tableView.indexPathForRow(at: p) {
        } else {
            print("couldn't find index path")
        }
    }

    
    @objc func cancelAction() {
        self.dataSource.unbind()
        self.dismiss(animated: true) {
        }
    }
    
    var dbListener: ListenerRegistration!
    var dataSource: FUIFirestoreTableViewDataSource!
    var vipDataSource: FUIFirestoreCollectionViewDataSource!

    func bindTableViewQuery(query: Query) {
        
        
        self.tableView.register(TopicOnlyCell.classForCoder(), forCellReuseIdentifier: "topicOnlyCell")
        self.tableView.register(TopicActionCell.classForCoder(), forCellReuseIdentifier: "topicActionCell")
        
        self.tableView.register(CKContactConversationCell.self, forCellReuseIdentifier: CKContactConversationCell.reuseId)
        self.tableView.register(CKContactConversationFirstMessageInCell.self, forCellReuseIdentifier: CKContactConversationFirstMessageInCell.reuseId)
        self.tableView.register(CKContactConversationFirstMessageOutCell.self, forCellReuseIdentifier: CKContactConversationFirstMessageOutCell.reuseId)


        
        self.dataSource = nil
        self.tableView.reloadData()
        
        self.dataSource = self.tableView.bind(toFirestoreQuery: query) { (tableView, indexPath, snapshot) -> UITableViewCell in
            
            print("LoadData bind")
            self.tableView.tableFooterView = nil
            
            
            let data = snapshot.data()
            let id = snapshot.documentID
            
            if(data != nil) {
                
                let conversation = CKConversation(id: snapshot.documentID, dictionary: data!)
                if(conversation?.intentMessageContent != nil) {
                    if(conversation?.lastMessageContent != nil) {
                        
                        let cell = self.tableView.dequeueReusableCell(withIdentifier: CKContactConversationCell.reuseId, for: indexPath) as! CKContactConversationCell
                        cell.selectionStyle = .none
                        cell.conversation = conversation
                        
                        cell.playMessageAction = {message in
                         
                            self.playMessageAction(message: message)
                         
                        }
                        return cell
                        
                    }
                    else {
                        
                        let cell = self.tableView.dequeueReusableCell(withIdentifier: conversation?.intentMessageContent.type == CKConversationType.outbound ? CKContactConversationFirstMessageOutCell.reuseId : CKContactConversationFirstMessageInCell.reuseId, for: indexPath) as! CKContactConversationBaseCell
                        cell.conversation = conversation
                        
                        
                        cell.playMessageAction = {message in
                            
                            self.playMessageAction(message: message)
                            
                        }
                        
                        return cell
                    }
                }

            }
            
            return UITableViewCell()
        }
    }

    func bindVIPViewQuery(query: Query) {
        
        
        self.vipDataSource = nil
        let collectionView = self.headerView.vipCollectionView.collectionView!
        self.headerView.vipCollectionView.collectionView.delegate = self
        collectionView.reloadData()
        
        self.vipDataSource = collectionView.bind(toFirestoreQuery: query, populateCell: { (collectionView, indexPath, documentSnapshot) -> UICollectionViewCell in
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "vipCell", for: indexPath) as! ContactVIPCell
            let data = documentSnapshot.data()
            
            if(data != nil) {
                var contact = CKContact(id: documentSnapshot.documentID, dictionary: data!)
                
                let absent = Bool.random()
                if(absent) {

                    let voiceMessage = Bool.random()
                    if(voiceMessage == false) {
                        let absenceMessage = CKContactStatusContent(id: "test", dictionary: ["absenceMessage.contentURL": "users/8oLNwkp4gAZQFaCYBTyaIRmheGH3/E73723F4-B601-4DA2-A989-BFE9F27EF648", "absenceMessage.contentType": "video/mp4", "absenceMessage.duration": 17.3, "absenceMessage.recorded": Timestamp(date: Date()), "absenceMessage.transcript": ""])
                        contact!.statusContent = absenceMessage
                    }
                    else {
                        let absenceMessage = CKContactStatusContent(id: "test", dictionary: ["absenceMessage.contentURL": "users/8oLNwkp4gAZQFaCYBTyaIRmheGH3/3AE7ADC0-D5A1-4DA5-B2D1-4C96B04EDC93", "absenceMessage.contentType": "audio/m4a", "absenceMessage.duration": 17.66, "absenceMessage.recorded": Timestamp(date: Date()), "absenceMessage.transcript": ""])
                        contact!.statusContent = absenceMessage
                    }
                    
                }
                
                cell.contact = contact
                cell.longTapHandler = {(contact) in
                    
                    self.simulator(contact: contact)
                    
                }
                
            }
            
            return cell
            
        })
        
    }

    
    func loadData() {
        
        let db  = ConversationKit.shared.db
        self.contact = ConversationKit.shared.appUser
        db?.collection("users").document(self.contact.id).addSnapshotListener({ (snapshot, error) in
            guard let document = snapshot else {
                print(error?.localizedDescription)
                return
            }
            let c = CKContact(id: document.documentID, dictionary: document.data()!)
            ConversationKit.shared.appUser = c
            self.contact = c
        })


        let query = db?.collection(ConversationKit.shared.currentUser.uid).document("data").collection("conversations").order(by: "lastMessage", descending: true)
        self.bindTableViewQuery(query: query!)

        let vipQuery = db?.collection(ConversationKit.shared.currentUser.uid).document("data").collection("contacts").whereField("vip", isEqualTo: true).order(by: "lastContact", descending: true)
        self.bindVIPViewQuery(query: vipQuery!)
        
    }

    private func playMessageAction(message: CKMessage) {
        
        
        if(message.messageType! == .voice) {
            let vc = VoiceMessagePlayerViewController()
            vc.message = message
            vc.transitioningDelegate = self
            vc.modalPresentationStyle = .custom
            self.presentedVC = vc.classForCoder
            
            
            self.present(vc, animated: true) {
                vc.playPauseAction()
            }
            
        }
        else if(message.messageType! == .video) {
            let vc = VideoMessagePlayerViewController()
            vc.message = message
            vc.transitioningDelegate = self
            vc.modalPresentationStyle = .custom
            self.presentedVC = vc.classForCoder
            
            
            self.present(vc, animated: true) {
                vc.playPauseAction()
            }
        }
        
        
        
        
    }
    
    func simulator(contact: CKContact) {
        
        let a1 = UIAlertAction(title: "Simulate incoming message", style: .default) { (action) in
            
            var message = CKMessage.simulateVoiceMessage(from: contact, to: ConversationKit.shared.appUser, text: "Hallo, ich brauche einen Termin für morgen")
            AppDelegateAccessor.runAssistant(viewController: self, message: message) { (transcript, intent) in
                message.text = transcript
                if(intent != nil) {
                    message.topic = intent?.name
                    message.topicDetails = intent
                }
                message.send(completion: { (success) in
                })
            }

        }
 
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
        }

        let actionSheet = UIAlertController(title: "Simulator", message: contact.name, preferredStyle: .actionSheet)
        actionSheet.addAction(a1)
        actionSheet.addAction(cancel)
        
        self.present(actionSheet, animated: true) {
        }
        
    }


    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension MessagesViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let cell = collectionView.cellForItem(at: indexPath) as! ContactVIPCell
        let contact = cell.contact!
        AppDelegateAccessor.showContact(contact: contact, modal: false, fromContactList: true)
        
        
    }
    
}


extension MessagesViewController: UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let v = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 44))
        v.backgroundColor = self.view.backgroundColor
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        v.addSubview(label)
        //label.topAnchor.constraint(equalTo: v.topAnchor, constant: 0).isActive = true
        label.leftAnchor.constraint(equalTo: v.leftAnchor, constant: 15).isActive = true
        label.rightAnchor.constraint(equalTo: v.rightAnchor, constant: 0).isActive = true
        label.bottomAnchor.constraint(equalTo: v.bottomAnchor, constant: -3).isActive = true
        
        label.textColor = .black
        label.textAlignment = .left
        label.font = UIFont.systemFont(ofSize: 20, weight: .bold)
        label.text = "Conversations"
        /*
        let maskLayer = CALayer()
        maskLayer.frame = CGRect(x: 0, y: 0, width: 100, height: 44)
        maskLayer.backgroundColor = UIColor.clear.cgColor
        v.layer.mask = maskLayer
        */
        
        return v
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {

        
        if cell.reuseIdentifier == "messageCell" {
            queue.asyncAfter(deadline: .now() + 1) {
                let messageCell = cell as! ConversationLastMessageCell
                
                if(messageCell.message.isRead == false) {
                    ConversationKit.shared.db.collection(ConversationKit.shared.currentUser.uid+"/data/conversations").document(messageCell.message.conversationId).setData(["lastMessageRead":true, "lastMessageContent":messageCell.message.dictionary], merge: true)
                    
                    ConversationKit.shared.db.collection(ConversationKit.shared.currentUser.uid+"/data/conversations").document(messageCell.message.conversationId).collection("messages").document(messageCell.message.id).setData(["isRead":true], merge: true)
                    
                    ConversationKit.shared.db.collection("conversations/updates/"+messageCell.conversation.id).document(messageCell.message.id).setData(["isRead": true, "contact":messageCell.conversation.contact])
                }
                
            }
        }
        
        
        
    }
    
    /*
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        
        let cellContent = conversations![indexPath.item]

        var conversation = cellContent.conversation!
        
        let cellIdentifier = conversation.lastMessageContent.type == .inbound ? "messageCell" : "messageCell_outbound"
        let cell = self.tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! MessageCell
        cell.conversation = conversation
        
        cell.selectionStyle = .none
        if(conversation.lastMessageContent != nil) {
            conversation.lastMessageContent.isRead = conversation.lastMessageRead
            cell.message = conversation.lastMessageContent
            cell.conversation = conversation
            cell.messageContent.presenterVC = self
        }
        cell.setNeedsUpdateConstraints()
        cell.layoutIfNeeded()
        return cell
        
    }
    */
 
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print("MessageViewController.didSelectRowAt "+String(indexPath.item))
        
        let cell = tableView.cellForRow(at: indexPath) as! CKContactConversationBaseCell
        let contactId = cell.conversation.contact

        tableView.deselectRow(at: indexPath, animated: false)
        
        
        let vc = ConversationViewController()
        
        vc.conversation = cell.conversation
        let nc = UINavigationController(rootViewController: vc)
        nc.modalPresentationStyle = .overFullScreen
        nc.navigationBar.prefersLargeTitles = false
        print("MessagesViewController.didSelectRowAt - conversationId = \(cell.conversation.id)")
        AppDelegateAccessor.mainViewController.present(nc, animated: true) {
            vc.loadData()
        }
        
    }
    
    
}

// MARK:- BonsaiController Delegate
extension MessagesViewController: BonsaiControllerDelegate {
    
    public func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        
        if(presented.classForCoder == VoiceMessagePlayerViewController.classForCoder() || presented.classForCoder == VideoMessagePlayerViewController.classForCoder()) {
            return BonsaiController(fromDirection: .bottom, blurEffectStyle: .dark, presentedViewController: presented, delegate: self)
        }
        else {
            return BonsaiController(fromView: self.view, blurEffectStyle: .dark,  presentedViewController: presented, delegate: self)
        }
    }
    
    public func frameOfPresentedView(in containerViewFrame: CGRect) -> CGRect {
        
        if(self.presentedVC == VoiceMessagePlayerViewController.classForCoder() || self.presentedVC == VideoMessagePlayerViewController.classForCoder()) {
            return CGRect(origin: CGPoint(x: 0, y: containerViewFrame.size.height * 0.3), size: CGSize(width: containerViewFrame.width, height: containerViewFrame.height * 0.7))
        }
        else {
            return CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: containerViewFrame.width, height: containerViewFrame.height))
        }
        
    }
}


