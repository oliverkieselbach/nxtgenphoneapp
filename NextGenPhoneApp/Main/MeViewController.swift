//
//  MeViewController.swift
//  NextGenPhoneApp
//
//  Created by Oliver Kieselbach on 08.10.18.
//  Copyright © 2018 Kieselbach, Oliver. All rights reserved.
//

import UIKit
import LetterAvatarKit
import Eureka
import FirebaseFirestore
import OnlyPictures
import ViewAnimator
import Pulley


class MissedCallsCell: ContactListCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.title.text = "Missed\nCalls"

        //self.contentView.gradientBackground(from: UIColor(0xEF5350), to: UIColor(0xB71C1C), direction: .topLeftToBottomRight)
        self.contentView.gradientBackground(from: UIColor(0xEF9A9A), to: UIColor(0xEF5350), direction: .topLeftToBottomRight)

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

class ContactListCell: UICollectionViewCell {
    
    var contactPictures: OnlyHorizontalPictures = {

        let v = OnlyHorizontalPictures()
        //v.translatesAutoresizingMaskIntoConstraints = false
        
        return v
        
    }()
    
    var contacts: [CKContactBasic] = [] {
        didSet {
            self.contactPictures.reloadData()
        }
    }
    
    var title: UILabel = {

        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .white
        label.textAlignment = .left
        label.font = UIFont.systemFont(ofSize: 28, weight: UIFont.Weight.bold)
        label.numberOfLines = 2
        
        return label
    }()
    
    var queue: DispatchQueue = DispatchQueue(label: "loadImages")
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.contentView.addSubview(self.contactPictures)
        self.contentView.addSubview(self.title)
        self.title.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 15).isActive = true
        self.title.leftAnchor.constraint(equalTo: self.contentView.leftAnchor, constant: 15).isActive = true

        self.contactPictures.frame = CGRect(x: 15, y: self.contentView.center.y + 10, width: self.contentView.frame.width - 30, height: 60)
        //self.contactPictures.layer.cornerRadius = 20.0
        self.contactPictures.layer.masksToBounds = true
        self.contactPictures.delegate = self
        self.contactPictures.dataSource = self
        self.contactPictures.alignment = .center
        self.contactPictures.countPosition = .right
        self.contactPictures.recentAt = .left
        self.contactPictures.spacingColor = UIColor.clear
        self.contactPictures.backgroundColorForCount = UIColor.init(red: 230/255, green: 230/255, blue: 230/255, alpha: 1.0)
        self.contactPictures.textColorForCount = .gray
        self.contactPictures.fontForCount = UIFont.systemFont(ofSize: 16)
        self.contactPictures.recentAt = .left
        
        self.contentView.layer.cornerRadius = 10
        self.clipsToBounds = true
        self.contentView.layer.masksToBounds = true


    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

extension ContactListCell: OnlyPicturesDelegate, OnlyPicturesDataSource {

    
    func visiblePictures() -> Int {
        return 3
    }
    
    func pictureViews(_ imageView: UIImageView, index: Int) {
        let person = self.contacts[index]
        
        let letterKit = LetterAvatarBuilderConfiguration()
        letterKit.username = person.name
        letterKit.backgroundColors = [UIColor.gray]
        letterKit.lettersColor = UIColor.white
        letterKit.lettersFont = UIFont.systemFont(ofSize: 10)
        
        DispatchQueue.main.async {
            if(person.profileImageRef == "") {
                imageView.image = UIImage.makeLetterAvatar(withConfiguration: letterKit)
                
            }
            else {
                imageView.sd_setImage(with: AppDelegateAccessor.storage.reference().child((person.profileImageRef)), placeholderImage: UIImage.makeLetterAvatar(withConfiguration: letterKit)) { (image, error, cache, ref) in
                    
                }
            }
        }
        
    }

    func pictureView(_ imageView: UIImageView, didSelectAt index: Int) {
    }
    
    func numberOfPictures() -> Int {
        return self.contacts.count
    }
    
    
}

class MessagesCell: ContactListCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.title.text = "Messages"
        self.contentView.gradientBackground(from: UIColor(0x64B5F6), to: UIColor(0x2196F3), direction: .topLeftToBottomRight)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class CKContactView: UIView {
    var availableColor = UIColor(displayP3Red: 46/255, green: 204/255, blue: 113/255, alpha: 1)
    var notAvailableColor = UIColor(displayP3Red: 231/255, green: 76/255, blue: 60/255, alpha: 1)

    var contactWithNoStatus: CKContactBasic! {
        didSet {
            self.contact = CKContact(personEmbedded: contactWithNoStatus)
        }
    }
    
    var contact: CKContact! {
        didSet {

            if(contact != nil) {
                let letterKit = LetterAvatarBuilderConfiguration()
                letterKit.username = contact.name
                letterKit.backgroundColors = [contact.status == .available ? self.availableColor : self.notAvailableColor]
                letterKit.lettersColor = UIColor.white
                letterKit.lettersFont = UIFont.systemFont(ofSize: UIFont.systemFontSize)
                
                if(contact.profileImageRef == "") {
                    self.avatar.image = UIImage.makeLetterAvatar(withConfiguration: letterKit)
                    
                }
                else {
                    self.avatar.sd_setImage(with: AppDelegateAccessor.storage.reference().child((contact.profileImageRef)), placeholderImage: UIImage.makeLetterAvatar(withConfiguration: letterKit)) { (image, error, cache, ref) in
                    }
                }
                
                
                
                let status: CKContactStatus! = contact.status!
                
                
                switch status {
                case .available?:
                    //self.layer.borderColor = self.availableColor.cgColor
                    self.dotsLayer.strokeColor = self.availableColor.cgColor

                case .notAvailable?:
                    //self.layer.borderColor = self.notAvailableColor.cgColor
                    self.dotsLayer.strokeColor = self.notAvailableColor.cgColor

                default:
                    //self.layer.borderColor = UIColor.clear.cgColor
                    self.dotsLayer.strokeColor = UIColor.clear.cgColor
                }

                
                let one : NSNumber = 1
                let two : NSNumber = self.contact.absenceMessage == nil ? 5 : 0
                self.dotsLayer.lineDashPattern = [one,two]


                self.layoutIfNeeded()
                
                self.needsUpdateConstraints()
            }
            
        }
    }
    
    var dotsLayer: CAShapeLayer!

    var avatarStatus: UIView = {
        
        let v = UIView(frame: .zero)
        v.translatesAutoresizingMaskIntoConstraints = false
        v.backgroundColor = .clear
        
        return v
        
    }()
    
    var avatar: UIImageView = {
        
        let imageView = UIImageView(frame: CGRect.zero)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.clipsToBounds = true
        imageView.backgroundColor = .clear
        imageView.contentMode = .scaleAspectFill
        
        return imageView
        
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .clear
        
        self.addSubview(self.avatarStatus)
        self.addSubview(self.avatar)
        self.avatar.topAnchor.constraint(equalTo: self.topAnchor, constant: 6).isActive = true
        self.avatar.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 6).isActive = true
        self.avatar.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -6).isActive = true
        self.avatar.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -6).isActive = true

        self.avatarStatus.topAnchor.constraint(equalTo: self.topAnchor, constant: 1).isActive = true
        self.avatarStatus.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 1).isActive = true
        self.avatarStatus.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -1).isActive = true
        self.avatarStatus.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -1).isActive = true

        self.dotsLayer = CAShapeLayer()

        self.avatarStatus.clipsToBounds = false
        
        self.clipsToBounds = true
        self.avatar.clipsToBounds = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.avatar.layer.cornerRadius = self.avatar.frame.size.height * 0.5
        //self.layer.cornerRadius = self.layer.frame.height * 0.5
        //self.layer.borderWidth = max(2, self.layer.frame.height * 0.02) //self.layer.frame.height * 0.02
        
        circleOfDots()
        
    }
    
    func addTapTarget(target: Any?, action: Selector?) {
        let tapGesture = UITapGestureRecognizer(target: target, action: action)
        self.addGestureRecognizer(tapGesture)
    }
    
    func circleOfDots() {
        let circlePath = UIBezierPath(arcCenter: self.avatarStatus.center, radius: self.avatarStatus.frame.height * 0.5, startAngle: CGFloat(0), endAngle:CGFloat(M_PI * 2), clockwise: true)
        let shapeLayer = self.dotsLayer!

        shapeLayer.path = circlePath.cgPath
        shapeLayer.position = CGPoint(x: (self.avatarStatus.bounds.midX - self.avatarStatus.center.x), y: (self.avatarStatus.bounds.midY - self.avatarStatus.center.y)) // CGPointMake(CGRectGetMidX(self.bounds)-200 ,CGRectGetMidY(self.bounds)-200 )
        //change the fill color
        shapeLayer.fillColor = UIColor.clear.cgColor
        //you can change the stroke color
        if(shapeLayer.strokeColor == nil) {
            shapeLayer.strokeColor = UIColor.clear.cgColor
        }
        //you can change the line width
        shapeLayer.lineWidth = max(2, self.frame.height * 0.02) //6.0
        if(shapeLayer.lineDashPattern == nil) {
            let one : NSNumber = 1
            let two : NSNumber = 5
            shapeLayer.lineDashPattern = [one,two]
        }
        shapeLayer.lineCap = CAShapeLayerLineCap.square
        self.avatarStatus.layer.addSublayer(shapeLayer)
    }
    
}

enum CollectionEntryType {
    case contact
    case missedCalls
    case messages
}

struct CollectionEntry {
    var type: CollectionEntryType!
    var value: Any!
}

class MeViewController: UIViewController {

    var backgroundImageView: UIImageView = {
        
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.clipsToBounds = true
        view.contentMode = .scaleAspectFill
        view.isHidden = true
        
        return view
        
    }()
    
    var widgetController: WidgetsViewController!
    var contactHeaderView: ContactHeaderView = {
        
        let view = ContactHeaderView(mode: .full)
        view.name.textColor = .black
        return view
        
    }()
    
    var collectionEntries: [CollectionEntry] = []
    var collectionView: UICollectionView!
    
    
    var contact: CKContact! {
        didSet {
            
            if(oldValue == nil) {
                self.contactHeaderView.contact = contact
                
                /*
                applyGradientToView(view: self.view, gradientStyle: contact.backgroundColorStyle)
                if(contact.backgroundImageRef != "") {
                    
                    self.backgroundImageView.sd_setImage(with: AppDelegateAccessor.storage.reference(withPath: self.contact.backgroundImageRef), placeholderImage: self.backgroundImageView.image) { (image, error, cacheType, ref) in
                        self.backgroundImageView.isHidden = false
                    }
                    
                    
                }
                else {
                    self.backgroundImageView.isHidden = true
                }
                 */
            }
            else {
                
                self.contactHeaderView.contact = contact
                
                /*
                if(oldValue.backgroundColorStyle != contact.backgroundColorStyle) {
                    applyGradientToView(view: self.view, gradientStyle: contact.backgroundColorStyle)
                }
                if(oldValue.backgroundImageRef != contact.backgroundImageRef) {
                    self.backgroundImageView.sd_setImage(with: AppDelegateAccessor.storage.reference(withPath: self.contact.backgroundImageRef), placeholderImage: self.backgroundImageView.image) { (image, error, cacheType, ref) in
                        self.backgroundImageView.isHidden = false
                    }
                    
                }
                 */
            }
            
        }
    }
    
    var missedCalls: [CKContactBasic] = []
    var contactsWithMessages: [CKContactBasic] = []
    var missedCallCellIndex: Int! = 0
    var contactsWithMessagesCellIndex: Int! = 0
    
    var missedCallCell: UICollectionViewCell?
    var messagesCell: UICollectionViewCell?
    
    var missedCallIndexPath: IndexPath? {
        didSet {
            
            if(missedCallIndexPath == nil) {
                if(messagesIndexPath != nil) {
                    let item = messagesIndexPath!.item - 1
                    if(self.collectionEntries[item].type == .messages) {
                            let entry = collectionEntries[item]
                            self.collectionEntries.remove(at: item)
                            self.collectionView.deleteItems(at: [messagesIndexPath!])
                            self.collectionEntries.insert(entry, at: 1)
                            self.collectionView.insertItems(at: [IndexPath(item: 1, section: 0)])
                            //self.collectionView.moveItem(at: IndexPath(item: item, section: 0), to: IndexPath(item: 1, section: 0))
                            self.collectionView.reloadItems(at: [IndexPath(item: 4, section: 0)])
                    }
                }
            }
            else {

                if(oldValue == nil) {
                    if(messagesIndexPath != nil) {
                        let item = messagesIndexPath!.item + 1
                        if(self.collectionEntries[item].type == .messages) {
                                let entry = collectionEntries[item]
                                self.collectionEntries.remove(at: item)
                                self.collectionView.deleteItems(at: [messagesIndexPath!])
                                self.collectionEntries.insert(entry, at: 4)
                                self.collectionView.insertItems(at: [IndexPath(item: 4, section: 0)])
                                //self.collectionView.moveItem(at: self.messagesIndexPath!, to: IndexPath(item: 4, section: 0))
                                self.collectionView.reloadItems(at: [IndexPath(item: 1, section: 0)])
                        }
                    }
                }

                
            }
            
            
        }
    }
    var messagesIndexPath: IndexPath?

    
    override func viewDidLoad() {
        super.viewDidLoad()

        /*
        self.view.addSubview(self.backgroundImageView)
        self.backgroundImageView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        self.backgroundImageView.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        self.backgroundImageView.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        self.backgroundImageView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        */
        
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongPress(gesture:)))
        self.view.addGestureRecognizer(longPress)
        self.view.backgroundColor = .white

        self.view.addSubview(self.contactHeaderView)
        self.contactHeaderView.translatesAutoresizingMaskIntoConstraints = false
        self.contactHeaderView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 60).isActive = true
        self.contactHeaderView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 0).isActive = true
        self.contactHeaderView.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 0).isActive = true
        self.contactHeaderView.heightAnchor.constraint(equalToConstant: 70).isActive = true
        self.contactHeaderView.tag = 99
        self.contactHeaderView.avatar.tag = 101
        
        //let avatarTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.avatarTapped))
        //self.contactHeaderView.avatar.avatarButton.addTarget(self, action: #selector(self.avatarTapped), for: .touchDown)

        let snCollectionViewLayout = SNCollectionViewLayout()
        snCollectionViewLayout.fixedDivisionCount = 3 // Columns for .vertical, rows for .horizontal
        snCollectionViewLayout.delegate = self
        snCollectionViewLayout.scrollDirection = .vertical
        snCollectionViewLayout.itemSpacing = 6

        self.collectionView = UICollectionView(frame: .zero, collectionViewLayout: snCollectionViewLayout)
        self.collectionView.translatesAutoresizingMaskIntoConstraints = false
        self.collectionView.backgroundColor = .white
        self.view.addSubview(self.collectionView)
        self.collectionView.topAnchor.constraint(equalTo: self.contactHeaderView.bottomAnchor, constant: 20).isActive = true
        self.collectionView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 0).isActive = true
        self.collectionView.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 0).isActive = true
        self.collectionView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
        self.collectionView.register(ContactCell.classForCoder(), forCellWithReuseIdentifier: "contactCell")
        self.collectionView.register(MessagesCell.classForCoder(), forCellWithReuseIdentifier: "messagesCell")
        self.collectionView.register(MissedCallsCell.classForCoder(), forCellWithReuseIdentifier: "missedCallsCell")
        self.collectionView.contentInset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
        self.collectionView.delegate = self
        self.collectionView.dataSource = self

        self.collectionView.collectionViewLayout = snCollectionViewLayout

        
        /*
        self.widgetController = WidgetsViewController(parentController: self, targetView: self.view, targetFrame: CGRect.zero)
        self.widgetController.view.translatesAutoresizingMaskIntoConstraints = false
        self.widgetController.view.topAnchor.constraint(equalTo: self.contactHeaderView.bottomAnchor, constant: 20).isActive = true
        self.widgetController.view.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 0).isActive = true
        self.widgetController.view.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 0).isActive = true
        self.widgetController.view.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true

        self.widgetController.view.backgroundColor = .clear
        // Do any additional setup after loading the view.

        let widget = FormWidget(frame: CGRect.zero)
        let form = Form()
        form +++ Section()
            <<< TextRow(){ row in
                row.title = "Text Row"
                row.placeholder = "Enter text here"
            }
            <<< TextRow(){ row in
                row.title = "Text Row"
                row.placeholder = "Enter text here"
            }
            <<< TextRow(){ row in
                row.title = "Text Row"
                row.placeholder = "Enter text here"
            }
            <<< PhoneRow(){
                $0.title = "Phone Row"
                $0.placeholder = "And numbers here"
        }

        widget.blurEffect = .extraLight
        widget.translatesAutoresizingMaskIntoConstraints = false
        widget.title = "Form"
        //widget.color = UIColor.white.withAlphaComponent(0.05) //  UIColor(0xd1d8e0).withAlphaComponent(0.7)  //UIColor(displayP3Red: 240/255, green: 240/255, blue: 240/255, alpha: 0.5)
        widget.titleViewColor = UIColor.white //UIColor(displayP3Red: 245/255, green: 245/255, blue: 245/255, alpha: 0.9)
        widget.titleLabelColor = .black
        widget.setForm(form: form)
        widget.widgetController = self.widgetController
        
        */
        
        //self.widgetController.addWidget(widgetConfig: WidgetConfig(name: "widget", value: widget))
/*
        let widget2 = TitledWidgetView(frame: CGRect.zero)
        widget2.blurEffect = .extraLight
        widget2.translatesAutoresizingMaskIntoConstraints = false
        widget2.heightAnchor.constraint(equalToConstant: 100).isActive = true
        widget2.title = "New Messages"
        //widget2.color = UIColor.white.withAlphaComponent(0.05) //UIColor(0x34495e) //UIColor(displayP3Red: 240/255, green: 240/255, blue: 240/255, alpha: 0.5)
        widget2.titleViewColor = UIColor.white //UIColor(displayP3Red: 245/255, green: 245/255, blue: 245/255, alpha: 0.9)
        widget2.titleLabelColor = .black
        
        
        self.widgetController.addWidget(widgetConfig: WidgetConfig(name: "widget", value: widget))
        self.widgetController.addWidget(widgetConfig: WidgetConfig(name: "widget", value: widget2))
*/
    }
        
    override func viewDidAppear(_ animated: Bool) {
        if(AppDelegateAccessor.currentUser == nil) {
            let welcomeVC = WelcomeViewController()
            self.present(welcomeVC, animated: true) {
                
            }
        }
        else {
            AppDelegateAccessor.db.collection("users").document(AppDelegateAccessor.currentUser.uid).getDocument { (documentSnapshot, error) in
                
                if(self.initialLoaded == false) {
                    self.loadData()
                }

                guard let document = documentSnapshot else {
                    print(error?.localizedDescription)
                    return
                }
                AppDelegateAccessor.appUser = CKContact(id: document.documentID, dictionary: document.data()!)
            }
        }
    }
    
    func loadInitialData() {
        
        let db = AppDelegateAccessor.db
        
        print("Load Initial Data...")
        
        let reloadBlock = {
            
            print("LoadInitialData - reload")
            
            //let animations = [AnimationType.zoom(scale: 0.1)]
            
            let animations = [AnimationType.from(direction: .top, offset: 30)]
            
            self.collectionView.reloadData()
            
            self.collectionView.performBatchUpdates({
                UIView.animate(views: self.collectionView!.orderedVisibleCells,
                               animations: animations, duration: 0.1, completion: {
                })
            }, completion: { (success) in
                
                self.initialLoaded = true
                self.installDataListeners()

            })
        }
        
        //Contacts Query
        db?.collection(AppDelegateAccessor.currentUser.uid).document("data").collection("contacts").order(by: "name", descending: false).getDocuments(completion: { (querySnapshot, error) in
            
            print("Load Contacts "+String((querySnapshot?.documentChanges.count)!))
            self.handleContactsSnapshot(querySnapshot: querySnapshot, error: error)
            
            if(self.missedCalls.count > 0) {
                self.collectionEntries.insert(CollectionEntry(type: .missedCalls, value: self.missedCalls), at: self.missedCallCellIndex)
            }
            
            if(self.contactsWithMessages.count > 0) {
                self.contactsWithMessagesCellIndex = self.missedCalls.count > 0 ? 4 : 1
                self.collectionEntries.insert(CollectionEntry(type: .messages, value: self.contactsWithMessages), at: self.contactsWithMessagesCellIndex)
                
            }

            reloadBlock()

        })

        
    }
    
    func handleContactsSnapshot(querySnapshot: QuerySnapshot?, error: Error?) {
        guard let documents = querySnapshot?.documents else {
            print("error loading contacts")
            return
        }
        
        self.collectionEntries = []
        for document in documents {
            let person = CKContact(id: document.documentID, dictionary: document.data())
            self.collectionEntries.append(CollectionEntry(type: .contact, value: person))
        }
    }
    
    
    func handleContactsWithMessagesSnapshpt(querySnapshot: QuerySnapshot?, error: Error?) {
        
        guard let documents = querySnapshot?.documents else {
            print("error loading messages collection")
            if(error != nil) {
                print(error?.localizedDescription)
            }
            
            return
        }
        var conversations: [CKConversation]! = []
        for document in documents {
            let conversation = CKConversation(id: document.documentID, dictionary: document.data())
            conversations.append(conversation!)
        }
        
        var groupedByContact: Dictionary<String, [CKConversation]> = Dictionary(grouping: conversations, by: {$0.contact})
        
        let sortedDict = groupedByContact.sorted(by: {$0.value[0].lastMessage < $1.value[0].lastMessage})
        
        self.contactsWithMessages = []
        for item in sortedDict {
            self.contactsWithMessages.append(item.value[0].contactDetails)
        }
        
        if(self.messagesIndexPath == nil) {
            let item = self.missedCallIndexPath != nil ? 4 : 1
            self.collectionEntries.insert(CollectionEntry(type: .messages, value: self.contactsWithMessages), at: item)
            
            self.collectionView.reloadData()
            let animations = [AnimationType.zoom(scale: 1.5)]
            //let animations = [AnimationType.from(direction: .top, offset: 30)]

            
            self.collectionView.performBatchUpdates({
                let cell = self.collectionView.cellForItem(at: IndexPath(item: item, section: 0))
                UIView.animate(views: [cell!],
                               animations: animations, completion: {
                })
            }, completion: { (success) in
                
            })
            
        }
        else {
            let item = self.messagesIndexPath!.item
            
            if(self.contactsWithMessages.count > 0) {
                if(self.collectionEntries[item].type == .messages) {
                    self.collectionEntries[item] = CollectionEntry(type: .messages, value: self.contactsWithMessages)
                    self.collectionView.reloadItems(at: [IndexPath(item: item, section: 0)])
                    
                }
            }
            else {
                self.collectionEntries.remove(at: item)
                self.collectionView.deleteItems(at: [IndexPath(item: item, section: 0)])
                self.messagesIndexPath = nil
                //self.collectionView.reloadData()
            }
        }

        

    }
    
    func handleMissedCallSnapshot(querySnapshot: QuerySnapshot?, error: Error?) {
        

        guard let documents = querySnapshot?.documents else {
            print("error loading missed calls collection")
            if(error != nil) {
                print(error?.localizedDescription)
            }
            return
        }
        
        var conversations: [CKConversation]! = []
        for document in documents {
            let conversation = CKConversation(id: document.documentID, dictionary: document.data())
            conversations.append(conversation!)
        }
        
        var groupedByContact: Dictionary<String, [CKConversation]> = Dictionary(grouping: conversations, by: {$0.contact})
        let sortedDict = groupedByContact.sorted(by: {$0.value[0].lastMessage < $1.value[0].lastMessage})
        
        self.missedCalls = []
        for item in sortedDict {
            self.missedCalls.append(item.value[0].contactDetails)
        }
        
        if(self.missedCallIndexPath == nil) {
            if(self.missedCalls.count > 0) {

                self.collectionEntries.insert(CollectionEntry(type: .missedCalls, value: self.missedCalls), at: self.missedCallCellIndex)
                //self.collectionView.insertItems(at: [IndexPath(item: 0, section: 0)])
                //self.missedCallIndexPath = IndexPath(item: 0, section: 0)
                self.collectionView.reloadData()
                let animations = [AnimationType.zoom(scale: 1.5)]

                self.collectionView.performBatchUpdates({
                    let cell = self.collectionView.cellForItem(at: IndexPath(item: 0, section: 0))
                    UIView.animate(views: [cell!],
                                   animations: animations, completion: {
                    })
                }, completion: { (success) in
                    
                })
            }

        }
        else {
            let item = self.missedCallIndexPath!.item
            
            if(self.missedCalls.count > 0) {
                if(self.collectionEntries[item].type == .missedCalls) {
                    self.collectionEntries[item] = CollectionEntry(type: .missedCalls, value: self.missedCalls)
                    self.collectionView.reloadItems(at: [self.missedCallIndexPath!])
                }
            }
            else {
                self.collectionEntries.remove(at: item)
                self.collectionView.deleteItems(at: [IndexPath(item: item, section: 0)])
                self.missedCallIndexPath = nil
                //self.collectionView.reloadData()
            }
        }
        
    }
    
    var contactsListener: ListenerRegistration!
    var missedCallListener: ListenerRegistration!
    var messagesListener: ListenerRegistration!
    
    
    func installDataListeners() {

        let db  = AppDelegateAccessor.db

        //Contacts Listener
        contactsListener = db?.collection(AppDelegateAccessor.currentUser.uid).document("data").collection("contacts").order(by: "name", descending: false).addSnapshotListener { (querySnapshot, error) in
            
            print("Contacts Listener - Changes="+String((querySnapshot?.documentChanges.count)!))
            self.handleContactsSnapshot(querySnapshot: querySnapshot, error: error)
            

        }
        
        //Missed Calls Listener
        missedCallListener = db?.collection(AppDelegateAccessor.currentUser.uid).document("data").collection("conversations").whereField("lastMessageRead", isEqualTo: false).whereField("lastMessageType", isEqualTo: CKMessageType.missedCall.rawValue).whereField("lastConversationType", isEqualTo: CKConversationType.inbound.rawValue).order(by: "lastMessage", descending: true).addSnapshotListener { (querySnapshot, error) in

            print("MissedCalls Listener - Changes="+String((querySnapshot?.documentChanges.count)!))
            self.handleMissedCallSnapshot(querySnapshot: querySnapshot, error: error)

            if(self.messagesListener == nil) {
                //Messages Listener
                self.messagesListener = db?.collection(AppDelegateAccessor.currentUser.uid).document("data").collection("conversations").order(by: "lastMessage", descending: true).addSnapshotListener { (querySnapshot, error) in
                    
                    print("Messages Listener - Changes="+String((querySnapshot?.documentChanges.count)!))
                    self.handleContactsWithMessagesSnapshpt(querySnapshot: querySnapshot, error: error)
                }
            }

        }


        

    }
    
    
    var initialLoaded: Bool = false
    
    func loadData() {
        
        
        let db  = AppDelegateAccessor.db
        db?.collection("users").document(AppDelegateAccessor.currentUser.uid).addSnapshotListener({ (snapshot, error) in
            
            if(error != nil) {
                print(error?.localizedDescription)
            }
            else {
                if(snapshot != nil && snapshot!.data() != nil) {
                    let user  = CKContact(id: (snapshot?.documentID)!, dictionary: (snapshot?.data())!)
                    
                    if(user != nil) {
                        DispatchQueue.main.async {
                            self.contact = user
                        }
                    }
                }
                else {
                    print("User snapshot data is nil")
                }
                
            }
            
        })
        
        if(!initialLoaded) {
            self.loadInitialData()
            return
        }
    }
    
    var incomingCallWidget: IncomingCallWidget!
    
    func handleIncomingCall(incomingCall: IncomingCall) {
        
        let caller = AppDelegateAccessor.db.collection("users").document(incomingCall.caller)
        caller.getDocument { (snapshot, error) in
            if(error == nil) {
                
                let id = snapshot?.documentID
                let contact = CKContact(id: id!, dictionary: (snapshot?.data())!)
                
                /*
                self.incomingCallWidget = IncomingCallWidget(frame: CGRect.zero)
                self.incomingCallWidget.translatesAutoresizingMaskIntoConstraints = false
                self.incomingCallWidget.title = "incoming call"
                self.incomingCallWidget.contact = contact
                self.incomingCallWidget.incomingCall = incomingCall
                self.incomingCallWidget.blurEffect = .extraLight
                self.incomingCallWidget.titleViewColor = UIColor.white //UIColor(displayP3Red: 245/255, green: 245/255, blue: 245/255, alpha: 0.9)
                self.incomingCallWidget.titleLabelColor = .black
                self.incomingCallWidget.delegate = self
                
                self.widgetController.addWidget(widgetConfig: WidgetConfig(name: "widget", value: self.incomingCallWidget))
                
                AppDelegateAccessor.pulleyController.setDrawerPosition(position: .partiallyRevealed, animated: true)
                */
                
            }
        }
        
    }
    
    @objc func avatarTapped() {
        
        let meSettingsVC = MeSettingsViewController()
        meSettingsVC.contact = self.contact
        meSettingsVC.title = "Settings"

        let transitionDelegate = SPStorkTransitioningDelegate()
        meSettingsVC.transitioningDelegate = transitionDelegate
        meSettingsVC.modalPresentationStyle = .custom
        self.present(meSettingsVC, animated: true, completion: nil)

/*
        AppDelegateAccessor.pulleyController.setDrawerContentViewController(controller: meSettingsVC, animated: false) { (true) in
            
            AppDelegateAccessor.pulleyController.setDrawerPosition(position: .open, animated: true)

            //meSettingsVC.showVoiceRecorder()
            
        }
 */
        
        
    }
    
    func showContact(contact: CKContact) {

        let contactVC = ContactViewController()
        contactVC.contact = contact
        contactVC.callManager = AppDelegateAccessor.callManagerMock
        
        

        let callVC = CallViewController()
        callVC.contact = contact
        callVC.callManager = AppDelegateAccessor.callManagerMock
        
        AppDelegateAccessor.callManagerMock.callVC = callVC
        
        
        let nc = UINavigationController(rootViewController: callVC)
        nc.navigationBar.isTranslucent = true
        nc.navigationBar.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        nc.navigationBar.backgroundColor = .clear
        nc.navigationBar.tintColor = UIColor.white
        
        
        //let pulley = ContactPulleyViewController(contentViewController: nc, drawerViewController: contactVC)
        self.present(nc, animated: true) {
            //pulley.setDrawerPosition(position: .partiallyRevealed, animated: false)
        }
        
        
    }
    
    @objc func handleLongPress(gesture : UILongPressGestureRecognizer!) {
        if gesture.state != .ended {
            return
        }
        let p = gesture.location(in: self.collectionView)
        
        if let indexPath = self.collectionView.indexPathForItem(at: p) {
            // get the cell at indexPath (the one you long pressed)

            let collectionEntry = self.collectionEntries[indexPath.item]
            if(collectionEntry.type == .contact) {
                let contact = collectionEntry.value as! CKContact
                print("Contact ID = "+contact.id)
                
                let action1 = UIAlertAction(title: "Simulate Incoming Unanswered Call", style: .default) { (action) in
                    
                    /*
                    self.missedCalls = []
                    self.missedCalls.append(self.contactsWithMessages[0])
                    
                    
                    if(self.missedCalls.count > 0) {
                        if(self.collectionEntries[self.missedCallCellIndex].type == .missedCalls) {
                            self.collectionEntries[self.missedCallCellIndex] = CollectionEntry(type: .missedCalls, value: self.missedCalls)
                        }
                        else {

                            self.collectionEntries.insert(CollectionEntry(type: .missedCalls, value: self.missedCalls), at: self.missedCallCellIndex)
                            self.collectionView.reloadData()
                            
                            if(self.contactsWithMessages.count > 0) {
                                
                                let previousIndex = self.contactsWithMessagesCellIndex! + 1
                                self.collectionEntries.remove(at: previousIndex)

                                self.contactsWithMessagesCellIndex = 4
                                self.collectionEntries.insert(CollectionEntry(type: .messages, value: self.contactsWithMessages), at: self.contactsWithMessagesCellIndex)
                                
                                self.collectionView.moveItem(at: IndexPath(item: previousIndex, section: 0), to: IndexPath(item: self.contactsWithMessagesCellIndex, section: 0))
                            }
                            
                        }
                    }

  */
                    
                    
                    let message = CKMessage.simuluateUnasweredCall(from: contact, to: AppDelegateAccessor.appUser)
                    AppDelegateAccessor.db.collection("conversations").document(AppDelegateAccessor.currentUser.uid).collection((message.conversationId)).addDocument(data: (message.dictionary))
    
                }
                let action2 = UIAlertAction(title: "Simulate Incoming Call", style: .default) { (action) in
                    
                    let message = CKMessage.simuluateCall(from: contact, to: AppDelegateAccessor.appUser)
                    AppDelegateAccessor.db.collection("conversations").document(AppDelegateAccessor.currentUser.uid).collection((message.conversationId)).addDocument(data: (message.dictionary))
                    
                }
                let action3 = UIAlertAction(title: "Simulate Incoming Text Message", style: .default) { (action) in
                    let message = CKMessage.simulateTextMessage(from: contact, to: AppDelegateAccessor.appUser)
                    AppDelegateAccessor.db.collection("conversations").document(AppDelegateAccessor.currentUser.uid).collection((message.conversationId)).addDocument(data: (message.dictionary))
                }
                let action4 = UIAlertAction(title: "Simulate Outgoing Unanswered Call", style: .default) { (action) in
                    
                    let message = CKMessage.simuluateUnasweredCall(from: AppDelegateAccessor.appUser, to: contact)
                    AppDelegateAccessor.db.collection("conversations").document(AppDelegateAccessor.currentUser.uid).collection((message.conversationId)).addDocument(data: (message.dictionary))
                    
                }

                let action5 = UIAlertAction(title: "Simulate Outgoing Call", style: .default) { (action) in
                    
                    let message = CKMessage.simuluateCall(from: AppDelegateAccessor.appUser, to: contact)
                    AppDelegateAccessor.db.collection("conversations").document(AppDelegateAccessor.currentUser.uid).collection((message.conversationId)).addDocument(data: (message.dictionary))
                    
                }
                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
                }

                let sheet = UIAlertController(title: "Simulator", message: nil, preferredStyle: .actionSheet)
                sheet.addAction(action1)
                sheet.addAction(action2)
                sheet.addAction(action3)
                sheet.addAction(action4)
                sheet.addAction(action5)
                sheet.addAction(cancelAction)
                self.present(sheet, animated: true) {
                }


            }
            else if(collectionEntry.type == .messages) {
                let action1 = UIAlertAction(title: "Reload Preview Pictures", style: .default) { (action) in
                    
                    self.contactsWithMessages = []
                    let cell = self.collectionView.cellForItem(at: indexPath) as! MessagesCell
                    cell.contacts = []
                    cell.contacts = self.contactsWithMessages
                    
                    
                    
                }
                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
                }
                let sheet = UIAlertController(title: "Simulator", message: nil, preferredStyle: .actionSheet)
                sheet.addAction(action1)
                sheet.addAction(cancelAction)
                self.present(sheet, animated: true) {
                }


            }


        } else {
            print("couldn't find index path")
        }
    }


}

extension MeViewController: SNCollectionViewLayoutDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(indexPath.item < self.collectionEntries.count) {
            
            let collectionEntry = self.collectionEntries[indexPath.item]
            
            var vc = UIViewController()
            var title = ""
            
            switch collectionEntry.type! {
            case .messages:
                title = "Messages"
                vc = MessagesViewController()
                (vc as! MessagesViewController).loadData()
            case .missedCalls:
                title = "Missed Calls"
                vc = MissedCallsViewController()
                (vc as! MissedCallsViewController).loadData()
            case .contact:
                self.showContact(contact: collectionEntry.value as! CKContact)
                return
            }

            vc.title = title
            let nc = UINavigationController(rootViewController: vc)
            self.present(nc, animated: true, completion: nil)
            /*
            let transitionDelegate = SPStorkTransitioningDelegate()
            vc.transitioningDelegate = transitionDelegate
            vc.modalPresentationStyle = .custom
            self.present(vc, animated: true, completion: nil)
            */
            
            /*
             AppDelegateAccessor.pulleyController.setDrawerContentViewController(controller: vc, animated: false) { (true) in
             
             AppDelegateAccessor.pulleyController.setDrawerPosition(position: .open, animated: true)
             */
            
             //meSettingsVC.showVoiceRecorder()
             
             //}
            

            
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.collectionEntries.count
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        if(indexPath.item < self.collectionEntries.count) {
            let collectionEntry = self.collectionEntries[indexPath.item]
            if(collectionEntry.type == .contact) {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "contactCell", for: indexPath) as! ContactCell
                cell.contact = collectionEntry.value as! CKContact
                return cell
            }
            else if(collectionEntry.type == .missedCalls) {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "missedCallsCell", for: indexPath) as! MissedCallsCell
                cell.contacts = collectionEntry.value as! [CKContactBasic]
                self.missedCallCell = cell
                self.missedCallIndexPath = indexPath
                return cell
            }
            else if(collectionEntry.type == .messages) {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "messagesCell", for: indexPath) as! MessagesCell
                cell.contacts = collectionEntry.value as! [CKContactBasic]
                self.messagesCell = cell
                self.messagesIndexPath = indexPath
                return cell
            }
        }
        
        return collectionView.dequeueReusableCell(withReuseIdentifier: "contactCell", for: indexPath) as! ContactCell

    }

    func scaleForItem(inCollectionView collectionView: UICollectionView, withLayout layout: UICollectionViewLayout, atIndexPath indexPath: IndexPath) -> UInt {
        if(indexPath.item >= self.collectionEntries.count) {
            return 1
        }

        let collectionEntry = self.collectionEntries[indexPath.item]
        if(collectionEntry.type == .contact) {
            return 1
        }
        else if(collectionEntry.type == .missedCalls) {
            return 2
        }
        else if(collectionEntry.type == .messages) {
            return 2
        }

        return 1
    }
}

extension MeViewController: IncomingCallWidgetDelegate {
    
    func acceptCall(contact: CKContact, callRequest: IncomingCall) {
        let phoneCallDrawerVC = PhoneCallDrawerViewController()
        
        let phoneCallVC = PhoneCallViewController_old()
        phoneCallVC.contact = contact
        
        //phoneCallVC.handleIncomingCall(callRequest: callRequest)


    }
    
    func rejectCall(contact: CKContact, callRequest: IncomingCall) {

        var incomingCall = callRequest
        incomingCall.status = "rejected"
        AppDelegateAccessor.db.collection("calls").document(incomingCall.id).setData(incomingCall.dictionary, merge: true)

    }
    
}

protocol IncomingCallWidgetDelegate {
    func acceptCall(contact: CKContact, callRequest: IncomingCall)
    func rejectCall(contact: CKContact, callRequest: IncomingCall)
}

class IncomingCallWidget: TitledWidgetView {

    var delegate: IncomingCallWidgetDelegate!
    
    var incomingCall: IncomingCall!
    
    var contact: CKContact! {
        
        didSet {
            
            self.avatar.contact = contact
            self.name.text = contact.name
            
        }
    }
    
    var avatar: CKContactView! = {
        
        let view = CKContactView(frame: CGRect.zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.heightAnchor.constraint(equalToConstant: 50).isActive = true
        view.widthAnchor.constraint(equalToConstant: 50).isActive = true
        view.layer.cornerRadius = 25
        
        return view
        
    }()
    
    var name: UILabel = {

        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.regular)
        
        return label
        
    }()
    
    var acceptButton: UIButton = {

        let button = UIButton(type: .custom)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = .green
        button.tintColor = .white
        button.layer.cornerRadius = 4
        button.setTitle("Accept", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.regular)
        button.heightAnchor.constraint(equalToConstant: 50).isActive = true
        button.widthAnchor.constraint(equalToConstant: 100).isActive = true
        
        return button
        
        
    }()

    var rejectButton: UIButton = {
        
        let button = UIButton(type: .custom)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = .red
        button.tintColor = .white
        button.layer.cornerRadius = 4
        button.setTitle("Reject", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.regular)
        button.heightAnchor.constraint(equalToConstant: 50).isActive = true
        button.widthAnchor.constraint(equalToConstant: 100).isActive = true
        
        return button
        
        
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.contentView.addSubview(self.avatar)
        self.contentView.addSubview(self.name)
        self.contentView.addSubview(self.acceptButton)
        self.contentView.addSubview(self.rejectButton)
        
        self.avatar.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 10).isActive = true
        self.avatar.leftAnchor.constraint(equalTo: self.contentView.leftAnchor, constant: 15).isActive = true

        self.name.centerYAnchor.constraint(equalTo: self.avatar.centerYAnchor, constant: 0).isActive = true
        self.name.rightAnchor.constraint(equalTo: self.contentView.rightAnchor, constant: -15).isActive = true
        self.name.leftAnchor.constraint(equalTo: self.avatar.rightAnchor, constant: 10).isActive = true

        self.acceptButton.topAnchor.constraint(equalTo: self.avatar.bottomAnchor, constant: 40).isActive = true
        self.acceptButton.leftAnchor.constraint(equalTo: self.contentView.leftAnchor, constant: 15).isActive = true

        self.rejectButton.topAnchor.constraint(equalTo: self.avatar.bottomAnchor, constant: 40).isActive = true
        self.rejectButton.rightAnchor.constraint(equalTo: self.contentView.rightAnchor, constant: -15).isActive = true
        
        self.bottomAnchor.constraint(equalTo: self.rejectButton.bottomAnchor, constant: 20).isActive = true
        
        self.acceptButton.addTarget(self, action: #selector(self.accept), for: .touchDown)
        self.rejectButton.addTarget(self, action: #selector(self.reject), for: .touchDown)

        
    }
    
    @objc func accept() {

        self.delegate.acceptCall(contact: self.contact, callRequest: self.incomingCall)
        
    }

    @objc func reject() {
        
        self.delegate.rejectCall(contact: self.contact, callRequest: self.incomingCall)
        
    }

    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
}
