//
//  VideoMessageViewController.swift
//  NextGenPhoneApp
//
//  Created by Kieselbach, Oliver on 05.11.18.
//  Copyright © 2018 Kieselbach, Oliver. All rights reserved.
//

import UIKit
import AVKit
import NextGenPhoneFramework


class AbsenceMessageViewController: UIViewController, AVAudioPlayerDelegate {

    var avPlayer: AVPlayer!
    var audioPlayer: AVAudioPlayer!

    var contact: CKContact! {
        didSet {
        }
    }
    
    var completion: (() -> Void)!

    var waveform: WaveformView = {
        let v = WaveformView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.heightAnchor.constraint(equalToConstant: 100).isActive = true
        
        return v
    }()
    
    var siriWave: PXSiriWave = {
        let wave = PXSiriWave(frame: .zero)
        //    siriWave.colors = [NSArray arrayWithObjects: [ViewController colorFromHexCode: @"#2085fc"], [ViewController colorFromHexCode: @"#5efca9"], [ViewController colorFromHexCode: @"#fd4767"], nil];
        wave.colors = [UIColor(0x2085fc), UIColor(0x5efca9), UIColor(0xfd4767)]

        return wave
        
    }()

    
    var imageView: UIImageView = {

        let v = UIImageView(frame: CGRect.zero)
        v.translatesAutoresizingMaskIntoConstraints = false
        v.contentMode = .scaleAspectFill
        v.clipsToBounds = true
        v.layer.cornerRadius = 10
        
        return v
    }()
    
    var activityIndicator: InstagramActivityIndicator = {
        
        let v = InstagramActivityIndicator(frame: CGRect.zero)
        v.translatesAutoresizingMaskIntoConstraints = false
        v.heightAnchor.constraint(equalToConstant: 80).isActive = true
        v.widthAnchor.constraint(equalToConstant: 80).isActive = true
        v.strokeColor = .white
        v.lineWidth = 3
        //v.rotationDuration = 30
        //v.animationDuration = 30
        v.numSegments = 10
        
        return v
        
    }()
    
    var videoProgrssBar: SegmentedProgressBar!
    var videoProgressY: CGFloat = 40
    
    func initAudioPlayer() {

        do {
            let audioData = try Data(contentsOf: self.mediaURL)
            try! self.audioPlayer = AVAudioPlayer(data: audioData, fileTypeHint: "m4a")
            
            self.audioPlayer.isMeteringEnabled = true
            self.audioPlayer.delegate = self
            let displayLink = CADisplayLink(target: self, selector: #selector(self.updateMetersFromPlayback))
            displayLink.add(to: RunLoop.current, forMode: RunLoop.Mode.common)
            
            if(self.videoProgrssBar == nil) {
                self.videoProgrssBar = SegmentedProgressBar(numberOfSegments: 1, duration: (self.contact.statusContent?.duration)!)
                self.videoProgrssBar.frame = CGRect(x: 15, y: self.videoProgressY, width: self.view.frame.size.width - 30, height: 2)
                self.view.addSubview(self.videoProgrssBar)
                
                self.videoProgrssBar.topColor = UIColor.white
                self.videoProgrssBar.bottomColor = UIColor.white.withAlphaComponent(0.25)
            }

        } catch {
            print(error)
        }

    }
    
    func initVideoPlayer() {

        self.view.backgroundColor = .black
        self.avPlayer = AVPlayer(url: self.mediaURL)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(playerItemDidReachEnd(notification:)),
                                               name: Notification.Name.AVPlayerItemDidPlayToEndTime,
                                               object: avPlayer?.currentItem)
        
        let playerLayer = AVPlayerLayer(player: avPlayer)
        playerLayer.videoGravity = .resizeAspectFill
        playerLayer.frame = self.imageView.bounds
        self.imageView.layer.addSublayer(playerLayer)
        //self.view.layer.insertSublayer(playerLayer, at: 1) // 1 because of the gradientLayer that is at 0
        
        self.avPlayer.addBoundaryTimeObserver(forTimes: [NSValue(time: CMTime(seconds: 0.1, preferredTimescale: 10))], queue: DispatchQueue.main) {
            
            self.activityIndicator.stopAnimating()
            self.imageView.isHidden = false
            
            if(self.videoProgrssBar == nil) {
                
                let duration = self.avPlayer.currentItem?.duration
                
                //print(CMTimeGetSeconds(duration!))
                
                self.videoProgrssBar = SegmentedProgressBar(numberOfSegments: 1, duration: CMTimeGetSeconds(duration!))
                self.videoProgrssBar.frame = CGRect(x: 15, y: self.videoProgressY, width: self.view.frame.size.width - 30, height: 2)
                self.view.addSubview(self.videoProgrssBar)
                
                self.videoProgrssBar.topColor = UIColor.white
                self.videoProgrssBar.bottomColor = UIColor.white.withAlphaComponent(0.25)
                
                self.videoProgrssBar.startAnimation()
                
            }
            
        }
        self.avPlayer.addPeriodicTimeObserver(forInterval: CMTimeMake(value: 1, timescale: 1), queue: DispatchQueue.main) { [unowned self] time in
            
            
            let timeElapsed = Float(CMTimeGetSeconds(time))
            
            //print(timeElapsed)
            //self.timeSlider.value = Float(timeElapsed)
            //self.startTimeLabel.text = self.createTimeString(time: timeElapsed)
        }


    }
    
    @objc func updateMetersFromPlayback() {
        if(audioPlayer != nil) {
            audioPlayer.updateMeters()
            var normalizedValue: Float = 0.0
            if(audioPlayer.averagePower(forChannel: 0) < -60 || audioPlayer.averagePower(forChannel: 0) / 20 == 0) {
                normalizedValue = 0
            }
            else {
                normalizedValue = pow(10, audioPlayer.averagePower(forChannel: 0) / 20)
            }

            //let normalizedValue = pow(10, audioPlayer.averagePower(forChannel: 0) / 20)

            //CGFloat value = powf((powf(10.0f, 0.05f * decibels) - powf(10.0f, 0.05f * -60.0f)) * (1.0f / (1.0f - powf(10.0f, 0.05f * -60.0f))), 1.0f / 2.0f);

            //let pow1 = pow(10, 0.05 * audioPlayer.averagePower(forChannel: 0)) - pow(10, 0.05 * -60)
            
            //let normalizedValue = pow((pow1) * (1.0 / (1.0 - pow(10, 0.05 * -60))), 1.0 / 2.0)
            //self.waveform.updateWithLevel(CGFloat(normalizedValue))
            
            self.siriWave.update(withLevel: CGFloat(normalizedValue))
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        

        self.view.backgroundColor = .black
        
        self.view.addSubview(self.imageView)
        self.imageView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        self.imageView.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        self.imageView.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        self.imageView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
        self.imageView.isHidden = true
        self.view.addSubview(self.waveform)
        self.waveform.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 15).isActive = true
        self.waveform.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -15).isActive = true
        self.waveform.centerYAnchor.constraint(equalTo: self.view.centerYAnchor, constant: -50).isActive = true
        self.waveform.backgroundColor = .clear
        self.waveform.waveColor = .white
        self.waveform.isHidden = true
        
        self.siriWave.translatesAutoresizingMaskIntoConstraints = false
        //self.siriWave.frame = CGRect(x: 15, y: self.view.center.y - 50, width: self.view.frame.width - 30, height: 100)
        self.siriWave.backgroundColor = .clear
        self.view.addSubview(self.siriWave)
        self.siriWave.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 0).isActive = true
        self.siriWave.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 0).isActive = true
        self.siriWave.centerYAnchor.constraint(equalTo: self.view.centerYAnchor, constant: -50).isActive = true
        self.siriWave.heightAnchor.constraint(equalToConstant: 150).isActive = true
        self.siriWave.configure()
        self.siriWave.isHidden = true
        
        self.view.addSubview(self.activityIndicator)
        self.activityIndicator.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        self.activityIndicator.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
        self.activityIndicator.isHidden = true
        
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.videoTapped))
        self.view.addGestureRecognizer(tapGesture)
        

    }
    
    
    var mediaURL: URL!
    public func play(completion: @escaping () -> Void) {
        self.completion = completion
        self.activityIndicator.startAnimating()
        
        ConversationKit.shared.storage.reference(withPath: (self.contact.statusContent?.contentURL)!).downloadURL { (url, error) in
            if(error == nil) {

                self.mediaURL = url
                
                
                DispatchQueue.main.async {
                    if(self.mediaURL != nil && self.contact.statusContent?.contentType == .video) {
                        self.imageView.isHidden = false
                        
                        self.imageView.sd_setImage(with: ConversationKit.shared.storage.reference(withPath: (self.contact.statusContent?.contentURL)!+"_thumbnail"), placeholderImage: nil) { (image, error, cache, ref) in
                            self.initVideoPlayer()
                            self.startPlay()
                        }
                    }
                    else {
                        if(self.contact.profileImageRef != "") {
                            self.imageView.isHidden = false
                            self.imageView.sd_setImage(with: ConversationKit.shared.storage.reference(withPath: self.contact.profileImageRef), placeholderImage: nil, completion: { (image, error, cache, ref) in
                                
                                self.imageView.addBlurView(style: .dark)
                            })
                        }
                        self.startPlay()
                        DispatchQueue.main.async {
                            self.activityIndicator.stopAnimating()
                        }

                    }
                }

            }
        }

        
    }
    
    func startPlay() {
        
        if(self.contact.statusContent?.contentType == .video) {
            self.avPlayer.play()
        }
        else {
            self.initAudioPlayer()
            self.siriWave.isHidden = false
            self.audioPlayer.prepareToPlay()
            self.audioPlayer.play()
            self.videoProgrssBar.rewind()
            self.videoProgrssBar.startAnimation()
        }
        
    }
    
    func continuePlay() {
        if(self.contact.statusContent?.contentType == .video) {
            self.avPlayer.play()
        }
        else {
            self.audioPlayer.play()
        }
    }
    
    func pause() {
        if(self.contact.statusContent?.contentType == .video) {
            if(self.avPlayer != nil) {
                self.avPlayer.pause()
            }
        }
        else {
            if(self.audioPlayer != nil) {
                self.audioPlayer.pause()
            }
        }
    }
    
    func stop() {
        self.audioPlayer = nil
        self.videoProgrssBar.removeFromSuperview()
        self.videoProgrssBar = nil
        self.avPlayer.currentItem!.seek(to: CMTime.zero, completionHandler: nil)

        self.completion()

    }
    
    func isPaused() -> Bool {
        if(self.contact.statusContent?.contentType == .video) {
            return self.avPlayer == nil ? true : self.avPlayer.timeControlStatus == .paused
        }
        else {
            return self.audioPlayer == nil ? true : !self.audioPlayer.isPlaying
        }
    }
    
    @objc func videoTapped() {
        
        if self.isPaused() == false {
            self.pause()
            if(self.videoProgrssBar != nil) {
                self.videoProgrssBar.isPaused = true
            }

        } else  {
            if(self.contact.statusContent?.contentType == .audio && self.audioPlayer == nil) {
                self.startPlay()
            }
            else {
                self.continuePlay()
                if(self.videoProgrssBar != nil) {
                    self.videoProgrssBar.isPaused = false
                }
            }
        }
        
        
    }
    
    @objc func closeAwayMessage() {
        self.pause()
        self.dismiss(animated: true) {
        }
    }

    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        self.audioPlayer = nil
        self.completion()
    }
    
    @objc func playerItemDidReachEnd(notification: Notification) {
        if let playerItem: AVPlayerItem = notification.object as? AVPlayerItem {
            self.videoProgrssBar.removeFromSuperview()
            self.videoProgrssBar = nil
            playerItem.seek(to: CMTime.zero, completionHandler: nil)
            self.completion()
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
