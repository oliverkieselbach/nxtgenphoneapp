//
//  MainTabBarController.swift
//  NextGenPhoneApp
//
//  Created by Kieselbach, Oliver on 08.01.19.
//  Copyright © 2019 Kieselbach, Oliver. All rights reserved.
//

import UIKit
import FirebaseFirestore
import FirebaseStorage
import AVKit
import PushKit
import NextGenPhoneFramework
import TwilioVideo


class MainTabBarController: UITabBarController {
    
    var foryouVC: MessagesViewController!
    var contactsVC: ContactsViewController!
    var settingsVC: MeSettingsViewController!

    var initialLoaded: Bool! = false
    
    var activeCall: Call! {
        didSet {
        }
    }
    
    var activeCallAvatar: CKContactView = {
        
        let a = CKContactView(frame: .zero)
        a.translatesAutoresizingMaskIntoConstraints = false
        a.heightAnchor.constraint(equalToConstant: 70).isActive = true
        a.widthAnchor.constraint(equalToConstant: 70).isActive = true
        
        return a
    }()


    override func viewDidLoad() {
        super.viewDidLoad()
        
        AppDelegateAccessor.twilioAudioDevice = TVIDefaultAudioDevice()
        TwilioVideo.audioDevice = AppDelegateAccessor.twilioAudioDevice

        self.tabBar.isTranslucent = true
        //self.tabBar.backgroundImage = UIImage()
        
        self.foryouVC = MessagesViewController()
        self.contactsVC = ContactsViewController()
        self.contactsVC.title = "Browse"
        self.settingsVC = MeSettingsViewController()
        self.settingsVC.title = "Settings"
        
        self.view.frame = CGRect(x: 50, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        
        let foryouNC = UINavigationController(rootViewController: self.foryouVC)
        
        self.foryouVC.tabBarItem = UITabBarItem(title: "For You", image: UIImage(imageLiteralResourceName: "foryou").withRenderingMode(.alwaysTemplate), selectedImage: UIImage(imageLiteralResourceName: "foryou_filled").withRenderingMode(.alwaysTemplate))
        self.contactsVC.tabBarItem = UITabBarItem(title: "Browse", image: UIImage(imageLiteralResourceName: "contacts").withRenderingMode(.alwaysTemplate), selectedImage: UIImage(imageLiteralResourceName: "contacts_filled").withRenderingMode(.alwaysTemplate))
        self.settingsVC.tabBarItem = UITabBarItem(title: "Settings", image: UIImage(imageLiteralResourceName: "myaccount").withRenderingMode(.alwaysTemplate), selectedImage: UIImage(imageLiteralResourceName: "myaccount_filled").withRenderingMode(.alwaysTemplate))
        

        self.viewControllers = [foryouNC, UINavigationController(rootViewController: self.contactsVC), UINavigationController(rootViewController: self.settingsVC)]
        
        let tabBarHeight = self.tabBar.frame.height + 2 //0.2
        
        self.delegate = self
        
        
 
        
        // Do any additional setup after loading the view.
    }

    @objc func showActiveCall() {
        self.selectedIndex = 0
    }
    
    override func viewDidAppear(_ animated: Bool) {

        if(ConversationKit.shared.currentUser == nil) {
            let welcomeVC = WelcomeViewController()
            self.present(welcomeVC, animated: true) {
                
            }
        }
        else {
            ConversationKit.shared.db.collection("users").document(ConversationKit.shared.currentUser.uid).getDocument { (documentSnapshot, error) in
                
                guard let document = documentSnapshot else {
                    print(error?.localizedDescription)
                    return
                }

                ConversationKit.shared.appUser = CKContact(id: document.documentID, dictionary: document.data()!)
                AppDelegateAccessor.registerDevice()

                if(self.initialLoaded == false) {
                    self.loadData()
                }
                
                
                
            }
            

        }
 
    }

    func loadData() {
        self.foryouVC.loadData()
        self.settingsVC.contact = ConversationKit.shared.appUser

        self.initialLoaded = true
    }
    

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension MainTabBarController: UITabBarControllerDelegate {
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        if(activeCall == nil) {
            if(self.viewControllers?.count == 4) {
                self.viewControllers?.removeFirst()
            }
        }

    }
    
}
