//
//  WelcomeViewController.swift
//  NextGenPhoneApp
//
//  Created by Oliver Kieselbach on 06.08.18.
//  Copyright © 2018 Kieselbach, Oliver. All rights reserved.
//

import UIKit
import Firebase
import FirebaseUI
import NextGenPhoneFramework


class WelcomeViewController: UIViewController, FUIAuthDelegate {

    var welcomeLabel: UILabel!
    var signInButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = UIColor.white
        //self.view.translatesAutoresizingMaskIntoConstraints = true
        self.welcomeLabel = UILabel()
        self.welcomeLabel.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(self.welcomeLabel)
        
        self.welcomeLabel.text = "Welcome to NextGenPhone"
        self.welcomeLabel.font = UIFont.boldSystemFont(ofSize: 20)
        self.welcomeLabel.textColor = UIColor.black
        self.welcomeLabel.textAlignment = .center
        self.welcomeLabel.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0).isActive = true
        self.welcomeLabel.centerYAnchor.constraint(equalTo: self.view.centerYAnchor, constant: 0).isActive = true
        self.welcomeLabel.widthAnchor.constraint(equalToConstant: self.view.frame.width).isActive = true
        self.welcomeLabel.sizeToFit()
        
        self.signInButton = UIButton(type: .custom)
        self.signInButton.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(self.signInButton)
        self.signInButton.setTitle("Sign In", for: .normal)
        self.signInButton.layer.cornerRadius = 4
        self.signInButton.titleLabel?.font = UIFont.systemFont(ofSize: 17)
        self.signInButton.setTitleColor(UIColor.white, for: .normal)
        self.signInButton.backgroundColor = UIColor.blue
        self.signInButton.contentEdgeInsets = UIEdgeInsets.init(top: 5, left: 15, bottom: 5, right: 15)
        self.signInButton.sizeToFit()
        self.signInButton.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0).isActive = true
        self.signInButton.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -100).isActive = true
        self.signInButton.addTarget(self, action: #selector(self.signIn), for: .touchDown)
        
        
 
        
        // Do any additional setup after loading the view.
    }
    
    @objc func signIn() {
        
        FUIAuth.defaultAuthUI()?.delegate = self
        let phoneProvider = FUIPhoneAuth.init(authUI: FUIAuth.defaultAuthUI()!)
        FUIAuth.defaultAuthUI()?.providers = [phoneProvider]
        
        //let phoneProvider = FUIAuth.defaultAuthUI()?.providers.first as! FUIPhoneAuth
        phoneProvider.signIn(withPresenting: self, phoneNumber: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func authUI(_ authUI: FUIAuth, didSignInWith authDataResult: AuthDataResult?, error: Error?) {

        defer {
            self.dismiss(animated: true)
        }
        
        if(error != nil) {
            NSLog("Authentication Error: "+(error?.localizedDescription)! )
        }
        else {
            NSLog("Successful authentication")
            ConversationKit.shared.currentUser = authDataResult?.user
            
            let user = CKContact(id: "", name: ConversationKit.shared.currentUser.phoneNumber!, phoneNumber: ConversationKit.shared.currentUser.phoneNumber!, description: "", status: .available, statusUpdated: Date(), profileImageRef: "app/standardProfileImage.png", numberOfCalls: 0,  statusContent: nil, blocked: false, vip: false)
            ConversationKit.shared.db.collection("users").document(ConversationKit.shared.currentUser.uid).setData(user.dictionary)
            
        }
        
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
