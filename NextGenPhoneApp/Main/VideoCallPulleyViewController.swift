//
//  VideoCallPulleyViewController.swift
//  NextGenPhoneApp
//
//  Created by Kieselbach, Oliver on 13.02.19.
//  Copyright © 2019 Kieselbach, Oliver. All rights reserved.
//

import UIKit
import Pulley
import FirebaseFirestore
import FirebaseStorage
import AVKit
import FirebaseFunctions
import NextGenPhoneFramework


class VideoCallPulleyViewController: PulleyViewController {

    var videoCallVC: VideoCallViewController!
    var mainTabBarVC: MainTabBarController!
    

    required init(contentViewController: UIViewController, drawerViewController: UIViewController) {
        super.init(contentViewController: contentViewController, drawerViewController: drawerViewController)
        self.videoCallVC = contentViewController as? VideoCallViewController
        
        self.videoCallVC.callStatus = .inactive
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()


        self.mainTabBarVC = AppDelegateAccessor.mainViewController
        self.addChild(AppDelegateAccessor.mainViewController)
        self.view.addSubview(AppDelegateAccessor.mainViewController.view)
        AppDelegateAccessor.mainViewController.view.frame = self.view.frame
        

    }
    
    
    func closeContact() {

        self.setDrawerPosition(position: .closed, animated: true)
        UIView.animate(withDuration: 0.3, animations: {
            self.mainTabBarVC.view.alpha = 1
        }) { (success) in
        }
    }

    
    func videoCall(contact: CKContact) {
        
    }
    
    func call(contact: CKContact) {
        
        self.videoCall(contact: contact)
        
    }
    
    func contact(contact: CKContact) {
        self.videoCall(contact: contact)
    }
    
    var restoreDrawerPosition: PulleyPosition! = .collapsed




    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
