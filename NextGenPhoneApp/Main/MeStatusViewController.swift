//
//  MeStatusViewController.swift
//  NextGenPhoneApp
//
//  Created by Kieselbach, Oliver on 16.01.19.
//  Copyright © 2019 Kieselbach, Oliver. All rights reserved.
//

import UIKit
import Eureka
import YPImagePicker
import AVKit
import Photos
import FirebaseStorage
import NextGenPhoneFramework

public class AbsenceMessage: Equatable {
    
    enum AbsenceMessageType: String {
        case audio = "audio/m4a"
        case video = "video/mp4"
    }
    
    var asset: AVAsset?
    var thumbnail: UIImage?
    var assetURL: URL?
    var type: AbsenceMessageType!
    var duration: TimeInterval!
    
    convenience init(asset: AVAsset?, duration: TimeInterval, thumbnail: UIImage?, assetURL: URL?, type: AbsenceMessageType) {
        self.init()
        self.asset = asset
        self.thumbnail = thumbnail
        self.assetURL = assetURL
        self.type = type
        self.duration = duration
    }
    
    public static func == (lhs: AbsenceMessage, rhs: AbsenceMessage) -> Bool {
        return lhs.assetURL == rhs.assetURL
    }
    
    
}

public class AbsenceMessageCell: PushSelectorCell<AbsenceMessage> {
    
    
    public override func setup() {
        super.setup()
        
        self.accessoryView = (row as! AbsenceMessageRow).playButton
        self.editingAccessoryView = (row as! AbsenceMessageRow).playButton
    }
    
    public override func update() {
        super.update()
        
        (row as! AbsenceMessageRow).playButton.isHidden = row.value == nil ? true : false
        
        self.detailTextLabel!.text = ""
        selectionStyle = .none
    }
}

public final class AbsenceMessageRow: Row<AbsenceMessageCell>, RowType {
    
    var presenterView: MeStatusViewController!
    var playButton: UIButton!
    
    public required init(tag: String?) {
        super.init(tag: tag)
        
        playButton = UIButton(type: .custom)
        playButton.frame = CGRect(x: 0, y: 0, width: 50, height: 30)
        playButton.layer.cornerRadius = 5
        playButton.layer.borderColor = UIColor.darkText.cgColor
        playButton.layer.borderWidth = 1
        playButton.setTitle("Play", for: .normal)
        playButton.titleLabel?.font = UIFont.systemFont(ofSize: UIFont.smallSystemFontSize)
        playButton.setTitleColor(UIColor.darkText, for: UIControl.State.normal)
        
        
    }
    override public func didSelect() {
        
        let recordVoiceAction = UIAlertAction(title: "Record Audio Message", style: .default) { (action) in
            self.presenterView.showVoiceRecorder()
        }
        let selectVideoAction = UIAlertAction(title: "Select or Record Video Message", style: .default) { (action) in
            self.presenterView.showVideoPicker()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
        }
        
        let clearAction = UIAlertAction(title: "Clear", style: .destructive) { (action) in
            let ref = ConversationKit.shared.storage.reference(forURL: self.presenterView.contact.statusContent!.contentURL)
            let path = ref.fullPath
            ref.delete(completion: { (error) in
                if(error != nil) {
                    print(error?.localizedDescription)
                }
                ConversationKit.shared.storage.reference(withPath: path+"_thumbnail").delete(completion: { (error) in
                    if(error != nil) {
                        print(error?.localizedDescription)
                    }
                })
                
            })
            self.presenterView.contact.statusContent = nil
            self.presenterView.save()
            
            self.value = nil
            self.playButton.isHidden = true
        }
        
        let actionSheet = UIAlertController(title: "Select an option", message: nil, preferredStyle: .actionSheet)
        actionSheet.addAction(recordVoiceAction)
        actionSheet.addAction(selectVideoAction)
        if(self.value != nil) {
            actionSheet.addAction(clearAction)
        }
        actionSheet.addAction(cancelAction)
        
        self.presenterView.present(actionSheet, animated: true, completion: nil)
        
        
        
    }
    
}

class MeStatusViewController: FormViewController {

    var availableColor = UIColor(displayP3Red: 46/255, green: 204/255, blue: 113/255, alpha: 1)
    var notAvailableColor = UIColor(displayP3Red: 231/255, green: 76/255, blue: 60/255, alpha: 1)
    
    var contact: CKContact! {
        didSet {
        }
    }
    
    var headerHeight: CGFloat = 60
    var headerColor: UIColor = .white
    
    var header: UIView = {
        
        let view = UIView()
        return view
    }()
    
    var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .left
        label.font = UIFont.systemFont(ofSize: 36, weight: UIFont.Weight.bold)
        
        return label
    }()

    var absenceMessage: AbsenceMessage! {
        didSet {
            let row = self.form.rowBy(tag: "rejectCallMessageRow") as! AbsenceMessageRow
            row.value = absenceMessage
            row.playButton?.isHidden = true
            if(row.value?.assetURL != nil) {
                row.playButton?.isHidden = false
            }
            row.cell.update()
            row.updateCell()
            
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.header.frame = CGRect(x: 0, y: 30, width: self.view.frame.size.width, height: self.headerHeight)
        self.view.addSubview(self.header)
        self.header.backgroundColor = self.headerColor
        self.header.addSubview(self.titleLabel)
        
        self.titleLabel.leftAnchor.constraint(equalTo: self.header.leftAnchor, constant: 15).isActive = true
        self.titleLabel.rightAnchor.constraint(equalTo: self.header.rightAnchor, constant: -15).isActive = true
        self.titleLabel.centerYAnchor.constraint(equalTo: self.header.centerYAnchor, constant: 0).isActive = true
        self.titleLabel.text = self.title

        
        self.view.backgroundColor = .white
        self.tableView.backgroundColor = .clear
        self.tableView.separatorStyle = .none
        
        self.tableView.contentInset = UIEdgeInsets(top: 30+self.headerHeight, left: 0, bottom: 40, right: 0)
        form +++ Section("Your Status")
            <<< SwitchRow("availableRow"){ row in
                row.title = self.contact.status == .available ? "Available to accept calls" : "Not able to accept calls"
                row.value = self.contact.status == .available ? true : false
                }
                .onChange({ (row) in
                    self.contact.status = row.value == true ? .available : .notAvailable
                    row.title = self.contact.status == .available ? "Available" : "Not able to accept calls"
                    self.contact.statusUpdated = Date()
                    self.save()
                })
            <<< TextAreaRow("statusTextRow") {row in
                row.title = "Status Text"
                row.value = "self.contact.statusText"
                /*
                 row.hidden = Condition.function(["availableRow"], { form in
                 return !((form.rowBy(tag: "availableRow") as? SwitchRow)?.value ?? false)
                 })
                 */
                }.onChange({ (row) in
                }).cellSetup({ (cell, row) in
                    cell.textView.backgroundColor = UIColor(red: 230/255, green: 230/255, blue: 230/255, alpha: 1)
                    cell.textView.layer.cornerRadius = 5
                    cell.textView.contentInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
                }).onCellHighlightChanged({ (cell, row) in
                    if(cell.textView.isFirstResponder == false) {
                        if(row.value != nil) {
                            self.contact.statusUpdated = Date()
                            self.save()
                        }
                    }
                })
            +++ Section("when not accepting incoming calls")
            <<< AbsenceMessageRow("rejectCallMessageRow") { row in
                row.title = "Play Message"
                row.presenterView = self
                row.playButton?.isHidden = true
                row.playButton.addTarget(self, action: #selector(self.playAbsenceVideoMessage), for: .touchDown)
                
                if(self.contact.statusContent != nil) {
                    row.value = AbsenceMessage(asset: nil, duration: self.contact.statusContent!.duration, thumbnail: nil, assetURL: URL(string: contact.statusContent!.contentURL)!, type: .audio)
                    //row.playButton.isHidden = false
                }
                
                }.onChange({ (row) in
                    if(row.value != nil) {
                        self.uploadAbsenceMessage(message: row.value!)
                    }
                })
            

    }
    
    func save() {
        
        ConversationKit.shared.db.collection("users").document(ConversationKit.shared.currentUser.uid).setData(self.contact.dictionary, merge: true)
        
    }

    var uploadVC: UploadViewController!
    
    func uploadAbsenceMessage(message: AbsenceMessage) {
        
        self.uploadVC = UploadViewController()
        uploadVC.header = message.type == .video ? "Upload Video Message" : "Upload Voice Message"
        uploadVC.previewImage = absenceMessage.thumbnail
        
        self.addChild(uploadVC)
        self.view.addSubview(uploadVC.view)
        
        do {
            let path = "users/\(self.contact.id)/\(UUID().uuidString)"
            let url = message.assetURL?.absoluteString
            let absenceMessageData = try Data(contentsOf: (message.assetURL!))
            let absenceMessageUploadTask = UploadTask(data: absenceMessageData, uploadPath: path, metadata: StorageMetadata(dictionary: ["contentType" : message.type.rawValue])!)
            uploadVC.beginUpload(task: absenceMessageUploadTask) { (error) in
                
                if(error == nil) {
                    
                    if(message.thumbnail != nil) {
                        let thumbnailUploadTask = UploadTask(data: message.thumbnail!.pngData()!, uploadPath: path+"_thumbnail", metadata: StorageMetadata(dictionary: ["contentType" : "image/png"])!)
                        self.uploadVC.beginUpload(task: thumbnailUploadTask, completionHandler: { (error) in
                            if(error == nil) {
                                self.uploadVC.header = "Ready"
                                self.uploadVC.finishUpload()
                                
                                self.absenceMessage = message
                                
                                let messageContent = CKContactStatusContent(id: "", contentURL: path, transcript: nil, duration: message.duration, recorded: Date(), contentType: .audio)
                                let docRef = ConversationKit.shared.db.collection("users").document(ConversationKit.shared.currentUser.uid).collection("absenceMessages").addDocument(data: messageContent.dictionary)
                                self.contact.statusContent = messageContent
                                
                                self.save()
                            }
                            else {
                                print(error!.localizedDescription)
                                self.uploadVC.finishUpload()
                                
                            }
                        })
                    }
                    else {
                        self.uploadVC.header = "Ready"
                        self.uploadVC.finishUpload()
                        
                        self.absenceMessage = message
                        
                        self.absenceMessage = message
                        
                        let messageContent = CKContactStatusContent(id: "", contentURL: path, transcript: nil, duration: message.duration, recorded: Date(), contentType: .audio)
                        let docRef = ConversationKit.shared.db.collection("users").document(ConversationKit.shared.currentUser.uid).collection("absenceMessages").addDocument(data: messageContent.dictionary)
                        self.contact.statusContent = messageContent
                        
                        self.save()
                    }
                }
                else {
                    print(error!.localizedDescription)
                    self.uploadVC.finishUpload()
                    
                }
            }
            
            
        } catch  {
            print("exception catch at block - while uploading video")
            print(error.localizedDescription)
        }
        
        
        
    }

    func getPickerConfig() -> YPImagePickerConfiguration {
        var config = YPImagePickerConfiguration()
        config.library.mediaType = .photo
        config.library.onlySquare  = false
        config.onlySquareImagesFromCamera = true
        config.targetImageSize = .cappedTo(size: 1024*1024)
        config.usesFrontCamera = true
        config.showsFilters = false
        /*
         config.filters = [YPFilterDescriptor(name: "Normal", filterName: ""),
         YPFilterDescriptor(name: "Mono", filterName: "CIPhotoEffectMono")]
         */
        config.shouldSaveNewPicturesToAlbum = false
        config.video.compression = AVAssetExportPresetHighestQuality
        //config.albumName = "MyGreatAppName"
        config.screens = [.library, .photo, .video]
        config.startOnScreen = .library
        config.video.recordingTimeLimit = 10
        config.video.libraryTimeLimit = 20
        config.showsCrop = .rectangle(ratio: (3/4))
        config.wordings.libraryTitle = "Gallery"
        config.hidesStatusBar = false
        //config.overlayView = myOverlayView
        config.library.maxNumberOfItems = 1
        config.library.minNumberOfItems = 1
        config.library.numberOfItemsInRow = 3
        config.library.spacingBetweenItems = 2
        config.isScrollToChangeModesEnabled = false
        
        return config
    }

    
    func showVideoPicker() {
        var config = self.getPickerConfig()
        config.screens = [.library, .video]
        config.library.mediaType = .video
        config.video.recordingTimeLimit = 20
        config.video.libraryTimeLimit = 20
        config.video.compression = AVAssetExportPresetMediumQuality
        
        let picker = YPImagePicker(configuration: config)
        picker.didFinishPicking { [unowned picker] items, _ in
            picker.dismiss(animated: false, completion: {
                if let video = items.singleVideo {
                    
                    let asset = AVAsset(url: video.url)
                    let duration = CMTimeGetSeconds(asset.duration)
                    let absenceMessage = AbsenceMessage(asset: asset, duration: duration, thumbnail: video.thumbnail, assetURL: video.url, type: .video)
                    self.absenceMessage = absenceMessage
                    
                }
            })
            
        }
        
        self.present(picker, animated: false) {
        }
    }
    
    func showVoiceRecorder() {
        
        let vc = AbsenceMessageVoiceRecorderViewController()
        vc.title = "Voice Recorder"
        vc.didFinishWithRecording = {audioURL, duration, transcript, cancelled  in
            vc.dismiss(animated: false, completion: {
                if(cancelled == false) {
                    let asset = AVAsset(url: audioURL!)
                    let absenceMessage = AbsenceMessage(asset: asset, duration: duration, thumbnail: nil, assetURL: audioURL, type: .audio)
                    self.absenceMessage = absenceMessage
                }
                else {
                }
            })
        }
        
        let nc = UINavigationController(rootViewController: vc)
        self.present(nc, animated: false) {
        }
        
    }
    
    @objc func playAbsenceVideoMessage() {
        
        let vc = AbsenceMessageViewController()
        vc.contact = self.contact
        self.present(vc, animated: true) {
            //vc.play()
            
        }
        
    }

}
