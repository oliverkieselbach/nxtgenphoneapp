//
//  AppDelegate.swift
//  NextGenPhoneApp
//
//  Created by Kieselbach, Oliver on 31.07.18.
//  Copyright © 2018 Kieselbach, Oliver. All rights reserved.
//

import UIKit
import PushKit
import Firebase
import FirebaseAuth
import FirebaseFirestore
import FirebaseStorage
import FirebaseFunctions
import AVFoundation
import AVKit
import FirebaseDynamicLinks
import NextGenPhoneFramework
import BonsaiController
import CallKit
import TwilioVideo

let UIScreenWidth:CGFloat = UIScreen.main.bounds.width
let UIScreenHeight:CGFloat = UIScreen.main.bounds.height
let AppDelegateAccessor:AppDelegate = (UIApplication.shared.delegate as! AppDelegate)


extension UIImage {
    
    func compressImage() -> UIImage? {
        // Reducing file size to a 10th
        var actualHeight: CGFloat = self.size.height
        var actualWidth: CGFloat = self.size.width
        let maxHeight: CGFloat = 1136.0
        let maxWidth: CGFloat = 640.0
        var imgRatio: CGFloat = actualWidth/actualHeight
        let maxRatio: CGFloat = maxWidth/maxHeight
        var compressionQuality: CGFloat = 0.5
        
        if actualHeight > maxHeight || actualWidth > maxWidth {
            if imgRatio < maxRatio {
                //adjust width according to maxHeight
                imgRatio = maxHeight / actualHeight
                actualWidth = imgRatio * actualWidth
                actualHeight = maxHeight
            } else if imgRatio > maxRatio {
                //adjust height according to maxWidth
                imgRatio = maxWidth / actualWidth
                actualHeight = imgRatio * actualHeight
                actualWidth = maxWidth
            } else {
                actualHeight = maxHeight
                actualWidth = maxWidth
                compressionQuality = 1
            }
        }
        let rect = CGRect(x: 0.0, y: 0.0, width: actualWidth, height: actualHeight)
        UIGraphicsBeginImageContext(rect.size)
        self.draw(in: rect)
        guard let img = UIGraphicsGetImageFromCurrentImageContext() else {
            return nil
        }
        UIGraphicsEndImageContext()
        guard let imageData = img.jpegData(compressionQuality: compressionQuality) else {
            return nil
        }
        return UIImage(data: imageData)
    }
}

public extension UIButton
{
    
    func alignTextUnderImage(spacing: CGFloat = 6.0)
    {
        if let image = self.imageView?.image
        {
            let imageSize: CGSize = image.size
            self.titleEdgeInsets = UIEdgeInsets(top: spacing, left: -imageSize.width, bottom: -(imageSize.height), right: 0.0)
            let labelString = NSString(string: self.titleLabel!.text!)
            let titleSize = labelString.size(withAttributes: [NSAttributedString.Key.font: self.titleLabel!.font])
            self.imageEdgeInsets = UIEdgeInsets(top: -(titleSize.height + spacing), left: 0.0, bottom: 0.0, right: -titleSize.width)
        }
    }
    
    /// Sets the background color to use for the specified button state.
    func setBackgroundColor(color: UIColor, forState: UIControl.State) {
        
        let minimumSize: CGSize = CGSize(width: 1.0, height: 1.0)
        
        UIGraphicsBeginImageContext(minimumSize)
        
        if let context = UIGraphicsGetCurrentContext() {
            context.setFillColor(color.cgColor)
            context.fill(CGRect(origin: .zero, size: minimumSize))
        }
        
        let colorImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        self.clipsToBounds = true
        self.setBackgroundImage(colorImage, for: forState)
    }
    
}

extension UIColor {
    
    enum ActionColor {
        case blue
        case red
        case green
    }
    
    static func actionColor(color: ActionColor) -> UIColor {
        
        switch color {
        case .blue:
            return UIColor(0x1E88E5) //UIColor(0x0288D1)
        case .red:
            return UIColor(0xC62828)
        case .green:
            return UIColor(0x66BB6A)
        default:
            return UIColor.black
        }
    }
}

extension UIImage {
    
    func scaledImage(withSize size: CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        defer { UIGraphicsEndImageContext() }
        draw(in: CGRect(x: 0.0, y: 0.0, width: size.width, height: size.height))
        return UIGraphicsGetImageFromCurrentImageContext()!
    }
    
    func scaleImageToFitSize(size: CGSize) -> UIImage {
        let aspect = self.size.width / self.size.height
        if size.width / aspect <= size.height {
            return scaledImage(withSize: CGSize(width: size.width, height: size.width / aspect))
        } else {
            return scaledImage(withSize: CGSize(width: size.height * aspect, height: size.height))
        }
    }
    
}
enum GradientStyle: String {
    case green = "green"
    case orange = "orange"
    case blue = "blue"
    case violett = "violett"
    case black = "black"
}



func applyGradientToView(view: UIView, gradientStyle: GradientStyle) {
    
    switch gradientStyle {
    case .green:
        view.gradientBackground(from: UIColor(0x08AEEA), to: UIColor(0x2AF598), direction: .topLeftToBottomRight)
    case .blue:
        view.gradientBackground(from: UIColor(0x21D4FD), to: UIColor(0xB721FF), direction: .topLeftToBottomRight)
    case .orange:
        view.gradientBackground(from: UIColor(0xFFE53B), to: UIColor(0xFF2525), direction: .topLeftToBottomRight)
    case .violett:
        view.gradientBackground(from: UIColor(0xFF3CAC), to: UIColor(0x784BA0), direction: .topLeftToBottomRight)
    case .black:
        //view.gradientBackground(from: UIColor(0x4C5A69), to: UIColor(0x1C2A39), direction: .topLeftToBottomRight)
        view.gradientBackground(from: UIColor(0x6c7a89), to: UIColor(0x2e3131), direction: .topLeftToBottomRight)
    default:
        break
    }
    
}

func generateThumbnailFromAsset(asset: AVAsset, forTime time: CMTime) -> UIImage {
    let imageGenerator = AVAssetImageGenerator(asset: asset)
    imageGenerator.appliesPreferredTrackTransform = true
    var actualTime: CMTime = CMTime.zero
    do {
        let imageRef = try imageGenerator.copyCGImage(at: time, actualTime: &actualTime)
        let image = UIImage(cgImage: imageRef)
        return image
    } catch let error as NSError {
        print("\(error.description). Time: \(actualTime)")
    }
    return UIImage()
}

func generateQRCode(from string: String) -> UIImage? {
    let data = string.data(using: String.Encoding.ascii)
    
    if let filter = CIFilter(name: "CIQRCodeGenerator") {
        filter.setValue(data, forKey: "inputMessage")
        let transform = CGAffineTransform(scaleX: 3, y: 3)
        
        if let output = filter.outputImage?.transformed(by: transform) {
            return UIImage(ciImage: output)
        }
    }
    
    return nil
}

class AirDropOnlyActivityItemSource: NSObject, UIActivityItemSource {
    ///The item you want to send via AirDrop.
    let item: Any
    
    init(item: Any) {
        self.item = item
    }
    
    func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
        //using NSURL here, since URL with an empty string would crash
        return NSURL(string: "")!
    }
    
    func activityViewController(_ activityViewController: UIActivityViewController, itemForActivityType activityType: UIActivity.ActivityType?) -> Any? {
        return item
    }
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, PKPushRegistryDelegate {
    

    var deviceToken: String! = nil {
        
        didSet {
            self.registerDevice()
        }
        
    }

    var window: UIWindow?
    var pushRegistry: PKPushRegistry!
    var callManager: CallManager!

    var mainViewController: MainTabBarController!

    var twilioAudioDevice: TVIDefaultAudioDevice!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.

        self.pushRegistry = PKPushRegistry(queue: DispatchQueue.main)
        self.pushRegistry.delegate = self
        self.pushRegistry.desiredPushTypes = Set([PKPushType.voIP])

        
        ConversationKit.configure(clientKey: "communicator")
        //ConversationKit.auth.signOut()
        
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.default
 
        UINavigationBar.appearance().backgroundColor = .white
        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().prefersLargeTitles = true
        UINavigationBar.appearance().barTintColor = .white
        UINavigationBar.appearance().isTranslucent = false
        
        UITabBar.appearance().isTranslucent = false
        UITabBar.appearance().barTintColor = .white
        
        /*
        UIView.appearance().tintColor = UIColor(0x00b5cc)
        UIButton.appearance().tintColor = UIColor(0x00b5cc)
        UINavigationBar.appearance().tintColor = UIColor(0x00b5cc)
        UITabBar.appearance().tintColor = UIColor(0x00b5cc)
        window?.tintColor = UIColor(0x00b5cc)
        */
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        
        self.mainViewController = MainTabBarController()
        
        self.window?.rootViewController = self.mainViewController
        self.window?.makeKeyAndVisible()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.communicateAction(notification:)), name: NSNotification.Name(rawValue: "communicator.communicate.action"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.simulateCommunicateAction(notification:)), name: NSNotification.Name(rawValue: "communicator.communicate.simulate.action"), object: nil)

        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        NSLog("UIApplication open url...")
        
        if let dynamicLink = DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url) {
            self.handleIncomingDynamicLink(dynamicLink)
            return true
        }
        else {
            // Process the URL.
            guard let components = NSURLComponents(url: url, resolvingAgainstBaseURL: true),
                let command = components.path,
                let params = components.queryItems else {
                    print("Invalid URL")
                    return false
            }
            
            
            if let profileId = params.first(where: { $0.name == "id" })?.value {
                print("command = \(command)")
                print("profileId = \(profileId)")
                self.addContact(id: profileId)
                
                return true
            } else {
                print("ProfileId missing")
                return false
            }
        }
        
        return false
    }
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        
        NSLog("UIApplication continue userActivity")
        
        if let incomingURL = userActivity.webpageURL {
            
            
            print("Incoming URL is "+incomingURL.absoluteString)
            
            let linkHandled = DynamicLinks.dynamicLinks().handleUniversalLink(incomingURL) { (dynamicLink, error) in
                
                guard error == nil else {
                    print("Found Error \(error!.localizedDescription)")
                    return
                }
                
                if let dynamicLink = dynamicLink {
                    
                    self.handleIncomingDynamicLink(dynamicLink)
                    
                }
                
            }
            
            if(linkHandled) {
                return true
            }
            else {
                return false
            }
            
        }
        else {
            guard let handle = userActivity.startCallHandle else {return false}
            self.callManager.performStartCallAction(handle: handle)
            return true
        }
        

        return false
    }

    func pushRegistry(_ registry: PKPushRegistry, didUpdate pushCredentials: PKPushCredentials, for type: PKPushType) {
        NSLog("pushRegistry:didUpdatePushCredentials")
        
        
        //self.callManager.fetchAccessToken()
        let deviceToken = pushCredentials.token.reduce("", {$0 + String(format: "%02X", $1) }) //(pushCredentials.token as NSData).description
        print(deviceToken)

        self.deviceToken = deviceToken
        
    }
    
    func pushRegistry(_ registry: PKPushRegistry, didReceiveIncomingPushWith payload: PKPushPayload, for type: PKPushType, completion: @escaping () -> Void) {
        NSLog("pushRegistry:didReceiveIncomingPushWithPayload:forType:completion:")

        guard type == .voIP else {return}
        NSLog(payload.dictionaryPayload.debugDescription)
        
        CommunicatorViewController.handleIncomingCallFromPush(payload: payload, pushEventCompletion: completion)
        
        
    }
    
    func handleIncomingDynamicLink(_ dynamicLink: DynamicLink) {
        
        guard let url = dynamicLink.url else {
            print("Error: dynamic link object does not have an URL property")
            return
        }
        
        print("Handle incoming deeplink = "+url.absoluteString)
        
        let components = URLComponents(url: url, resolvingAgainstBaseURL: true)
        let command = components!.path
        let params = components!.queryItems
        
        /*
        switch command {
        case "/authentication":
            self.videoCallPulleyController.requestAuthenticationFromOtherApp(params: params!)
        default:
            return
        }
        */
        
    }
    
    func addContact(id: String) {
        
        
        ConversationKit.shared.db.collection("users").document(id).getDocument { (documentSnapshot, error) in
            guard let document = documentSnapshot else {
                print(error?.localizedDescription)
                return
                
            }
            
            let contact = CKContact(id: document.documentID, dictionary: document.data()!)
            
            let controller = DJSemiModalViewController()
            
            controller.title = "Add Contact"
            
            let avatar = CKContactView(frame: .zero)
            avatar.translatesAutoresizingMaskIntoConstraints = false
            avatar.heightAnchor.constraint(equalToConstant: 100).isActive = true
            avatar.widthAnchor.constraint(equalToConstant: 100).isActive = true
            avatar.contact = contact
            controller.addArrangedSubview(view: avatar)
            
            let nameLabel = UILabel()
            nameLabel.translatesAutoresizingMaskIntoConstraints = false
            nameLabel.widthAnchor.constraint(equalToConstant: UIScreenWidth * 0.75).isActive = true
            nameLabel.text = contact?.name
            nameLabel.textAlignment = .center
            nameLabel.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
            nameLabel.textColor = UIColor.black
            controller.addArrangedSubview(view: nameLabel)
            
            let statusLabel = UILabel()
            statusLabel.text = "contact?.statusText"
            statusLabel.textAlignment = .center
            statusLabel.font = UIFont.systemFont(ofSize: 17, weight: .regular)
            statusLabel.textColor = UIColor.gray
            statusLabel.numberOfLines = 0
            controller.addArrangedSubview(view: statusLabel)
            
            controller.automaticallyAdjustsContentHeight = true
            
            controller.maxWidth = 420
            
            controller.minHeight = 200
            
            controller.titleLabel.font = UIFont.systemFont(ofSize: 22, weight: UIFont.Weight.bold)
            
            controller.closeButton.setTitle("Add Contact", for: .normal)
            controller.closeButton.backgroundColor = UIColor.actionColor(color: .blue)
            controller.closeButton.setTitleColor(.white, for: .normal)
            
            controller.closeButtonClosure = {() in
                
                CKContact.addToMyContactList(contact: contact!, completion: { (success) in
                    if(success) {
                    }
                })
                
            }
            controller.presentOn(presentingViewController: AppDelegateAccessor.window!.rootViewController!, animated: true, onDismiss: { })
            
            
        }
        
        
    }
    
    func showContact(embeddedContact: CKContactBasic) {
        
        ConversationKit.shared.db.collection(ConversationKit.shared.currentUser.uid).document("data").collection("contacts").document(embeddedContact.id).getDocument { (documentSnapshot, error) in
            guard let document = documentSnapshot else {
                print(error?.localizedDescription)
                return
                
            }
            if(document.data() == nil) {
                ConversationKit.shared.db.collection("users").document(embeddedContact.id).getDocument { (documentSnapshot, error) in
                    guard let document = documentSnapshot else {
                        print(error?.localizedDescription)
                        return
                        
                    }
                    let contact = CKContact(id: document.documentID, dictionary: document.data()!)
                    self.showContact(contact: contact!, modal: false, fromContactList: false)
                    
                }
                
            }
            else {
                let contact = CKContact(id: document.documentID, dictionary: document.data()!)
                self.showContact(contact: contact!, modal: false)
            }
        }

    }
    
    
    func showContact(contact: CKContact, modal: Bool? = true, fromContactList: Bool? = true) {

        CKContactDetailsViewController.open(contact: contact, modal: modal!)

    }
    
    func messageAction(presenterViewController: UIViewController, contact: CKContact) {
/*
        let voice = UIAlertAction(title: "Voice Message", style: .default) { (action) in
            let leaveMessageVC = LeaveAMessageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal)
            let vc = VoiceMessageViewController()
            vc.didCreateMessage = { (leaveMessage, success) in
                if(success) {
                    self.runAssistant(viewController: leaveMessageVC, message: leaveMessage, completion: {(intent) in
                        leaveMessageVC.dismiss(animated: true, completion: {
                            self.sendMessage(leaveMessage: leaveMessage, intent: intent, contact: contact)
                        })
                    })
                }
                else {
                    leaveMessageVC.dismiss(animated: true, completion: {
                    })
                }
            }
            
            leaveMessageVC.messageVC = vc
            //let transitionDelegate = SPStorkTransitioningDelegate()
            //leaveMessageVC.transitioningDelegate = transitionDelegate
            //leaveMessageVC.modalPresentationStyle = .custom
            //transitionDelegate.customHeight = 350
            let nc = UINavigationController(rootViewController: leaveMessageVC)
            nc.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            leaveMessageVC.title = "Voice Message"
            presenterViewController.present(nc, animated: true, completion: nil)
        }
        
        let video = UIAlertAction(title: "Video Message", style: .default) { (action) in
            /*
            let leaveMessageVC = LeaveAMessageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal)
            
            self.videoCallVC.videoMessageRecorderVC.recordButton.isHidden = false
            self.videoCallVC.videoMessageRecorderVC.closeButton.isHidden = false
            self.restoreDrawerPosition = self.drawerPosition
            
            self.videoCallVC.videoMessageRecorderVC.didCreateMessage = {(leaveMessage, success) in
                if(success) {
                    let nc = UINavigationController(rootViewController: leaveMessageVC)
                    nc.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
                    let blankVC = UIViewController()
                    blankVC.view.backgroundColor = .white
                    leaveMessageVC.title = "Video Message"
                    leaveMessageVC.messageVC = blankVC
                    presenterViewController.present(nc, animated: true, completion: {
                        self.runAssistant(viewController: leaveMessageVC, message: leaveMessage, completion: {(intent) in
                            leaveMessageVC.dismiss(animated: true, completion: {
                                self.videoCallVC.videoMessageRecorderVC.recordButton.isHidden = true
                                self.setDrawerPosition(position: .open, animated: true)
                                self.sendMessage(leaveMessage: leaveMessage, intent: intent, contact: contact)
                            })
                        })
                    })
                    
                }
                else {
                    self.videoCallVC.videoMessageRecorderVC.recordButton.isHidden = true
                    self.setDrawerPosition(position: .open, animated: true)
                }
            }
 */
            /*
             
             let leaveMessageVC = LeaveAMessageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal)
             leaveMessageVC.didCancel = { () in
             self.videoCallVC.startPreview()
             }
             let vc = VideoMessageViewController()
             
             vc.didCreateMessage = { (leaveMessage, success) in
             if(success) {
             self.videoCallVC.startPreview()
             self.runAssistant(viewController: leaveMessageVC, message: leaveMessage, completion: {(topic) in
             leaveMessageVC.dismiss(animated: true, completion: {
             self.sendMessage(leaveMessage: leaveMessage, topic: topic, contact: contact)
             })
             })
             }
             else {
             self.videoCallVC.startPreview()
             leaveMessageVC.dismiss(animated: true, completion: {
             })
             }
             }
             
             leaveMessageVC.messageVC = vc
             //let transitionDelegate = SPStorkTransitioningDelegate()
             //leaveMessageVC.transitioningDelegate = transitionDelegate
             //leaveMessageVC.modalPresentationStyle = .custom
             
             self.videoCallVC.camera!.stopCapture(completion: { (error) in
             if(error != nil) {
             print(error?.localizedDescription)
             }
             
             let nc = UINavigationController(rootViewController: leaveMessageVC)
             nc.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
             nc.navigationBar.isTranslucent = true
             nc.navigationBar.backgroundColor = .clear
             leaveMessageVC.title = ""
             presenterViewController.present(nc, animated: true, completion: {
             self.videoCallVC.remoteView?.alpha = 0
             })
             
             //presenterViewController.present(leaveMessageVC, animated: true, completion: nil)
             })
             */
        }
        
        let text = UIAlertAction(title: "Text Message", style: .default) { (action) in
            let leaveMessageVC = LeaveAMessageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal)
            let vc = TextMessageViewController()
            vc.didCreateMessage = { (leaveMessage, success) in
                
                if(success) {
                    self.runAssistant(viewController: leaveMessageVC, message: leaveMessage, completion: {(intent) in
                        leaveMessageVC.dismiss(animated: true, completion: {
                            self.sendMessage(leaveMessage: leaveMessage, intent: intent, contact: contact)
                        })
                    })
                }
                else {
                    leaveMessageVC.dismiss(animated: true, completion: {
                    })
                }
            }
            
            leaveMessageVC.messageVC = vc
            
            let nc = UINavigationController(rootViewController: leaveMessageVC)
            nc.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            leaveMessageVC.title = "Text Message"
            presenterViewController.present(nc, animated: true, completion: nil)
            
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
        }
        
        let actionSheet = UIAlertController(title: "Leave A Message", message: nil, preferredStyle: .actionSheet)
        //actionSheet.popoverPresentationController?.sourceView = self.contactVC.contactProfileHeader.messageMeButton
        //actionSheet.popoverPresentationController?.sourceRect = self.contactVC.contactProfileHeader.messageMeButton.bounds
        
        actionSheet.addAction(voice)
        actionSheet.addAction(video)
        actionSheet.addAction(text)
        actionSheet.addAction(cancel)
        presenterViewController.present(actionSheet, animated: true, completion: nil)
        
        
        */
    }
    
    //Obsolete because of new verson of "runAssistant" method
    func runAssistant(viewController: UIViewController, message: CKMessage, completion: @escaping (String?, CKIntent?) -> Void)  {
        let assistantVC = AssistantDetectIntentionViewController()
        assistantVC.message = message
        
        assistantVC.resultHandler = {(success, intent) in
            assistantVC.dismiss(animated: true, completion: {
            })
            if(success) {

                completion(assistantVC.message.text, intent)
            }
            else {
                completion(assistantVC.message.text, nil)
            }
        }
        
        assistantVC.transitioningDelegate = self
        assistantVC.modalPresentationStyle = .custom
        viewController.present(assistantVC, animated: true) {
            assistantVC.detect()
        }
        
    }

    func runAssistant(viewController: UIViewController, message: CKMessage, action: CKAction, completion: @escaping (CKIntent?) -> Void) {
        let assistantVC = AssistantDetectIntentionViewController()
        
        assistantVC.message = message
        
        assistantVC.resultHandler = {(success, intent) in
            if(success) {
                
                assistantVC.dismiss(animated: true, completion: {
                    completion(intent)
                })
                
            }
            else {
                assistantVC.dismiss(animated: true, completion: {
                    completion(nil)
                })
            }
        }
        
        assistantVC.transitioningDelegate = self
        assistantVC.modalPresentationStyle = .custom
        viewController.present(assistantVC, animated: true) {
            assistantVC.detect(action: action)
        }
    }

    func runAssistantX(viewController: UIViewController, message: CKMessage, completion: @escaping (Bool, CKMessage) -> Void) {
        let assistantVC = AssistantDetectIntentionViewController()
        
        assistantVC.message = message
        assistantVC.resultHandler = {(success, intent) in
            if(success) {
                
                var topicMessage = message
                topicMessage.topicDetails = intent
                topicMessage.topic = intent?.name
                
                assistantVC.dismiss(animated: true, completion: {
                    let messageSendVC = NewConversationSendViewController()
                    let messageSendNC = UINavigationController(rootViewController: messageSendVC)
                    messageSendNC.navigationBar.prefersLargeTitles = false
                    messageSendVC.message = topicMessage
                    messageSendVC.resultHandler = {(success, resultMessage) in
                        messageSendNC.dismiss(animated: true, completion: {
                            completion(success, resultMessage)
                        })
                    }
                    
                    //messageSendVC.createForm()
                    viewController.present(messageSendNC, animated: true, completion: {
                    })
                })
                
            }
            else {
                completion(success, message)
            }
        }

        assistantVC.transitioningDelegate = self
        assistantVC.modalPresentationStyle = .custom
        viewController.present(assistantVC, animated: true) {
            assistantVC.detect()
        }
    }

    
    func sendMessage(message: CKMessage) {
        
        if(message.messageType != .text) {
            
            do {
                let data = try Data(contentsOf: URL(string: message.mediaURL!)!)
                let storageMetaData = StorageMetadata(dictionary: ["contentType": message.mediaType.rawValue])
                
                
                ConversationKit.shared.storage.reference(withPath: message.mediaRef!).putData(data, metadata: storageMetaData) { (metadata, error) in
                    
                    let asset = AVAsset(url: URL(string: message.mediaURL!)!)
                    ConversationKit.shared.storage.reference(withPath: message.mediaRef!).downloadURL(completion: { (url, error) in
                        if(message.messageType == .video) {
                            let image = generateThumbnailFromAsset(asset: asset, forTime: CMTime(seconds: 1, preferredTimescale: 1))
                            
                            ConversationKit.shared.storage.reference(withPath: message.mediaRef!+"_thumbnail").putData(image.pngData()!, metadata: StorageMetadata(dictionary: ["contentType": "image/png"]))
                        }
                        
                        var toBeSendMessage = message
                        
                        toBeSendMessage.mediaURL = url?.absoluteString
                        
                        toBeSendMessage.send(completion: { (success) in
                            
                        })
                        
                        
                    })
                    
                }
                
            }
            catch {
                print(error.localizedDescription)
                
            }
            
        }
        else {
            message.send(completion: { (success) in
                
            })
        }

    }
        
    func requestAuthenticationFromOtherApp(params: [URLQueryItem])  {
        
        var clientKey = ""
        var callbackURL = ""
        
        for queryItem in params {
            if(queryItem.name == "clientKey") {
                clientKey = queryItem.value == nil ? "" : queryItem.value!
            }
            if(queryItem.name == "appCallbackURL") {
                callbackURL = queryItem.value == nil ? "" : queryItem.value!
            }
        }
        
        print("Authentication Request - clientKey = "+clientKey)
        print("Authentication Request - appCallbackURL = "+callbackURL)
        
        let controller = DJSemiModalViewController()
        
        controller.title = "Authentication Request"
        
        controller.automaticallyAdjustsContentHeight = true
        
        controller.maxWidth = 420
        
        controller.minHeight = 200
        
        controller.titleLabel.font = UIFont.systemFont(ofSize: 22, weight: UIFont.Weight.bold)
        
        controller.closeButton.setTitle("Give Access", for: .normal)
        controller.closeButton.backgroundColor = UIColor.actionColor(color: .blue)
        controller.closeButton.setTitleColor(.white, for: .normal)
        
        controller.closeButtonClosure = {() in
            
            let functions = Functions.functions()
            functions.httpsCallable("authenticate").call(["uid": ConversationKit.shared.currentUser.uid]) { (result, error) in
                
                if let error = error as NSError? {
                    if error.domain == FunctionsErrorDomain {
                        let code = FunctionsErrorCode(rawValue: error.code)
                        let message = error.localizedDescription
                        let details = error.userInfo[FunctionsErrorDetailsKey]
                        print(message)
                    }
                }
                
                let data = result?.data as! [String: Any]
                
                let token = data["token"]
                print(token)
                
                let callback = URL(string: callbackURL)
                var components = URLComponents(url: callback!, resolvingAgainstBaseURL: true)
                let tokenParam = URLQueryItem(name: "token", value: token! as! String)
                components?.queryItems = [tokenParam]
                
                UIApplication.shared.open((components?.url)!, options: [:], completionHandler: nil)
                
                
            }
            
            
        }
        controller.presentOn(presentingViewController: AppDelegateAccessor.mainViewController, animated: true, onDismiss: { })
        
        
    }
    
    func registerDevice() {
        
        if(self.deviceToken != nil) {
            if(ConversationKit.shared.currentUser != nil) {
                ConversationKit.shared.db.collection("registereddevices").document(ConversationKit.shared.currentUser.uid).setData(["deviceToken_ios" : deviceToken])
                ConversationKit.shared.db.collection("registereddevices").document("general").setData(["deviceToken_ios" : deviceToken])
            }
        }
        
    }
    
    var transitionFromView: UIView?
    var presentedViewController: UIViewController?
    
    @objc func communicateAction(notification: NSNotification) {

        let userInfo = notification.userInfo
        if(userInfo!["contact"] != nil) {
            
            let contact = userInfo!["contact"] as? CKContact
            if(contact != nil) {
                
                let conversationId = userInfo!["conversationId"] == nil ? CKConversation.new(contact: contact!).id : userInfo!["conversationId"] as! String
                let contextMessage = userInfo!["contextMessage"] == nil ? nil : userInfo!["contextMessage"] as! CKMessage
                let presentingViewController = userInfo!["presentingViewController"] == nil ? AppDelegateAccessor.window?.rootViewController : userInfo!["presentingViewController"] as! UIViewController
                
                let incomingCall = userInfo!["incomingCall"] == nil ? false : userInfo!["incomingCall"] as! Bool
                let call = userInfo!["call"] == nil ? nil : userInfo!["call"] as! Call
                /*
                let room = userInfo!["roomId"] == nil ? "" : userInfo!["roomId"] as! String
                let intent = userInfo!["intent"] == nil ? nil : userInfo!["intent"] as! CKIntent
                */
                
                let communicatorVC = CommunicatorViewController()
                communicatorVC.videoCallVC.initCallKit()
            

                communicatorVC.contact = contact
                communicatorVC.conversationId = conversationId
                communicatorVC.contextMessage = contextMessage

                let communicatorNC = UINavigationController(rootViewController: communicatorVC)
                communicatorNC.navigationBar.prefersLargeTitles = false
                
                communicatorNC.navigationBar.isTranslucent = true
                communicatorNC.view.backgroundColor = .clear
                communicatorNC.navigationBar.setBackgroundImage(UIImage(), for: .default)
                communicatorNC.navigationBar.barTintColor = .clear
                communicatorNC.navigationBar.backgroundColor = .clear


                if(userInfo!["fromView"] != nil) {
                    self.transitionFromView = userInfo!["fromView"] as? UIView
                    if(self.transitionFromView != nil) {
                        communicatorNC.transitioningDelegate = self
                        communicatorNC.modalPresentationStyle = .custom
                    }
                }
                

                presentingViewController!.present(communicatorNC, animated: true, completion: {
                    if(incomingCall) {
                        communicatorVC.reportIncomingCall(uuid: UUID(), call: call!)
                    }
                })

            }
            
            
        }

        
    }
    

    
    @objc func simulateCommunicateAction(notification: NSNotification) {

        let userInfo = notification.userInfo
        if(userInfo!["contact"] != nil) {
            
            let contact = userInfo!["contact"] as? CKContact
            var message = CKMessage.simulateVoiceMessage(from: ConversationKit.shared.appUser, to: contact!, text: "Hallo, bitte um Rückruf")

            self.runAssistant(viewController: AppDelegateAccessor.window!.rootViewController!, message: message) { (transcript, intent) in
                
                message.text = transcript
                if(intent != nil) {
                    message.topic = intent?.name
                    message.topicDetails = intent
                }

                message.send(completion: { (success) in
                })

            }
/*
            self.runAssistant(viewController: AppDelegateAccessor.window!.rootViewController!, message: message) { (success, message) in
                message.send(completion: { (success) in
                })
            }
*/
            
        }

        
    }
    
    






}

extension AppDelegate: BonsaiControllerDelegate {
    
    public func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        
        self.presentedViewController = presented
        
        if(presented.classForCoder == UINavigationController.classForCoder()) {
            return BonsaiController(fromView: self.transitionFromView!, blurEffectStyle: .dark,  presentedViewController: presented, delegate: self)
        }
        else if(presented.classForCoder == AssistantDetectIntentionViewController.classForCoder()){
            return BonsaiController(fromDirection: .bottom, blurEffectStyle: .dark, presentedViewController: presented, delegate: self)
        }
        return nil
    }
    
    public func frameOfPresentedView(in containerViewFrame: CGRect) -> CGRect {
        
        if(self.presentedViewController?.classForCoder == AssistantDetectIntentionViewController.classForCoder()) {
            return CGRect(origin: CGPoint(x: 10, y: containerViewFrame.size.height * 0.50), size: CGSize(width: containerViewFrame.width - 20, height: (containerViewFrame.height * 0.50) - 20) )
        }
        else {
            return CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: containerViewFrame.width, height: containerViewFrame.height))
        }

    }
}


