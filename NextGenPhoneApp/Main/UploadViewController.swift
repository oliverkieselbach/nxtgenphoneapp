//
//  UploadViewController.swift
//  NextGenPhoneApp
//
//  Created by Kieselbach, Oliver on 02.11.18.
//  Copyright © 2018 Kieselbach, Oliver. All rights reserved.
//

import UIKit
import NextGenPhoneFramework
import FirebaseStorage

struct UploadTask {
    
    var data: Data
    var uploadPath: String
    var metadata: StorageMetadata
    
}

class UploadViewController: UIViewController {

    var header: String = "" {
        didSet {
            self.titleLabel.text = header
        }
    }
    var previewImage: UIImage! {
        didSet {
            self.preview.image = previewImage
        }
    }
    
    var titleLabel: UILabel = {
        
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 24, weight: UIFont.Weight.bold)
        return label
        
    }()
    
    var preview: UIImageView = {
        
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 10
        view.heightAnchor.constraint(equalToConstant: 200).isActive = true
        view.widthAnchor.constraint(equalToConstant: 200).isActive = true
        view.clipsToBounds = true
        
        return view
    }()
    
    var blurpreview: UIVisualEffectView = {
        
        let effect = UIBlurEffect(style: .light)
        let view = UIVisualEffectView(effect: effect)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.alpha = 0.9
        
        return view
    }()

    
    var activityIndicator: InstagramActivityIndicator = {

        let v = InstagramActivityIndicator(frame: CGRect.zero)
        v.translatesAutoresizingMaskIntoConstraints = false
        v.heightAnchor.constraint(equalToConstant: 80).isActive = true
        v.widthAnchor.constraint(equalToConstant: 80).isActive = true
        v.strokeColor = .black
        v.lineWidth = 3
        //v.rotationDuration = 30
        //v.animationDuration = 30
        v.numSegments = 10
        
        return v
        
    }()
    
    var statusView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.layer.cornerRadius = 10
        
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)

        self.view.addSubview(statusView)
        self.statusView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -40).isActive = true
        self.statusView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 15).isActive = true
        self.statusView.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -15).isActive = true
        self.statusView.heightAnchor.constraint(equalToConstant: 300).isActive = true

        self.statusView.addSubview(titleLabel)
        self.titleLabel.topAnchor.constraint(equalTo: self.statusView.topAnchor, constant: 20).isActive = true
        self.titleLabel.leftAnchor.constraint(equalTo: self.statusView.leftAnchor, constant: 15).isActive = true
        self.titleLabel.rightAnchor.constraint(equalTo: self.statusView.rightAnchor, constant: -15).isActive = true

        self.statusView.addSubview(self.preview)
        self.preview.centerXAnchor.constraint(equalTo: self.statusView.centerXAnchor).isActive = true
        self.preview.bottomAnchor.constraint(equalTo: self.statusView.bottomAnchor, constant: -20).isActive = true

        self.preview.addSubview(blurpreview)
        self.blurpreview.topAnchor.constraint(equalTo: self.statusView.topAnchor).isActive = true
        self.blurpreview.leftAnchor.constraint(equalTo: self.statusView.leftAnchor).isActive = true
        self.blurpreview.rightAnchor.constraint(equalTo: self.statusView.rightAnchor).isActive = true
        self.blurpreview.bottomAnchor.constraint(equalTo: self.statusView.bottomAnchor).isActive = true

        
        self.preview.addSubview(self.activityIndicator)
        self.activityIndicator.centerXAnchor.constraint(equalTo: self.preview.centerXAnchor).isActive = true
        self.activityIndicator.centerYAnchor.constraint(equalTo: self.preview.centerYAnchor).isActive = true

        //self.activityIndicator.center = self.statusView.center

        //self.activityIndicator.startAnimating()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    
    
    func beginUpload(task: UploadTask, completionHandler: @escaping (Error?) -> Void) {
        
        
        DispatchQueue.main.async {
            if(self.activityIndicator.isAnimating == false) {
                self.activityIndicator.startAnimating()
            }
        }
        
        ConversationKit.shared.storage.reference(withPath: task.uploadPath).putData(task.data, metadata: task.metadata) { (metadata, error) in
            completionHandler(error)
        }
    
            
    }
    
    func finishUpload() {
        self.activityIndicator.stopAnimating()

        UIView.animate(withDuration: 0.5, animations: {
            self.blurpreview.alpha = 0
        }) { (true) in
            
            let t = Timer.scheduledTimer(withTimeInterval: 2, repeats: false, block: { (timer) in
                    timer.invalidate()
                    self.view.removeFromSuperview()
                    self.removeFromParent()
            })
            
            
        }
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
