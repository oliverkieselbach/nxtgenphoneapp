//
//  VoiceRecorderViewController.swift
//  NextGenPhoneApp
//
//  Created by Kieselbach, Oliver on 06.11.18.
//  Copyright © 2018 Kieselbach, Oliver. All rights reserved.
//

import UIKit
import AVFoundation
import Speech
import NextGenPhoneFramework


class AbsenceMessageVoiceRecorderViewController: VoiceRecorderViewController {
    
    var playbackButton: UIButton = {
        
        let button = UIButton(type: .custom)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.heightAnchor.constraint(equalToConstant: 80).isActive = true
        button.widthAnchor.constraint(equalToConstant: 80).isActive = true
        button.layer.cornerRadius = 40
        button.tintColor = .black
        button.backgroundColor = UIColor(red: 230/255, green: 230/255, blue: 230/255, alpha: 230/255)
        button.setImage(UIImage(imageLiteralResourceName: "play").withRenderingMode(UIImage.RenderingMode.alwaysTemplate), for: .normal)
        button.setImage(UIImage(imageLiteralResourceName: "pause").withRenderingMode(UIImage.RenderingMode.alwaysTemplate), for: .selected)
        button.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        
        return button
        
    }()
    
    var resetButton: UIButton = {
        
        let button = UIButton(type: .custom)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Reset", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.contentEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        //button.titleEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        button.layer.cornerRadius = 5
        button.backgroundColor = UIColor(red: 230/255, green: 230/255, blue: 230/255, alpha: 230/255)
        
        return button
        
    }()
    
    var saveButton: UIButton = {
        
        let button = UIButton(type: .custom)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Save", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.contentEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        button.layer.cornerRadius = 5
        button.backgroundColor = UIColor(red: 230/255, green: 230/255, blue: 230/255, alpha: 230/255)
        
        return button
        
    }()
    
    override var audioURL: URL? {
        didSet {
            self.playbackButton.isHidden = audioURL == nil ? true : false
            self.recordButton.isHidden = audioURL == nil ? false : true
            self.resetButton.isHidden = self.playbackButton.isHidden
            self.saveButton.isHidden = self.playbackButton.isHidden
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addSubview(self.playbackButton)
        self.playbackButton.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0).isActive = true
        self.playbackButton.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -100).isActive = true
        self.playbackButton.addTarget(self, action: #selector(self.playback), for: .touchUpInside)
        
        self.view.addSubview(self.resetButton)
        self.view.addSubview(self.saveButton)
        
        self.resetButton.centerYAnchor.constraint(equalTo: self.playbackButton.centerYAnchor).isActive = true
        self.resetButton.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 15).isActive = true
        self.resetButton.addTarget(self, action: #selector(self.reset), for: .touchDown)
        
        self.saveButton.centerYAnchor.constraint(equalTo: self.playbackButton.centerYAnchor).isActive = true
        self.saveButton.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -15).isActive = true
        self.saveButton.addTarget(self, action: #selector(self.save), for: .touchDown)

    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.resetButton.sizeToFit()
        self.saveButton.sizeToFit()
    }
    
    @objc func reset() {
        self.audioURL = nil
    }
    
    @objc func save() {
        let duration = try! AVAudioPlayer(contentsOf: self.audioURL!).duration
        self.didFinishWithRecording!(self.audioURL, duration, "", false)
    }
    
    @objc func playback() {
        
        if(self.audioPlayer == nil) {
            try! self.audioPlayer = AVAudioPlayer(contentsOf: self.audioURL!)
            self.audioPlayer.isMeteringEnabled = true
            self.audioPlayer.prepareToPlay()
            self.audioPlayer.play()
            self.playbackButton.isSelected = true
            self.audioPlayer.delegate = self
            let displayLink = CADisplayLink(target: self, selector: #selector(updateMetersFromPlayback))
            displayLink.add(to: RunLoop.current, forMode: RunLoop.Mode.common)
        }
        else {
            
            if(self.audioPlayer.isPlaying) {
                self.audioPlayer.pause()
                self.playbackButton.isSelected = false
            }
            else {
                self.audioPlayer.play()
                self.playbackButton.isSelected = true
            }
            
        }
        
    }
    
    override func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        self.playbackButton.isSelected = false
        self.audioPlayer.stop()
        self.audioPlayer = nil
    }




}

class VoiceRecorderViewController: UIViewController, RecordingButtonDelegate, AVAudioPlayerDelegate {

    public var didFinishWithRecording: ((URL?, TimeInterval, String?, Bool) -> Void)?

    //public func didFinishWithRecording(completion: @escaping (_ audioURL: URL, _ transcript: String, _ cancelled: Bool) -> Void)
    
    var audioRecorder: AVAudioRecorder!
    var audioPlayer: AVAudioPlayer!
    /*
    var waveform: WaveformView = {
        let v = WaveformView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.heightAnchor.constraint(equalToConstant: 100).isActive = true
        
        return v
    }()
    */

    var waveform: PXSiriWave = {
        let v = PXSiriWave(frame: .zero)
        v.translatesAutoresizingMaskIntoConstraints = false
        v.heightAnchor.constraint(equalToConstant: 150).isActive = true
        v.colors = [UIColor(0x2085fc), UIColor(0x5efca9), UIColor(0xfd4767)]

        return v
    }()

    var recordButton: RecordingButton = {

        let button = RecordingButton(type: .custom)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.heightAnchor.constraint(equalToConstant: 100).isActive = true
        button.widthAnchor.constraint(equalToConstant: 100).isActive = true
        button.outlineColor = UIColor(red: 230/255, green: 230/255, blue: 230/255, alpha: 230/255)
        return button
    }()
    
    
    var transriptView: UITextView!

    var audioURL: URL? {
        didSet {
            
            if(self.transriptView != nil) {
                self.transriptView.removeFromSuperview()
                self.transriptView = nil
            }

        }
    }
    
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        
        self.audioURL = nil

        let height = self.view.frame.height
        self.view.addSubview(self.waveform)
        self.waveform.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 0).isActive = true
        self.waveform.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 0).isActive = true
        self.waveform.centerYAnchor.constraint(equalTo: self.view.centerYAnchor, constant: 0).isActive = true
        self.waveform.backgroundColor = .clear
        self.waveform.intensity = 0.32
        self.waveform.configure()

        //self.waveform.waveColor = .red
        
        self.view.addSubview(self.recordButton)
        self.recordButton.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0).isActive = true
        self.recordButton.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -30).isActive = true
        
        self.recordButton.recordingDuration = 20;
        self.recordButton.delegate=self;
        self.recordButton.addTarget(self, action: #selector(self.startRecording), for: .touchUpInside)
        
        

        let cancelButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.cancel, target: self, action: #selector(self.cancel))
        self.navigationItem.leftBarButtonItem = cancelButton
        

        audioRecorder = audioRecorder(VoiceRecorderViewController.timestampedFilePath())
        
        let displayLink = CADisplayLink(target: self, selector: #selector(updateMeters))
        displayLink.add(to: RunLoop.current, forMode: RunLoop.Mode.common)
        

        // Do any additional setup after loading the view.
    }

    @objc func cancel() {
        self.didFinishWithRecording!(self.audioURL, 0, "", true)
    }
    

    @objc func startRecording() {
        self.recordButton.record()
    }
    
    // RecordingButtonDelegate Methods
    func didStartCapture() {
        // call when capturing starts.
        print("Start Capture")
        //audioRecorder = audioRecorder(VoiceRecorderViewController.timestampedFilePath())
        //self.waveform.intensity = 0.3
        audioRecorder.record()

        //let displayLink = CADisplayLink(target: self, selector: #selector(updateMeters))
        //displayLink.add(to: RunLoop.current, forMode: RunLoop.Mode.common)

    }
    
    func didEndCapture() {
        // call when capturing end.
        print("Stop Capture")
        self.audioRecorder.stop()
        //self.waveform.intensity = 0.02

    }

    @objc func updateMetersFromPlayback() {
        if(audioPlayer != nil) {
            audioPlayer.updateMeters()
            var normalizedValue: Float = 0.0
            if(audioPlayer.averagePower(forChannel: 0) < -60 || audioPlayer.averagePower(forChannel: 0) / 20 == 0) {
                normalizedValue = 0
            }
            else {
                normalizedValue = pow(10, audioPlayer.averagePower(forChannel: 0) / 20)
            }
            self.waveform.update(withLevel: CGFloat(normalizedValue))
        }
    }

    @objc func updateMeters() {
        audioRecorder.updateMeters()

        var normalizedValue: Float = 0.0
        if(audioRecorder.averagePower(forChannel: 0) < -40 || audioRecorder.averagePower(forChannel: 0) / 20 == 0) {
            normalizedValue = 0
        }
        else {
            normalizedValue = pow(10, audioRecorder.averagePower(forChannel: 0) / 20)
        }
        
        self.waveform.update(withLevel: CGFloat(normalizedValue))
    }
    
    func audioRecorder(_ filePath: URL) -> AVAudioRecorder {
        let recorderSettings: [String : AnyObject] = [
            AVSampleRateKey: 44100.0 as AnyObject,
            AVFormatIDKey: NSNumber(value: kAudioFormatMPEG4AAC),
            AVNumberOfChannelsKey: 2 as AnyObject,
            AVEncoderAudioQualityKey: AVAudioQuality.min.rawValue as AnyObject
        ]
        
        try! AVAudioSession.sharedInstance().setCategory(.playAndRecord, mode: .spokenAudio, options: .defaultToSpeaker)
        
        let audioRecorder = try! AVAudioRecorder(url: filePath, settings: recorderSettings)
        audioRecorder.isMeteringEnabled = true
        audioRecorder.prepareToRecord()
        audioRecorder.delegate = self
        
        return audioRecorder
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        self.audioPlayer.stop()
        self.audioPlayer = nil
    }

    static func timestampedFilePath() -> URL {
        let dirPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let currentDateTime = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "ddMMyyyy-HHmmss"
        let recordingName = formatter.string(from: currentDateTime)+".m4a"
        let filePath = URL(string: "file://\(dirPath)/\(recordingName)")
        
        return filePath!
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension VoiceRecorderViewController: AVAudioRecorderDelegate {
    
    public func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        self.audioURL = recorder.url
        self.audioRecorder.stop()
        //try! AVAudioSession.sharedInstance().setActive(false)

    }
    
    public func audioRecorderEncodeErrorDidOccur(_ recorder: AVAudioRecorder, error: Error?) {
    }
    
}
