//
//  MeSettingsViewController.swift
//  NextGenPhoneApp
//
//  Created by Kieselbach, Oliver on 23.10.18.
//  Copyright © 2018 Kieselbach, Oliver. All rights reserved.
//

import UIKit
import Eureka
import LetterAvatarKit
import YPImagePicker
import AVKit
import Photos
import FirebaseStorage
import NextGenPhoneFramework



public class BackgroundCell: PushSelectorCell<UIImage> {
    public override func setup() {
        super.setup()
        
        accessoryType = .none
        editingAccessoryView = .none
        
        let view = UIImageView(frame: CGRect(x: 0, y: 0, width: 44, height: 88))
        view.layer.masksToBounds = true
        view.clipsToBounds = true
        
        accessoryView = view
        editingAccessoryView = view
    }
    
    public override func update() {
        super.update()
        

        /*
        let view = UIImageView(frame: CGRect(x: 0, y: 0, width: 44, height: 88))
        view.clipsToBounds = true
        if(row as! BackgroundRow).image != nil {
            view.image = (row as! BackgroundRow).image
        }
        else {
        }
        */

        self.detailTextLabel!.text = ""

        
        //accessoryView = view
        //editingAccessoryView = view
        accessoryView?.layer.cornerRadius = 5
        editingAccessoryView?.layer.cornerRadius = 5
        selectionStyle = .none
    }
}


public final class MyImageRow: _ImageRow<ImageCell>, RowType {
    
    var presenterView: MeSettingsViewController!
    
    public required init(tag: String?) {
        super.init(tag: tag)
        
    }
    override public func didSelect() {
        //super.didSelect()
        self.presenterView.showMediaPicker(rowTag: self.tag!)
    }
    
}

class MeSettingsViewController: FormViewController {
    
    var availableColor = UIColor(displayP3Red: 46/255, green: 204/255, blue: 113/255, alpha: 1)
    var notAvailableColor = UIColor(displayP3Red: 231/255, green: 76/255, blue: 60/255, alpha: 1)
    

    var contact: CKContact! {
        didSet {
        }
    }

    //var placeHolderImage: UIImage!
    var profileImage: UIImage! {
        didSet {
            let row = self.form.rowBy(tag: "profileImageRow") as! MyImageRow
            row.value = profileImage
            
        }
    }
    var imageRow: MyImageRow = MyImageRow("profileImageRow") { row in
        row.title = "Profile Image"
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.modalPresentationCapturesStatusBarAppearance = true

        /*
        self.header.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.headerHeight)
        self.view.addSubview(self.header)
        self.header.backgroundColor = self.headerColor
        self.header.addSubview(self.titleLabel)
        
        self.titleLabel.leftAnchor.constraint(equalTo: self.header.leftAnchor, constant: 15).isActive = true
        self.titleLabel.rightAnchor.constraint(equalTo: self.header.rightAnchor, constant: -15).isActive = true
        self.titleLabel.centerYAnchor.constraint(equalTo: self.header.centerYAnchor, constant: 0).isActive = true
        self.titleLabel.text = self.title
        */
 
        self.view.backgroundColor = .white
        self.tableView.backgroundColor = .clear
        self.tableView.separatorStyle = .none
    
        //self.tableView.contentInset = UIEdgeInsets(top: self.headerHeight, left: 0, bottom: 40, right: 0)
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        form
            +++ Section("Your Profile")
            <<< TextRow("yourNameRow"){ row in
                row.title = "Your Name"
                row.value = contact.name
            }
                .onCellHighlightChanged({ (cell, row) in
                    if(cell.isSelected == false) {
                        self.contact.name = row.value!
                        self.save()
                    }
                })

            <<< self.imageRow
                .cellUpdate { cell, row in
                }.cellSetup { cell, row in
                    row.presenterView = self
                    
                    let letterKit = LetterAvatarBuilderConfiguration()
                    letterKit.username = self.contact.name
                    letterKit.backgroundColors = [self.contact.status == .available ? self.availableColor : self.notAvailableColor]
                    letterKit.lettersColor = UIColor.white
                    letterKit.lettersFont = UIFont.systemFont(ofSize: UIFont.systemFontSize)
                    let profileImage = UIImage.makeLetterAvatar(withConfiguration: letterKit)
                    if(self.contact.profileImageRef != "") {
                        (cell.accessoryView as! UIImageView).sd_setImage(with: ConversationKit.shared.storage.reference(withPath: self.contact.profileImageRef), placeholderImage: profileImage, completion: { (image, error, cache, ref) in
                            if(error == nil) {
                                row.value = image
                            }
                        })
                    }

                    cell.heightAnchor.constraint(equalToConstant: 60).isActive = true
                    cell.accessoryView?.layer.cornerRadius = 25
                    cell.accessoryView?.frame = CGRect(x: 0, y: 0, width: 50, height: 50)

                }.onChange({ (row) in
                    self.uploadProfileImage(image: row.value!)
                })
                .onCellSelection({ (cell, row) in
                })

            <<< PhoneRow("phoneRow"){ row in
                row.title = "Phone Number"
                row.value = contact.phoneNumber
            }
                .onChange({ (row) in
                    self.contact.phoneNumber = row.value!
                    self.save()
                })
            +++ MultivaluedSection(multivaluedOptions: [.Insert, .Delete],
                                   header: "Assistants") {
                                    $0.addButtonProvider = { section in
                                        return ButtonRow(){
                                            $0.title = "Add New Assistant"
                                            }.cellUpdate { cell, row in
                                                cell.textLabel?.textAlignment = .left
                                        }
                                    }
                                    $0.multivaluedRowToInsertAt = { index in
                                        return NameRow() {
                                            $0.placeholder = "Assistant Name"
                                        }
                                    }
            }
            +++ Section("")
                <<< ButtonRow("qrCodeRow", { (row) in
                    row.title = "Show Profile QR Code"
                }).onCellSelection({ (cell, row) in
                    self.showQRCodeAction()
                })
            <<< ButtonRow("shareProfile", { (row) in
                row.title = "Share Profile"
            }).onCellSelection({ (cell, row) in
                self.shareProfile()
            })
                
        

        
/*
            <<< BackgroundRow("backgroundRow"){ row in
                row.title = "Background"
                row.colorMode = contact.backgroundColorStyle
                row.presenterView = self
                
                }.cellSetup({ (cell, row) in
                    cell.heightAnchor.constraint(equalToConstant: 100).isActive = true

                    if(self.contact.backgroundImageRef != "") {
                        (cell.accessoryView as! UIImageView).sd_setImage(with: AppDelegateAccessor.storage.reference(withPath: self.contact.backgroundImageRef), placeholderImage: nil, completion: { (image, error, cache, ref) in
                            if(error == nil) {
                                row.value = image
                            }
                        })
                    }
                    else {
                        applyGradientToView(view: cell.accessoryView!, gradientStyle: self.contact.backgroundColorStyle)
                    }

                }).cellUpdate({ (cell, row) in
                    if(row.value != nil) {
                        (cell.accessoryView as! UIImageView).image = row.value
                    }
                    else {
                        applyGradientToView(view: cell.accessoryView!, gradientStyle: row.colorMode!)
                        self.contact.backgroundColorStyle = row.colorMode
                        self.save()
                    }
 
                }).onCellHighlightChanged({ (cell, row) in
                }).onChange({ (row) in
                    if(row.value != nil) {
                        row.cell.accessoryView = UIImageView(frame: CGRect(x: 0, y: 0, width: 44, height: 88))
                        (row.cell.accessoryView as! UIImageView).image = row.value
                        row.cell.accessoryView?.layer.cornerRadius = 5

                        self.uploadBackgroundImage(image: row.value!)
                    }
                    else {
                        if(self.contact.backgroundImageRef != "") {
                            self.deleteBackgroundImage()
                        }
                        else {
                            self.contact.backgroundColorStyle = row.colorMode
                            self.save()
                        }
                    }
                })
*/
        // Do any additional setup after loading the view.
        
        
    }
    
    /*
    override func close() {
        AppDelegateAccessor.pulleyController.showContactsDrawer()
    }
    */

    
    func shareProfile() {

        let itemSource = AirDropOnlyActivityItemSource(item: self.contact.getShareingURL())
        let activityVc = UIActivityViewController(activityItems: [itemSource], applicationActivities: nil)
        activityVc.popoverPresentationController?.sourceView = self.view
        activityVc.popoverPresentationController?.sourceRect = self.view.bounds
        
        self.present(activityVc, animated: true, completion: {
        })

    }
    
    
    @objc func showQRCodeAction() {

        let qrCodeImage = generateQRCode(from: "nextgenphoneapp:addContact?id="+ConversationKit.shared.currentUser.uid)

        let controller = DJSemiModalViewController()
        
        
        controller.title = "Share Profile"
        
        let label = UILabel()
        label.text = "Your Profile QR Code"
        label.textAlignment = .center
        controller.addArrangedSubview(view: label)
        
        let imageView = UIImageView(frame: .zero)
        imageView.heightAnchor.constraint(equalToConstant: 150).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: 150).isActive = true
        imageView.image = qrCodeImage
        controller.addArrangedSubview(view: imageView)

        controller.automaticallyAdjustsContentHeight = true
        
        controller.maxWidth = 420
        
        controller.minHeight = 200
        
        controller.titleLabel.font = UIFont.systemFont(ofSize: 22, weight: UIFont.Weight.bold)
        
        controller.closeButton.setTitle("Done", for: .normal)
        
        controller.presentOn(presentingViewController: AppDelegateAccessor.mainViewController, animated: true, onDismiss: { })
        
    }
    
    func save() {
        
        ConversationKit.shared.db.collection("users").document(ConversationKit.shared.currentUser.uid).setData(self.contact.dictionary, merge: true)
        
    }
    
    func uploadProfileImage(image: UIImage) {

        let currentPath = self.contact.profileImageRef
        let newPath = "users/\(self.contact.id)/\(UUID().uuidString)"
        
        ConversationKit.shared.storage.reference(withPath: newPath).putData(image.pngData()!, metadata: nil) { (metadata, error) in

            if(currentPath.starts(with: "users/\(self.contact.id)") == true) {
                ConversationKit.shared.storage.reference(withPath: currentPath).delete(completion: { (error) in
                    print(error?.localizedDescription)
                })
            }
            self.contact.profileImageRef = newPath
            self.save()
        }
        
        
        
    }

    func getPickerConfig() -> YPImagePickerConfiguration {
        var config = YPImagePickerConfiguration()
        config.library.mediaType = .photo
        config.library.onlySquare  = false
        config.onlySquareImagesFromCamera = true
        config.targetImageSize = .cappedTo(size: 1024*1024)
        config.usesFrontCamera = true
        config.showsFilters = false
/*
        config.filters = [YPFilterDescriptor(name: "Normal", filterName: ""),
                          YPFilterDescriptor(name: "Mono", filterName: "CIPhotoEffectMono")]
*/
        config.shouldSaveNewPicturesToAlbum = false
        config.video.compression = AVAssetExportPresetHighestQuality
        //config.albumName = "MyGreatAppName"
        config.screens = [.library, .photo, .video]
        config.startOnScreen = .library
        config.video.recordingTimeLimit = 10
        config.video.libraryTimeLimit = 20
        config.showsCrop = .rectangle(ratio: (3/4))
        config.wordings.libraryTitle = "Gallery"
        config.hidesStatusBar = false
        //config.overlayView = myOverlayView
        config.library.maxNumberOfItems = 1
        config.library.minNumberOfItems = 1
        config.library.numberOfItemsInRow = 3
        config.library.spacingBetweenItems = 2
        config.isScrollToChangeModesEnabled = false

        return config
    }
    
    func showMediaPicker(rowTag: String) {
        
        let picker = YPImagePicker(configuration: self.getPickerConfig())
        picker.didFinishPicking { [unowned picker] items, _ in
            if let photo = items.singlePhoto {
                
                if(rowTag == "profileImageRow") {
                    self.profileImage = photo.image
                }
                
            }
            picker.dismiss(animated: true, completion: nil)
        }

        self.present(picker, animated: true) {
        }
        
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
