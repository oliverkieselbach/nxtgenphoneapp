//
//  UIViewExtensions.swift
//  NextGenPhoneApp
//
//  Created by Oliver Kieselbach on 05.10.18.
//  Copyright © 2018 Kieselbach, Oliver. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    func setShadowWithColor(color: UIColor?, opacity: Float?, offset: CGSize?, radius: CGFloat, viewCornerRadius: CGFloat?) {
        //layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: viewCornerRadius ?? 0.0).CGPath
        layer.shadowColor = color?.cgColor ?? UIColor.black.cgColor
        layer.shadowOpacity = opacity ?? 1.0
        layer.shadowOffset = offset ?? CGSize.zero
        layer.shadowRadius = radius ?? 0
    }
    /*
    func addBlurView() {
        let mask = autoresizingMask
        let blurContainer = UIView(frame: frame)
        addSubview(blurContainer)
        blurContainer.frame = bounds
        blurContainer.autoresizingMask = mask
        let effect = UIBlurEffect(style: .extraLight)
        let blurView = UIVisualEffectView(effect: effect)
        blurContainer.addSubview(blurView)
        blurView.frame = bounds
        blurView.autoresizingMask = mask
    }
    */
    
    func addBlurView(style: UIBlurEffect.Style? = .extraLight) {

        for view in self.subviews {
            if(view.tag == 9999) {
                view.removeFromSuperview()
                break
            }
        }

        let mask = autoresizingMask
        let blurContainer = UIView(frame: frame)
        blurContainer.tag = 9999
        addSubview(blurContainer)
        blurContainer.frame = bounds
        blurContainer.autoresizingMask = mask
        let effect = UIBlurEffect(style: style!)
        let blurView = UIVisualEffectView(effect: effect)
        blurContainer.addSubview(blurView)
        blurView.frame = bounds
        blurView.autoresizingMask = mask
    }
}
