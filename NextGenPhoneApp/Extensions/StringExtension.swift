//
//  StringExtension.swift
//  NextGenPhoneApp
//
//  Created by Kieselbach, Oliver on 01.10.18.
//  Copyright © 2018 Kieselbach, Oliver. All rights reserved.
//

import Foundation

extension String {
    
    public var length: Int { return (self as NSString).length }
    
    public func charAt(index: Int) -> String {
        return String(Array(self)[index])
    }
    
    public func charCodeAt(index: Int) -> Int {
        return Int((self as NSString).character(at: index))
    }
    
    public func concat(strings: String...) -> String {
        var out = self
        for str in strings {
            out += str
        }
        return out
    }
    
    public func fromCharCode() -> String {
        return String(NSString(format: "%c", self.count))
    }
    
    public func replaceFirst(of pattern:String,
                             with replacement:String) -> String {
        if let range = self.range(of: pattern){
            return self.replacingCharacters(in: range, with: replacement)
        }else{
            return self
        }
    }
    
    public func replaceAll(of pattern:String,
                           with replacement:String,
                           options: NSRegularExpression.Options = []) -> String{
        do{
            let regex = try NSRegularExpression(pattern: pattern, options: [])
            let range = NSRange(0..<self.utf16.count)
            return regex.stringByReplacingMatches(in: self, options: [],
                                                  range: range, withTemplate: replacement)
        }catch{
            NSLog("replaceAll error: \(error)")
            return self
        }
    }

    
}
