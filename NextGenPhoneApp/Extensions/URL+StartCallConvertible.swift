//
//  URL+StartCallConvertible.swift
//  NextGenPhoneApp
//
//  Created by Kieselbach, Oliver on 01.08.18.
//  Copyright © 2018 Kieselbach, Oliver. All rights reserved.
//

import Foundation

extension URL: StartCallConvertible {
    
    private struct Constants {
        static let URLScheme = "nextgenphoneapp"
    }
    
    var startCallHandle: String? {
        guard scheme == Constants.URLScheme else {
            return nil
        }
        
        return host
    }
    
}
