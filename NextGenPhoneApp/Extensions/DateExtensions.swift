//
//  DateExtensions.swift
//  NextGenPhoneApp
//
//  Created by Oliver Kieselbach on 05.10.18.
//  Copyright © 2018 Kieselbach, Oliver. All rights reserved.
//

import Foundation
import FirebaseFirestore

extension Date {
    
    func toFirebaseTimestamp() -> Timestamp {
        return Timestamp(date: self)
    }
    
    init?(timestamp: Timestamp) {
        self.init()
        self = timestamp.dateValue()
    }
    
}

