//
//  FormFactory.swift
//  NextGenPhoneApp
//
//  Created by Kieselbach, Oliver on 18.05.19.
//  Copyright © 2019 Kieselbach, Oliver. All rights reserved.
//

import Foundation
import UIKit
import NextGenPhoneFramework
import FirebaseStorage
import FirebaseFirestore
import FirebaseFunctions
import Eureka

public enum FormParamType: String {
    case string = "string"
    case date = "date"
    case time = "time"
    case bool = "bool"
}

public struct FormParameter {
    
    public var id: String!
    public var name: String!
    public var type: FormParamType!
    
    public var dictionary: [String: Any] {
        return [
            "id" : id,
            "name": name,
            "type": type.rawValue
        ]
        
    }
    
}

extension FormParameter {
    public init?(id: String, dictionary: [String : Any]) {
        self.init(id: id, dictionary: dictionary)
    }
    
    public init?(id: String, name: String, type: FormParamType) {
        self.id = id
        self.name = name
        self.type = type
    }
}

struct FormHeader {
    var title: String
    var description: String
}

class FormHeaderView: UIView {
    
    var formTitle: UILabel = {
        
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        label.textAlignment = .left
        label.font = UIFont.systemFont(ofSize: 22, weight: .bold)
        
        return label
        
    }()
    
    var formDescription: UILabel = {
        
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .gray
        label.textAlignment = .left
        label.font = UIFont.systemFont(ofSize: 13, weight: .regular)
        label.numberOfLines = 0
        
        return label
        
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubview(self.formTitle)
        self.addSubview(self.formDescription)
        
        self.backgroundColor = .white
        
        self.formTitle.bottomAnchor.constraint(equalTo: self.formDescription.topAnchor, constant: -5).isActive = true
        self.formTitle.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 15).isActive = true
        self.formTitle.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -15).isActive = true
        
        self.formDescription.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 15).isActive = true
        self.formDescription.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -15).isActive = true
        
        self.formDescription.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -10).isActive = true
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


class FormFactory {
    
    static func createForm(form: Form, formParamater: [FormParameter], formHeader: FormHeader, formDocument: DocumentSnapshot) -> Form {

        let documentData = formDocument.data()
        
        let section = Section(){ section in
            var header = HeaderFooterView<FormHeaderView>(.class)
            header.height = {130}
            header.onSetupView = { view, _ in
                view.formTitle.text = formHeader.title
                view.formTitle.textColor = .black
                view.formDescription.text = formHeader.description
                view.formDescription.textColor = .gray
                //view.backgroundColor = UIColor(red: 249/255, green: 249/255, blue: 249/255, alpha: 1)
            }
            section.header = header
        }
        
        form.append(section)
        
        for i in 0..<formParamater.count {
            
            let parameter = formParamater[i]
            
            switch parameter.type! {
            case .date:
                section <<< DateInlineRow() {
                    $0.tag = parameter.name
                    $0.title = parameter.name
                    $0.cell.detailTextLabel?.textColor = UIColor.actionColor(color: .red)
                    //$0.cell.backgroundColor = UIColor(red: 249/255, green: 249/255, blue: 249/255, alpha: 1)

                    //read from formDocument
                    if(documentData != nil) {
                        if(documentData![parameter.id] != nil) {
                            $0.value = Date(timestamp: documentData![parameter.id] as! Timestamp)
                        }
                    }
                    //$0.disabled = Condition(booleanLiteral: true)

                    $0.onChange({ (row) in
                        //write to formdocument
                        formDocument.reference.setData([parameter.id : row.value?.toFirebaseTimestamp()], merge: true)
                    })
                }
            case .time:
                section <<< TimeInlineRow() {
                    $0.tag = parameter.name
                    $0.title = parameter.name
                    $0.cell.detailTextLabel?.textColor = UIColor.actionColor(color: .red)
                    //$0.cell.backgroundColor = UIColor(red: 249/255, green: 249/255, blue: 249/255, alpha: 1)


                    //read from formDocument
                    if(documentData != nil) {
                        if(documentData![parameter.id] != nil) {
                            $0.value = Date(timestamp: documentData![parameter.id] as! Timestamp)
                        }
                    }

                    $0.onChange({ (row) in
                        //write to formdocument
                        formDocument.reference.setData([parameter.id : row.value?.toFirebaseTimestamp()], merge: true)
                    })
                }
            case .string:
                section <<< TextRow() {
                    $0.tag = parameter.name

                    $0.title = parameter.name
                    $0.cell.detailTextLabel?.textColor = UIColor.actionColor(color: .red)
                    //$0.cell.backgroundColor = UIColor(red: 249/255, green: 249/255, blue: 249/255, alpha: 1)


                    if(documentData != nil) {
                        if(documentData![parameter.id] != nil) {
                            $0.value = documentData![parameter.id] as! String
                        }
                    }
                    $0.onChange({ (row) in
                        //write to formdocument
                        formDocument.reference.setData([parameter.id : row.value!], merge: true)
                    })
                }
            case .bool:
                section <<< SwitchRow() {
                    $0.tag = parameter.name

                    $0.title = parameter.name
                    //$0.cell.backgroundColor = UIColor(red: 249/255, green: 249/255, blue: 249/255, alpha: 1)


                    if(documentData != nil) {
                        if(documentData![parameter.id] != nil) {
                            $0.value = documentData![parameter.id] as! Bool
                        }
                    }
                    $0.onChange({ (row) in
                        formDocument.reference.setData([parameter.id : row.value!], merge: true)
                    })
                }
            }
            
        }

        return form
    }
    
}

