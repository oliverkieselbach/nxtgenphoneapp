//
//  IntentHandler.swift
//  CallIntentHandler
//
//  Created by Kieselbach, Oliver on 01.08.18.
//  Copyright © 2018 Kieselbach, Oliver. All rights reserved.
//

import Intents

// As an example, this class is set up to handle Message intents.
// You will want to replace this or add other intents as appropriate.
// The intents you wish to handle must be declared in the extension's Info.plist.

// You can test your example integration by saying things to Siri like:
// "Send a message using <myApp>"
// "<myApp> John saying hello"
// "Search for messages in <myApp>"

class IntentHandler: INExtension, INStartAudioCallIntentHandling {
    
    func handle(intent: INStartAudioCallIntent, completion: @escaping (INStartAudioCallIntentResponse) -> Void) {
        let response: INStartAudioCallIntentResponse
        defer {
            completion(response)
        }
        
        // Ensure there is a person handle
        guard intent.contacts?.first?.personHandle != nil else {
            response = INStartAudioCallIntentResponse(code: .failure, userActivity: nil)
            return
        }
        
        let userActivity = NSUserActivity(activityType: String(describing: INStartAudioCallIntent.self))
        
        response = INStartAudioCallIntentResponse(code: .continueInApp, userActivity: userActivity)
    }
    
}
